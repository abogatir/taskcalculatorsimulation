﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.ViewModels
{
    public class TimeLineDistributionRowViewModel
    {
        private List<object> RowParams { get; set; }
        public TimeLineDistributionRowViewModel(Country country, BidParameterCountryMonth[] parameterCountryMounth)
        {
            RowParams = new List<object>();

            RowParams.Insert(0, country.Description);
            for (var i = 0; i < parameterCountryMounth.Count(); i++)
            {
                RowParams.Insert(i + 1, parameterCountryMounth[i].Value);
            }
            RowParams.Insert(parameterCountryMounth.Count() + 1, parameterCountryMounth.Sum(x => x.Value));
        }

        public object[] ToParamsArray()
        {
            return RowParams.ToArray<object>();
        }
    }
}
