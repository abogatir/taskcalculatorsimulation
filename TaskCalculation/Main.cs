﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TaskCalculation.Core.Classes;
using TaskCalculation.Core.Mocks;
using TaskCalculation.Core.Models;
using TaskCalculation.ViewModels;

namespace TaskCalculation
{
    public partial class Main : Form
    {

        List<BidParameterCountryMonth> mCountryMonths;
        protected List<BidParameterCountryMonth> CountryMonths
        {
            get
            {
                if (mCountryMonths == null)
                    mCountryMonths = OneDayVisits.GetList(291556);
                return mCountryMonths;
            }
        }
        List<Country> mCountryList;
        List<Country> CountryList
        {
            get
            {
                if (mCountryList == null)
                {
                    List<int> countries = CountryMonths.OrderBy(cm => cm.BidParameterCountryId).Select(cm => cm.BidParameterCountryId).Distinct().ToList();

                    mCountryList = Countries.CountriesList.Where(c => countries.Contains(c.CountryId)).ToList();
                }
                return mCountryList;
            }
        }

        public Main()
        {
            InitializeComponent();
            BindData();
        }

        public void BindData()
        {
            bidRatesDropDown.DataSource = BidRates.BidRatesList;
            bidRatesDropDown.DisplayMember = "DisplayMember";
            bidRatesDropDown.ValueMember = "BidRateId";
            bidPriceDriverCountryDropDown.DataSource = BidPriceDriverCountries.ListBidPriceDriverCountries;
            bidPriceDriverCountryDropDown.DisplayMember = "DisplayMember";
            bidPriceDriverCountryDropDown.ValueMember = "BidPriceDriverCountryId";
            taskNameLabel.Text = @"Task: "+ MockProvider.BidTask.Description;
            BindTimelineDistributionData();           

        }

        private void BindTimelineDistributionData()
        {
            BidParameterCountryMonth first = CountryMonths.FirstOrDefault();
            if (first == null)
                return;
            timelineDistributionGridView.ReadOnly = true;
            timelineDistributionGridView.Columns.Add(new DataGridViewColumn(){HeaderText = "Country",CellTemplate = new DataGridViewTextBoxCell()});
            CountryMonths
                .Where(cm => cm.BidParameterCountryId == first.BidParameterCountryId)
                .OrderBy(cm => cm.BidParameterCountryMonthId)
                .ToList()
                .ForEach(x => timelineDistributionGridView.Columns
                    .Add(new DataGridViewColumn() { HeaderText = x.BidParameterCountryMonthId.ToString(), CellTemplate = new DataGridViewTextBoxCell(), Width = 30 }));
            timelineDistributionGridView.Columns.Add(new DataGridViewColumn() { HeaderText = "Total", CellTemplate = new DataGridViewTextBoxCell() });            

            foreach (Country country in CountryList)
            {                
                var countryMonths = CountryMonths.Where(cm => cm.BidParameterCountryId == country.CountryId).ToArray();
                var timeLineRowViewModel = new TimeLineDistributionRowViewModel(country, countryMonths);
                timelineDistributionGridView.Rows.Add(timeLineRowViewModel.ToParamsArray());
            }
        }


        private void btnCalculate_Click(object sender, EventArgs e)
        {
            var scenario = MockProvider.BidScenario;
            var task = MockProvider.BidTask;
            var tree = MockProvider.BidTaskTree;

            TaskCalculator.Output result = TaskCalculator.CalculateIndividualTask
                                (scenario,
                                 tree.BidFunctionalServiceAreaId,
                                 tree.BidServiceId,
                                 tree.BidActivityId,
                                 task,
                                 DateTime.Now);

            ScenarioCalculator.CalculateAndSaveBidOverview(result.Overview);

            ShowCalculationResults(result);
        }

        private void ShowCalculationResults(TaskCalculator.Output result)
        {
            taskDetailsGrid.Rows.Clear();

            taskDetailsGrid.Rows.Add("Rate Card Price",
                TextFormatter.GetCurrencyText(result.Overview.TotalServicePrice, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalFeePrice, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalExpensePrice, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalPrice, result.Overview.CurrencyCode));

            taskDetailsGrid.Rows.Add("Total Cost",
                TextFormatter.GetCurrencyText(result.Overview.TotalServiceCost, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalFeeCost, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalExpenseCost, result.Overview.CurrencyCode),
                TextFormatter.GetCurrencyText(result.Overview.TotalCost, result.Overview.CurrencyCode));

            taskDetailsGrid.Rows.Add("Margin",
                TextFormatter.GetPercentageText(result.Overview.ServiceMargin) + "%",
                TextFormatter.GetPercentageText(result.Overview.FeeMargin) + "%",
                TextFormatter.GetPercentageText(result.Overview.ExpenseMargin) + "%",
                TextFormatter.GetPercentageText(result.Overview.TotalMargin) + "%");

            taskDetailsGrid.Rows.Add("Total Hours",
                Math.Round(result.Overview.TotalEffort, 2).ToString(SiteConstants.HOURS_DISPLAY) + " hour(s)");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            taskDetailsGrid.Rows.Clear();
        }

        private void bidRatesDropDown_SelectedValueChanged(object sender, EventArgs e)
        {
            int bidRateId;

            if (Int32.TryParse(bidRatesDropDown.SelectedValue.ToString(), out bidRateId))
            {
                var bidRate = BidRates.BidRatesList.FirstOrDefault(x => x.BidRateId == bidRateId);

                if (bidRate != null)
                {
                    decimal appliedRate;
                    if (bidRate.BidRateOverride != null && bidRate.BidRateOverride.BidRateOverrideId != -1)
                    {
                        appliedRate = bidRate.BidRateOverride.AppliedRate;
                    }
                    else
                    {
                        appliedRate = bidRate.AppliedRate;
                    }
                    bidRateAppliedRate.Text = appliedRate.ToString();
                }
            }
        }

        private void bidRateAppliedRate_TextChanged(object sender, EventArgs e)
        {
            decimal appliedRate;
            int bidRateId;

            if (Decimal.TryParse(bidRateAppliedRate.Text, out appliedRate) && Int32.TryParse(bidRatesDropDown.SelectedValue.ToString(), out bidRateId))
            {
                var bidRate = BidRates.BidRatesList.FirstOrDefault(x => x.BidRateId == bidRateId);

                if (bidRate != null)
                {
                    if (bidRate.BidRateOverride != null && bidRate.BidRateOverride.BidRateOverrideId != -1)
                    {
                        bidRate.BidRateOverride.AppliedRate = appliedRate;
                    }
                    else
                    {
                        bidRate.AppliedRate = appliedRate;
                    }
                }
            }
        }

        private void bidPriceDriverCountryDropDown_SelectedValueChanged(object sender, EventArgs e)
        {
            int bidPriceDriverCountryId;

            if (Int32.TryParse(bidPriceDriverCountryDropDown.SelectedValue.ToString(), out bidPriceDriverCountryId))
            {
                var bidPriceDriverCountry = BidPriceDriverCountries.ListBidPriceDriverCountries.FirstOrDefault(x => x.BidPriceDriverCountryId == bidPriceDriverCountryId);

                if (bidPriceDriverCountry != null)
                {                    
                    bidPriceDriverCountryCostPerUnit.Text = bidPriceDriverCountry.MasterCostPerUnit.ToString();
                    bidPriceDriverCountryPricePerUnit.Text = bidPriceDriverCountry.MasterPricePerUnit.ToString();
                }
            }
        }

        private void bidPriceDriverCountryCostPerUnit_TextChanged(object sender, EventArgs e)
        {
            decimal costPerUnit;
            int bidPriceDriverCountryId;

            if (Decimal.TryParse(bidPriceDriverCountryCostPerUnit.Text, out costPerUnit) && Int32.TryParse(bidPriceDriverCountryDropDown.SelectedValue.ToString(), out bidPriceDriverCountryId))
            {
                var bidPriceDriverCountry = BidPriceDriverCountries.ListBidPriceDriverCountries.FirstOrDefault(x => x.BidPriceDriverCountryId == bidPriceDriverCountryId);

                if (bidPriceDriverCountry != null)
                {
                    bidPriceDriverCountry.MasterCostPerUnit = costPerUnit;
                }
            }
        }

        private void bidPriceDriverCountryPricePerUnit_TextChanged(object sender, EventArgs e)
        {
            decimal pricePerUnit;
            int bidPriceDriverCountryId;

            if (Decimal.TryParse(bidPriceDriverCountryPricePerUnit.Text, out pricePerUnit) && Int32.TryParse(bidPriceDriverCountryDropDown.SelectedValue.ToString(), out bidPriceDriverCountryId))
            {
                var bidPriceDriverCountry = BidPriceDriverCountries.ListBidPriceDriverCountries.FirstOrDefault(x => x.BidPriceDriverCountryId == bidPriceDriverCountryId);

                if (bidPriceDriverCountry != null)
                {
                    bidPriceDriverCountry.MasterPricePerUnit = pricePerUnit;
                }
            }
        }

    }
}
