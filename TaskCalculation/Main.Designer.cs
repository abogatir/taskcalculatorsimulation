﻿namespace TaskCalculation
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.taskDetailsGrid = new System.Windows.Forms.DataGridView();
            this.RowCaptions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServicesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FeesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpensesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidRatesDropDown = new System.Windows.Forms.ComboBox();
            this.bidRateAppliedRate = new System.Windows.Forms.TextBox();
            this.bidRateDropDownLabel = new System.Windows.Forms.Label();
            this.appliedRateLabel = new System.Windows.Forms.Label();
            this.bidPriceDriverDropDownLabel = new System.Windows.Forms.Label();
            this.costPerUnitLabel = new System.Windows.Forms.Label();
            this.pricePerUnitLabel = new System.Windows.Forms.Label();
            this.bidPriceDriverCountryDropDown = new System.Windows.Forms.ComboBox();
            this.bidPriceDriverCountryCostPerUnit = new System.Windows.Forms.TextBox();
            this.bidPriceDriverCountryPricePerUnit = new System.Windows.Forms.TextBox();
            this.taskNameLabel = new System.Windows.Forms.Label();
            this.timelineDistributionGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.taskDetailsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timelineDistributionGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(28, 12);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 0;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(121, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // taskDetailsGrid
            // 
            this.taskDetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.taskDetailsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowCaptions,
            this.ServicesColumn,
            this.FeesColumn,
            this.ExpensesColumn,
            this.TotalColumn});
            this.taskDetailsGrid.Location = new System.Drawing.Point(28, 56);
            this.taskDetailsGrid.Name = "taskDetailsGrid";
            this.taskDetailsGrid.Size = new System.Drawing.Size(544, 167);
            this.taskDetailsGrid.TabIndex = 3;
            // 
            // RowCaptions
            // 
            this.RowCaptions.HeaderText = "";
            this.RowCaptions.Name = "RowCaptions";
            this.RowCaptions.ReadOnly = true;
            this.RowCaptions.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ServicesColumn
            // 
            this.ServicesColumn.HeaderText = "Services";
            this.ServicesColumn.Name = "ServicesColumn";
            this.ServicesColumn.ReadOnly = true;
            this.ServicesColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FeesColumn
            // 
            this.FeesColumn.HeaderText = "Fees";
            this.FeesColumn.Name = "FeesColumn";
            this.FeesColumn.ReadOnly = true;
            this.FeesColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ExpensesColumn
            // 
            this.ExpensesColumn.HeaderText = "Expenses";
            this.ExpensesColumn.Name = "ExpensesColumn";
            this.ExpensesColumn.ReadOnly = true;
            this.ExpensesColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TotalColumn
            // 
            this.TotalColumn.HeaderText = "Total";
            this.TotalColumn.Name = "TotalColumn";
            this.TotalColumn.ReadOnly = true;
            this.TotalColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // bidRatesDropDown
            // 
            this.bidRatesDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bidRatesDropDown.FormattingEnabled = true;
            this.bidRatesDropDown.Location = new System.Drawing.Point(25, 257);
            this.bidRatesDropDown.Name = "bidRatesDropDown";
            this.bidRatesDropDown.Size = new System.Drawing.Size(171, 21);
            this.bidRatesDropDown.TabIndex = 4;
            this.bidRatesDropDown.SelectedValueChanged += new System.EventHandler(this.bidRatesDropDown_SelectedValueChanged);
            // 
            // bidRateAppliedRate
            // 
            this.bidRateAppliedRate.Location = new System.Drawing.Point(229, 258);
            this.bidRateAppliedRate.Name = "bidRateAppliedRate";
            this.bidRateAppliedRate.Size = new System.Drawing.Size(121, 20);
            this.bidRateAppliedRate.TabIndex = 5;
            this.bidRateAppliedRate.TextChanged += new System.EventHandler(this.bidRateAppliedRate_TextChanged);
            // 
            // bidRateDropDownLabel
            // 
            this.bidRateDropDownLabel.AutoSize = true;
            this.bidRateDropDownLabel.Location = new System.Drawing.Point(25, 238);
            this.bidRateDropDownLabel.Name = "bidRateDropDownLabel";
            this.bidRateDropDownLabel.Size = new System.Drawing.Size(48, 13);
            this.bidRateDropDownLabel.TabIndex = 6;
            this.bidRateDropDownLabel.Text = "Bid Rate";
            // 
            // appliedRateLabel
            // 
            this.appliedRateLabel.AutoSize = true;
            this.appliedRateLabel.Location = new System.Drawing.Point(229, 238);
            this.appliedRateLabel.Name = "appliedRateLabel";
            this.appliedRateLabel.Size = new System.Drawing.Size(68, 13);
            this.appliedRateLabel.TabIndex = 7;
            this.appliedRateLabel.Text = "Applied Rate";
            // 
            // bidPriceDriverDropDownLabel
            // 
            this.bidPriceDriverDropDownLabel.AutoSize = true;
            this.bidPriceDriverDropDownLabel.Location = new System.Drawing.Point(28, 285);
            this.bidPriceDriverDropDownLabel.Name = "bidPriceDriverDropDownLabel";
            this.bidPriceDriverDropDownLabel.Size = new System.Drawing.Size(101, 13);
            this.bidPriceDriverDropDownLabel.TabIndex = 8;
            this.bidPriceDriverDropDownLabel.Text = "Price Driver Country";
            // 
            // costPerUnitLabel
            // 
            this.costPerUnitLabel.AutoSize = true;
            this.costPerUnitLabel.Location = new System.Drawing.Point(229, 285);
            this.costPerUnitLabel.Name = "costPerUnitLabel";
            this.costPerUnitLabel.Size = new System.Drawing.Size(69, 13);
            this.costPerUnitLabel.TabIndex = 9;
            this.costPerUnitLabel.Text = "Cost Per Unit";
            // 
            // pricePerUnitLabel
            // 
            this.pricePerUnitLabel.AutoSize = true;
            this.pricePerUnitLabel.Location = new System.Drawing.Point(379, 284);
            this.pricePerUnitLabel.Name = "pricePerUnitLabel";
            this.pricePerUnitLabel.Size = new System.Drawing.Size(72, 13);
            this.pricePerUnitLabel.TabIndex = 10;
            this.pricePerUnitLabel.Text = "Price Per Unit";
            // 
            // bidPriceDriverCountryDropDown
            // 
            this.bidPriceDriverCountryDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bidPriceDriverCountryDropDown.FormattingEnabled = true;
            this.bidPriceDriverCountryDropDown.Location = new System.Drawing.Point(25, 302);
            this.bidPriceDriverCountryDropDown.Name = "bidPriceDriverCountryDropDown";
            this.bidPriceDriverCountryDropDown.Size = new System.Drawing.Size(171, 21);
            this.bidPriceDriverCountryDropDown.TabIndex = 11;
            this.bidPriceDriverCountryDropDown.SelectedValueChanged += new System.EventHandler(this.bidPriceDriverCountryDropDown_SelectedValueChanged);
            // 
            // bidPriceDriverCountryCostPerUnit
            // 
            this.bidPriceDriverCountryCostPerUnit.Location = new System.Drawing.Point(232, 304);
            this.bidPriceDriverCountryCostPerUnit.Name = "bidPriceDriverCountryCostPerUnit";
            this.bidPriceDriverCountryCostPerUnit.Size = new System.Drawing.Size(118, 20);
            this.bidPriceDriverCountryCostPerUnit.TabIndex = 12;
            this.bidPriceDriverCountryCostPerUnit.TextChanged += new System.EventHandler(this.bidPriceDriverCountryCostPerUnit_TextChanged);
            // 
            // bidPriceDriverCountryPricePerUnit
            // 
            this.bidPriceDriverCountryPricePerUnit.Location = new System.Drawing.Point(382, 304);
            this.bidPriceDriverCountryPricePerUnit.Name = "bidPriceDriverCountryPricePerUnit";
            this.bidPriceDriverCountryPricePerUnit.Size = new System.Drawing.Size(110, 20);
            this.bidPriceDriverCountryPricePerUnit.TabIndex = 13;
            this.bidPriceDriverCountryPricePerUnit.TextChanged += new System.EventHandler(this.bidPriceDriverCountryPricePerUnit_TextChanged);
            // 
            // taskNameLabel
            // 
            this.taskNameLabel.AutoSize = true;
            this.taskNameLabel.Location = new System.Drawing.Point(202, 17);
            this.taskNameLabel.Name = "taskNameLabel";
            this.taskNameLabel.Size = new System.Drawing.Size(60, 13);
            this.taskNameLabel.TabIndex = 14;
            this.taskNameLabel.Text = "Task Label";
            // 
            // timelineDistributionGridView
            // 
            this.timelineDistributionGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.timelineDistributionGridView.Location = new System.Drawing.Point(28, 343);
            this.timelineDistributionGridView.Name = "timelineDistributionGridView";
            this.timelineDistributionGridView.Size = new System.Drawing.Size(959, 144);
            this.timelineDistributionGridView.TabIndex = 15;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 499);
            this.Controls.Add(this.timelineDistributionGridView);
            this.Controls.Add(this.taskNameLabel);
            this.Controls.Add(this.bidPriceDriverCountryPricePerUnit);
            this.Controls.Add(this.bidPriceDriverCountryCostPerUnit);
            this.Controls.Add(this.bidPriceDriverCountryDropDown);
            this.Controls.Add(this.pricePerUnitLabel);
            this.Controls.Add(this.costPerUnitLabel);
            this.Controls.Add(this.bidPriceDriverDropDownLabel);
            this.Controls.Add(this.appliedRateLabel);
            this.Controls.Add(this.bidRateDropDownLabel);
            this.Controls.Add(this.bidRateAppliedRate);
            this.Controls.Add(this.bidRatesDropDown);
            this.Controls.Add(this.taskDetailsGrid);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCalculate);
            this.Name = "Main";
            this.Text = "Task Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.taskDetailsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timelineDistributionGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridView taskDetailsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowCaptions;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServicesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FeesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpensesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalColumn;
        private System.Windows.Forms.ComboBox bidRatesDropDown;
        private System.Windows.Forms.TextBox bidRateAppliedRate;
        private System.Windows.Forms.Label bidRateDropDownLabel;
        private System.Windows.Forms.Label appliedRateLabel;
        private System.Windows.Forms.Label bidPriceDriverDropDownLabel;
        private System.Windows.Forms.Label costPerUnitLabel;
        private System.Windows.Forms.Label pricePerUnitLabel;
        private System.Windows.Forms.ComboBox bidPriceDriverCountryDropDown;
        private System.Windows.Forms.TextBox bidPriceDriverCountryCostPerUnit;
        private System.Windows.Forms.TextBox bidPriceDriverCountryPricePerUnit;
        private System.Windows.Forms.Label taskNameLabel;
        private System.Windows.Forms.DataGridView timelineDistributionGridView;
    }
}

