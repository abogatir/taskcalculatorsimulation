﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Models
{
    public class CountryMonthValueList
    {
        public Country country;
        public List<CountryMonthValue> CountryMonthValue;
        public CountryMonthValueList(Country country, IEnumerable<BidParameterCountryMonth> values)
        {
            this.country = country;
            BidParameterCountryMonth bpcm = values.First();
            int a = bpcm.Value.HasValue ? (int)bpcm.Value.Value : 0;
            CountryMonthValue = values.Select(b => new CountryMonthValue(b.Value.HasValue ? (int)b.Value.Value : 0)).ToList();
        }
    }
}
