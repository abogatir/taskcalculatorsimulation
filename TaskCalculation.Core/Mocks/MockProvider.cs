﻿using System;
using System.Collections.Generic;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class MockProvider
    {
        public static BidOverview TaskCalculatorOutputBidOverview = new BidOverview()
        {
            BidActivityId = 2359444,
            BidFunctionalServiceAreaId = 292741,
            BidOverviewId = -1,
            BidScenarioId = 291556,
            BidServiceId = 568469,
            BidTaskId = 6403721,
            CurrencyCode = "USD",
            ExpenseMargin = 0,
            FeeMargin = 0,
            RegionId = -1,
            ServiceMargin = 0,
            TotalCost = 0,
            TotalEffort = 3682,
            TotalExpenseCost = 126810,
            TotalExpensePrice = 126810,
            TotalFeeCost = 0,
            TotalFeePrice = 0,
            TotalMargin = 0,
            TotalPrice = 0,
            TotalServiceCost = 221504.38M,
            TotalServicePrice = 416326.44M,
        };

        public static BidScenario BidScenario = new BidScenario
        {
            BidId = 280385,
            BidScenarioId = 291556,
            BtmLineDiscDefaultAlloc = true,
            BtmLineDiscPriceDriven = true,
            CalculationsAreComplete = false,
            CreatedBy = 2569,
            DataManagementProcMethodId = -1,
            DateCreated = new DateTime(2011,04,21,15,45,51),
            Description = "CO#1 addition of 49 sites & new counties",
            LongDescription = " ",
            OutputCurrencyCode = "USD",
            PrimaryUnitGridId = 130214,
            ScenarioNumber = 6

        };

        public static BidTask BidTask = new BidTask
        {
            AlwaysDefaultOn = true,
            BidActivityId = 2359444,
            BidInstructions = "",
            BidTaskId = 6403721,
            ClientDescription = "",
            ConditionalOnStatement = "",
            CopyNumber = 0,
            Description = "Monitoring Review (On-site)",
            DisplayOrder = 3,
            EligibleForEfficiency = true,
            EndDate = new DateTime (2011, 12, 30),
            EndDateOffset = 0,
            EndDateOffsetIntervalId = 0,
            Level4Code = "AOW",
            StartDate = new DateTime (2010, 7, 30),
            LongDescription = "CRA time on-site to source verify CRFs, complete IP reconciliation, and review regulatory binder.",
            MasterActivityId = 146749,
            MasterTaskId = 138441,
            OutlineNumber = "3.B.3",
            Selected = true,
            StartDateOffset = 0,
            StartDateOffsetIntervalId = 0,
            TaskEndParameterId = 5334177,
            TaskStartParameterId = 5334193,
            UniqueName = "1.0_IMVS_MONITORING_REVIEW"
        };

        public static BidTaskTree BidTaskTree = new BidTaskTree
        {
            BidActivityId = 2359444,
            BidFunctionalServiceAreaId = 292741,
            BidScenarioId = 291556,
            BidServiceId = 568469,
            BidTaskId = 6403721
        };
        public static List<BidTask> BidPriceDriverBidTasks = new List<BidTask>
        {
            new BidTask(),
            new BidTask(),
            new BidTask(),
            new BidTask(),
        };
        public static BidInflationCard BidInflationCardForBidScenario = new BidInflationCard()
        {
            BidInflationCardId = 141793,
            BidScenarioId = 291556,
            Description = "Standard Billing",
            ExpirationDate = new DateTime(2025,5,1),
            InflationCardType = DBProperties.enuInflationCardType.ACTIVE_BILLING_INFLATION_CARD,
            InflationEndDate = new DateTime(2012,2,17),
            InflationStartDate = new DateTime(2010,1,1),
            MasterInflationCardId = 101742,
            StartDate = new DateTime(2009,1,5)
        };
    }
}
