﻿using System.Collections.Generic;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidRates
    {
        public static List<BidRate> BidRatesList = new List<BidRate>()
        {
            //CR
            //0
            new BidRate()
            {
                AppliedRate = 55.09M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637476,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode = "ISRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 35
            },
            //1
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637650,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "FLRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109
            },
            //2
            new BidRate()
            {
                AppliedRate = 120.73M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637805,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 102.62M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "WAWCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 79M
            },
            //3
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637965,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode = "USMCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 140M
            },
            //4
            new BidRate()
            {
                AppliedRate = 120.73M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637748,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 102.62M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "ROUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 79M
            },
            //5
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637738,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "PRSCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //6
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637906,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "NLDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 109M
            },
            //7
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637531,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "MHGCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //8
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637549,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "PRSCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //9
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637707,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode ="MADCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //10
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637997,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode ="BRUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 109M
            },
            //11
            new BidRate()
            {
                AppliedRate = 77.66M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637605,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode ="USMCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1,
                InflationRate = 1.0494M,
                Rate = 74M
            },
            //12
            new BidRate()
            {
                AppliedRate = 44.07M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637569,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode ="SAOCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1,
                InflationRate = 1.0494M,
                Rate = 42M
            },
            //13
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637734,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode ="NLDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //14
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637892,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode ="MHGCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 109M
            },
            //15
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638101,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode ="MADCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 109M
            },
            //16
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638012,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode ="BUECR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 106M
            },
            //17
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638005,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode ="BUDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 79M
            },
            //18
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637977,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode ="WAWCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 79M
            },
            //19
            new BidRate()
            {
                AppliedRate = 55.09M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637590,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "SYDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 35M
            },
            //20
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637691,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "LHRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //21
            new BidRate()
            {
                AppliedRate = 107.99M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637863,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 91.79M,
                    CurrencyCode = "USD",
                    ExchangeRate = 1M,
                    InflationRate = 1.0188M
                },
                BufCode = "BUECR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1M,
                InflationRate = 1.0188M,
                Rate = 106M
            },
            //22
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637465,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode = "FLRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //23
            new BidRate()
            {
                AppliedRate = 107.99M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637758,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 91.79M,
                    CurrencyCode = "USD",
                    ExchangeRate = 1,
                    InflationRate = 1.0188M
                },
                BufCode = "SAOCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1M,
                InflationRate = 1.0188M,
                Rate = 106M
            },
            //24
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637501,
                BidRateOverride = new BidRateOverride()
                {
                    BidRateOverrideId = -1
                },
                BufCode = "LHRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //25
            new BidRate()
            {
                AppliedRate = 44.07M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637433,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "BUECR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1M,
                InflationRate = 1.0494M,
                Rate = 42M
            },
            //26
            new BidRate()
            {
                AppliedRate = 120.73M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637856,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 102.62M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "BUDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 79M
            },
            //27
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637920,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "ROUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 79M
            },
            //28
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637518,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "MADCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //29
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637418,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "BRUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //30
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637848,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "BRUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //31
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638044,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "FLRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 109M
            },
            //32
            new BidRate()
            {
                AppliedRate = 142.63M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637793,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 121.24M,
                    CurrencyCode = "USD",
                    ExchangeRate = 1M,
                    InflationRate = 1.0188M
                },
                BufCode = "USMCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 1M,
                InflationRate = 1.0188M,
                Rate = 140M
            },
            //33
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637951,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "SYDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 79M
            },
            //34
            new BidRate()
            {
                AppliedRate = 55.09M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637559,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "ROUCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 35M
            },
            //35
            new BidRate()
            {
                AppliedRate = 77.13M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637545,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "NLDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 49M
            },
            //36
            new BidRate()
            {
                AppliedRate = 166.57M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637720,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 141.59M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "MHGCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 109M
            },
            //37
            new BidRate()
            {
                AppliedRate = 55.09M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637426,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "BUDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 35M
            },
            //38
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638055,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "ISRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 79M
            },
            //39
            new BidRate()
            {
                AppliedRate = 120.73M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637661,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 102.62M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "ISRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 79M
            },
            //40
            new BidRate()
            {
                AppliedRate = 55.09M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637617,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "WAWCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 35M
            },      
            //41
            new BidRate()
            {
                AppliedRate = 120.73M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637779,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 102.62M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "SYDCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 79M
            },  
            //42
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637930,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "SAOCR",
                CurrencyCode = "USD",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 106M
            },  
            //43
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5637910,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "PRSCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 109M
            },  
            //44
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638085,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "LHRCR",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1M,
                Rate = 109M
            },  
            //0
            new BidRate()
            {
                AppliedRate = 152.82M,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149713,
                BidRateId = 5637640,
                BidRateOverride = new BidRateOverride()
                {
                    AppliedRate = 129.9M,
                    CurrencyCode = "EUR",
                    ExchangeRate = 1.5M,
                    InflationRate = 1.0188M
                },
                BufCode = "EUSDS",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Rate = 100M
            },  
            //1
            new BidRate()
            {
                AppliedRate = 61.39M,
                BidRateCard = BidRateCards.CostBidRateCard,
                BidRateCardId = 149712,
                BidRateId = 5637455,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "EUSDS",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 1.5M,
                InflationRate = 1.0494M,
                Rate = 39M
            },
            //2
            new BidRate()
            {
                AppliedRate = 0,
                BidRateCard = BidRateCards.PriceBidRateCard,
                BidRateCardId = 149714,
                BidRateId = 5638034,
                BidRateOverride = new BidRateOverride(){
                    BidRateOverrideId = -1
                },
                BufCode = "EUSDS",
                CurrencyCode = "EUR",
                DiscountPercent = 0,
                ExchangeRate = 0,
                InflationRate = 1,
                Rate = 100M
            },
        };
    }
}
