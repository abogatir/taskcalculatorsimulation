﻿using System.Collections.Generic;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidWorkAllocations
    {
        public static List<BidWorkAllocation> BidWorkAllocationsList = new List<BidWorkAllocation>()
        {
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172157,
                BuCode = "EUS",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = -1,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "DS",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_LOCATION_AND_PERCENTAGE
            },
            //0
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172092,
                BuCode = "USM",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 2,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION
            },
            //1
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171700,
                BuCode = "PRS",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 7,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //2
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171711,
                BuCode = "FLR",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 10,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //3
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171710,
                BuCode = "ISR",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 21,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //4
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172079,
                BuCode = "ROU",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 35,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //5
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172021,
                BuCode = "BUE",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 49,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //6
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171704,
                BuCode = "MHG",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 8,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //7
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171699,
                BuCode = "SAO",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 13,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //8
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172074,
                BuCode = "SYD",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 48,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //9
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172019,
                BuCode = "BRU",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 6,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //10
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172076,
                BuCode = "WAW",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 11,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //11
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172084,
                BuCode = "MAD",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 12,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //12
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172073,
                BuCode = "NLD",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 26,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //13
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172089,
                BuCode = "LHR",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 3,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //14
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171707,
                BuCode = "BUD",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 9,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //15
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4172164,
                BuCode = "BUE",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 14,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            },
            //16
            new BidWorkAllocation()
            {
                BidScenarioId = 291556,
                BidWorkAllocationId = 4171628,
                BuCode = "SYD",
                BusinessUnit = new BusinessUnit(),
                Country = new Country(),
                CountryId = 19,
                FunctionCode = new FunctionCode(),
                NatFunctionCode = "CR",
                Percentage = 1,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION                
            }
        };
    }
}
