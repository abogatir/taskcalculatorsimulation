﻿using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidCurrencyExchangeRates
    {
        public static BidCurrencyExchange SetInflationExchangeRateAndDiscountExchangeRate = new BidCurrencyExchange()
        {
            ExchangeRate = 1,
            FromCurrencyCode = "USD",
            ToCurrencyCode = "USD"
        };
    }
}
