﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class Countries
    {
        public static List<Country> CountriesList = new List<Country>()
        {
            new Country()
            {
                CountryId = 69,
                Description = "PHILIPPINES"
            },
            new Country()
            {
                CountryId = 141,
                Description = "LUXEMBOURG"
            },
            new Country()
            {
                CountryId = 66,
                Description = "MACEDONIA"
            },
            new Country()
            {
                CountryId = 64,
                Description = "DOMINICAN REPUBLIC"
            },
            new Country()
            {
                CountryId = 65,
                Description = "GEORGIA"
            },
            new Country()
            {
                CountryId = 57,
                Description = "ALBANIA"
            },
            new Country()
            {
                CountryId = 14,
                Description = "ARGENTINA"
            },
            new Country()
            {
                CountryId = 19,
                Description = "AUSTRALIA"
            },
            new Country()
            {
                CountryId = 38,
                Description = "AUSTRIA"
            },
            new Country()
            {
                CountryId = 39,
                Description = "BELARUS"
            },
            new Country()
            {
                CountryId = 6,
                Description = "BELGIUM"
            },
            new Country()
            {
                CountryId = 13,
                Description = "BRAZIL"
            },
            new Country()
            {
                CountryId = 30,
                Description = "BULGARIA"
            },
            new Country()
            {
                CountryId = 2,
                Description = "CANADA"
            },
            new Country()
            {
                CountryId = 40,
                Description = "CHILE"
            },
            new Country()
            {
                CountryId = 18,
                Description = "CHINA"
            },
            new Country()
            {
                CountryId = 62,
                Description = "COLOMBIA"
            },
            new Country()
            {
                CountryId = 41,
                Description = "COSTA RICA"
            },
            new Country()
            {
                CountryId = 34,
                Description = "CROATIA"
            },
            new Country()
            {
                CountryId = 24,
                Description = "CZECH REPUBLIC"
            },
            new Country()
            {
                CountryId = 36,
                Description = "DENMARK"
            },
            new Country()
            {
                CountryId = 42,
                Description = "ESTONIA"
            },
            new Country()
            {
                CountryId = 32,
                Description = "FINLAND"
            },
            new Country()
            {
                CountryId = 7,
                Description = "FRANCE"
            },
            new Country()
            {
                CountryId = 8,
                Description = "GERMANY"
            },
            new Country()
            {
                CountryId = 37,
                Description = "GREECE"
            },
            new Country()
            {
                CountryId = 43,
                Description = "GUATEMALA"
            },
            new Country()
            {
                CountryId = 17,
                Description = "HONG KONG"
            },
            new Country()
            {
                CountryId = 9,
                Description = "HUNGARY"
            }
            ,
            new Country()
            {
                CountryId = 5,
                Description = "INDIA"
            },
            new Country()
            {
                CountryId = 44,
                Description = "IRELAND"
            },
            new Country()
            {
                CountryId = 21,
                Description = "ISRAEL"
            },
            new Country()
            {
                CountryId = 10,
                Description = "ITALY"
            },
            new Country()
            {
                CountryId = 28,
                Description = "JAPAN"
            },
            new Country()
            {
                CountryId = 45,
                Description = "KAZAKHSTAN"
            },
            new Country()
            {
                CountryId = 58,
                Description = "LATVIA"
            },
            new Country()
            {
                CountryId = 59,
                Description = "LITHUANIA"
            },
            new Country()
            {
                CountryId = 63,
                Description = "MALAYSIA"
            },
            new Country()
            {
                CountryId = 4,
                Description = "MEXICO"
            },
            new Country()
            {
                CountryId = 46,
                Description = "MOLDOVA"
            },
            new Country()
            {
                CountryId = 47,
                Description = "MONTENEGRO"
            },
            new Country()
            {
                CountryId = 26,
                Description = "NETHERLANDS"
            },
            new Country()
            {
                CountryId = 48,
                Description = "NEW ZEALAND"
            },
            new Country()
            {
                CountryId = 31,
                Description = "NORWAY"
            },
            new Country()
            {
                CountryId = 29,
                Description = "PANAMA"
            },
            new Country()
            {
                CountryId = 49,
                Description = "PERU"
            },
            new Country()
            {
                CountryId = 11,
                Description = "POLAND"
            },
            new Country()
            {
                CountryId = 50,
                Description = "PORTUGAL"
            },
            new Country()
            {
                CountryId = 35,
                Description = "ROMANIA"
            },
            new Country()
            {
                CountryId = 22,
                Description = "RUSSIA"
            },
            new Country()
            {
                CountryId = 52,
                Description = "SERBIA"
            },
            new Country()
            {
                CountryId = 20,
                Description = "SINGAPORE"
            },
            new Country()
            {
                CountryId = 25,
                Description = "SLOVAKIA"
            },
            new Country()
            {
                CountryId = 53,
                Description = "SLOVENIA"
            },
            new Country()
            {
                CountryId = 16,
                Description = "SOUTH AFRICA"
            },
            new Country()
            {
                CountryId = 54,
                Description = "SOUTH KOREA"
            },
            new Country()
            {
                CountryId = 12,
                Description = "SPAIN"
            },
            new Country()
            {
                CountryId = 27,
                Description = "SWEDEN"
            },
            new Country()
            {
                CountryId = 55,
                Description = "SWITZERLAND"
            },
            new Country()
            {
                CountryId = 15,
                Description = "TAIWAN"
            },
            new Country()
            {
                CountryId = 56,
                Description = "TURKEY"
            },
            new Country()
            {
                CountryId = 23,
                Description = "UKRAINE"
            },
            new Country()
            {
                CountryId = 23,
                Description = "UKRAINE"
            },
            new Country()
            {
                CountryId = 3,
                Description = "UNITED KINGDOM"
            },
            new Country()
            {
                CountryId = 1,
                Description = "UNITED STATES"
            }
        };
    }
}
