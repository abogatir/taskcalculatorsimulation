﻿using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidValueParameters
    {
        public static BidValueParameter BidPriceDriverBidValueParameter = new BidValueParameter()
        {
            Active = true,
            
            Equation =
                "SumCountryMonths('ONE_DAY_VISITS', MonthNumber(TimeParam('FIRST_PATIENT_IN')), MonthNumber(TimeParam('LAST_PATIENT_OUT')))",            
        };
    }
}
