﻿using System.Collections.Generic;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidPriceDriverWorkAllocations
    {
        
        public static List<BidPriceDriverWorkAllocation> BidPriceDriverWorkAllocationsList = new List<BidPriceDriverWorkAllocation>()
        {
            //CR
            //0
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 121.24M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802357,
                BuCode = "USM",
                CurrencyCode = "USD",
                DiscountPercent = 0.15M,
                ExchangeRate = 1,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 140M,
                TotalEffort = 216M,
                TotalPrice = 26187.84M
            },
            //1
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802358,
                BuCode = "PRS",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109,
                TotalEffort = 204,
                TotalPrice = 28884.36M
            },
            //2
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802359,
                BuCode = "FLR",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109,
                TotalEffort = 162,
                TotalPrice = 22937.58M
            },
            //3
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 102.62M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802360,
                BuCode = "ISR",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 79,
                TotalEffort = 162,
                TotalPrice = 16624.44M
            },
            //4
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 102.62M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802361,
                BuCode = "ROU",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 79,
                TotalEffort = 204,
                TotalPrice = 20934.48M
            },
            //5
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 91.79M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802362,
                BuCode = "BUE",
                CurrencyCode = "USD",
                DiscountPercent = 0.15M,
                ExchangeRate = 1,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 106,
                TotalEffort = 474M,
                TotalPrice = 43508.46M
            },
            //6
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802363,
                BuCode = "MHG",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109M,
                TotalEffort = 246M,
                TotalPrice = 34831.14M
            },
            //7
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 91.79M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802364,
                BuCode = "SAO",
                CurrencyCode = "USD",
                DiscountPercent = 0.15M,
                ExchangeRate = 1,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 106,
                TotalEffort = 504,
                TotalPrice = 46262.16M
            },
            //8
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 102.62M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802365,
                BuCode = "SYD",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 79M,
                TotalEffort = 408M,
                TotalPrice = 41868.96M
            },
            //9
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802366,
                BuCode = "BRU",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109M,
                TotalEffort = 102M,
                TotalPrice = 14442.18M
            },
            //10
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 102.62M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802367,
                BuCode = "WAW",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 79M,
                TotalEffort = 324M,
                TotalPrice = 33248.88M
            },
            //11
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802368,
                BuCode = "MAD",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109,
                TotalEffort = 168M,
                TotalPrice = 23787.12M
            },
            //12
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802369,
                BuCode = "NLD",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109M,
                TotalEffort = 60M,
                TotalPrice = 8495.4M
            },
            //13
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 141.59M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802370,
                BuCode = "LHR",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 109M,
                TotalEffort = 144M,
                TotalPrice = 20388.96M
            },
            //14
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 102.62M,
                BidPriceDriverId = 20286741,
                BidPriceDriverWorkAllocationId = 140802371,
                BuCode = "BUD",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 79M,
                TotalEffort = 204M,
                TotalPrice = 20934.48M
            },
            //DS
            //0
            new BidPriceDriverWorkAllocation()
            {
                AppliedRate = 129.9M,
                BidPriceDriverId = 72143388,
                BidPriceDriverWorkAllocationId = 140802372,
                BuCode = "EUS",
                CurrencyCode = "EUR",
                DiscountPercent = 0.15M,
                ExchangeRate = 1.5M,
                InflationRate = 1.0188M,
                Percentage = 1,
                Rate = 100M,
                TotalEffort = 100M,
                TotalPrice = 12990M
            }
        };
    }
}
