﻿using System.Collections.Generic;
using TaskCalculation.Core.Classes;
using System.Linq;
using System.Collections.Generic;

namespace TaskCalculation.Core.Mocks
{
    public static class BidParameterCountries
    {
        public static List<BidParameterCountry> mCuntryInputCountrySpecificUnitParameters;

        private static void Initialize()
        {
            mCuntryInputCountrySpecificUnitParameters = new List<BidParameterCountry>() {
            new BidParameterCountry()
            {
                CountryId = 14,
                Value = 56
            },
            //1
            new BidParameterCountry()
            {
               CountryId = 19,
               Value = 48
            },
            //2,
            new BidParameterCountry()
            {
                CountryId = 6,
                Value = 17
            },
            //3,
            new BidParameterCountry()
            {
                CountryId = 13,
                Value = 84
            },
            //4
            new BidParameterCountry()
            {
                CountryId = 2,
                Value = 36
            },
            //5
            new BidParameterCountry()
            {
                CountryId = 7,
                Value = 34
            },
            //6
            new BidParameterCountry()
            {
                CountryId = 8,
                Value = 41
            },
            //7
            new BidParameterCountry()
            {
                CountryId = 9,
                Value = 34
            },
            //8
            new BidParameterCountry()
            {
                CountryId = 21,
                Value = 27
            },//9
            new BidParameterCountry()
            {
                CountryId = 10,
                Value = 27
            },
            //10
            new BidParameterCountry()
            {
                CountryId = 26,
                Value = 10
            },
            //11
            new BidParameterCountry()
            {
                CountryId = 48,
                Value = 20
            },
            //12
            new BidParameterCountry()
            {
                CountryId = 49,
                Value = 23
            },
            //13
            new BidParameterCountry()
            {
                CountryId = 11,
                Value = 54
            },
            //14
            new BidParameterCountry()
            {
                CountryId = 35,
                Value = 34
            },
            //15
            new BidParameterCountry()
            {
                CountryId = 12,
                Value = 28
            },
            //16
            new BidParameterCountry()
            {
                CountryId = 3,
                Value = 24
            }
            };
            OneDayVisits.AttachToCountries(291556, mCuntryInputCountrySpecificUnitParameters);
        }
        public static List<BidParameterCountry> CuntryInputCountrySpecificUnitParameters
        {
            get
            {
                if (mCuntryInputCountrySpecificUnitParameters == null)
                    Initialize();
                return mCuntryInputCountrySpecificUnitParameters;
            }
        }
    }
}
