﻿using System.Collections.Generic;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidPriceDriverCountries
    {
        private static List<BidPriceDriverCountry> mListBidPriceDriverCountries;
        public static List<BidPriceDriverCountry> ListBidPriceDriverCountries
        {
            get
            {
                mListBidPriceDriverCountries = Initialize();
                return mListBidPriceDriverCountries;
            }
        }
        private static List<BidPriceDriverCountry> Initialize()
        {
            List<BidPriceDriverCountry> pdc = new List<BidPriceDriverCountry>()
        {
            //20286741
            //0
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234847,
                BidPriceDriverId = 20286741,
                CountryId = 14,
                CountryParameterValue = 56M,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 336,
                TotalPriceOutput = 0,
                TotalUnits = 56
            },
            //1
            new BidPriceDriverCountry()
            {
               BidPriceDriverCountryId = 162234852,
               BidPriceDriverId = 20286741,
               CountryId = 19,
               CountryParameterValue = 48M,
               ExchangeRate = 0,
               InflationRate = 0,
               MasterCostPerUnit = 0,
               MasterCurrencyCode = "",
               MasterEffortPerUnit = 6,
               MasterPriceDriverId = 125034,
               MasterPricePerUnit = 0,
               TotalCostOutput = 0,
               TotalEffortInHours = 288,
               TotalPriceOutput = 0,
               TotalUnits = 48
            },
            //2,
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234839,
                BidPriceDriverId = 20286741,
                CountryId = 6,
                CountryParameterValue = 17,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 102,
                TotalPriceOutput = 0,
                TotalUnits = 17
            },
            //3,
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234846,
                BidPriceDriverId = 20286741,
                CountryId = 13,
                CountryParameterValue = 84M,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 504,
                TotalPriceOutput = 0,
                TotalUnits = 84
            },
            //4
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234835,
                BidPriceDriverId = 20286741,
                CountryId = 2,
                CountryParameterValue = 36,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 216,
                TotalPriceOutput = 0,
                TotalUnits = 36
            },
            //5
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234840,
                BidPriceDriverId = 20286741,
                CountryId = 7,
                CountryParameterValue = 34,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 204,
                TotalPriceOutput = 0,
                TotalUnits = 34
            },
            //6
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234841,
                BidPriceDriverId = 20286741,
                CountryId = 8,
                CountryParameterValue = 41,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 246,
                TotalPriceOutput = 0,
                TotalUnits = 41
            },
            //7
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234842,
                BidPriceDriverId = 20286741,
                CountryId = 9,
                CountryParameterValue = 34,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 204,
                TotalPriceOutput = 0,
                TotalUnits = 34
            },
            //8
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234854,
                BidPriceDriverId = 20286741,
                CountryId = 21,
                CountryParameterValue = 27,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 162,
                TotalPriceOutput = 0,
                TotalUnits = 27
            },//9
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234843,
                BidPriceDriverId = 20286741,
                CountryId = 10,
                CountryParameterValue = 27,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 162,
                TotalPriceOutput = 0,
                TotalUnits = 27                
            },
            //10
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234859,
                BidPriceDriverId = 20286741,
                CountryId = 26,
                CountryParameterValue = 10,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 60,
                TotalPriceOutput = 0,
                TotalUnits = 10
            },
            //11
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234881,
                BidPriceDriverId = 20286741,
                CountryId = 48,
                CountryParameterValue = 20,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 120,
                TotalPriceOutput = 0,
                TotalUnits = 20
            },
            //12
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234882,
                BidPriceDriverId = 20286741,
                CountryId = 49,
                CountryParameterValue = 23,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 138,
                TotalPriceOutput = 0,
                TotalUnits = 23
            },
            //13
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234844,
                BidPriceDriverId = 20286741,
                CountryId = 11,
                CountryParameterValue = 54,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 324,
                TotalPriceOutput = 0,
                TotalUnits = 54                
            },
            //14
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234868,
                BidPriceDriverId = 20286741,
                CountryId = 35,
                CountryParameterValue = 34,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 204,
                TotalPriceOutput = 0,
                TotalUnits = 34                
            },
            //15
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234845,
                BidPriceDriverId = 20286741,
                CountryId = 12,
                CountryParameterValue = 28,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 168,
                TotalPriceOutput = 0,
                TotalUnits = 28                    
            },
            //16
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234836,
                BidPriceDriverId = 20286741,
                CountryId = 3,
                CountryParameterValue = 24,
                ExchangeRate = 0,
                InflationRate = 0,
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "",
                MasterEffortPerUnit = 6,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                TotalCostOutput = 0,
                TotalEffortInHours = 144,
                TotalPriceOutput = 0,
                TotalUnits = 24                  
            },
            //20286743
            //0
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234987,
                BidPriceDriverId = 20286743,
                CountryId = 14,
                CountryParameterValue = 56M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 8960M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 8960M,
                TotalUnits = 56
            },
            //1
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234992,
                BidPriceDriverId = 20286743,
                CountryId = 19,
                CountryParameterValue = 48M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 7680M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 7680M,
                TotalUnits = 48M
            },
            //2
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234979,
                BidPriceDriverId = 20286743,
                CountryId = 6,
                CountryParameterValue = 17M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 2720M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 2720M,
                TotalUnits = 17M
            },
            //3
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234986,
                BidPriceDriverId = 20286743,
                CountryId = 13,
                CountryParameterValue = 84M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 13440M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 13440M,
                TotalUnits = 84M
            },
            //4
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234975,
                BidPriceDriverId = 20286743,
                CountryId = 2,
                CountryParameterValue = 36M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 200M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 200M,
                TotalCostOutput = 7200M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 7200M,
                TotalUnits = 36M
            },
            //5
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234980,
                BidPriceDriverId = 20286743,
                CountryId = 7,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 5440M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 5440M,
                TotalUnits = 34M
            },
            //6
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234981,
                BidPriceDriverId = 20286743,
                CountryId = 8,
                CountryParameterValue = 41M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 6560M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 6560M,
                TotalUnits = 41M
            },
            //7
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234982,
                BidPriceDriverId = 20286743,
                CountryId = 9,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 5440M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 5440M,
                TotalUnits = 34M
            },
            //8
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234994,
                BidPriceDriverId = 20286743,
                CountryId = 21,
                CountryParameterValue = 27M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 4320M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 4320M,
                TotalUnits = 27M
            },
            //9
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234983,
                BidPriceDriverId = 20286743,
                CountryId = 10,
                CountryParameterValue = 27M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 4320M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 4320M,
                TotalUnits = 27M
            },
            //10
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234999,
                BidPriceDriverId = 20286743,
                CountryId = 26,
                CountryParameterValue = 10M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 1600M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 1600M,
                TotalUnits = 10
            },
            //11
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162235021,
                BidPriceDriverId = 20286743,
                CountryId = 48,
                CountryParameterValue = 20M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 3200M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 3200M,
                TotalUnits = 20
            },
            //12
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162235022,
                BidPriceDriverId = 20286743,
                CountryId = 49,
                CountryParameterValue = 23M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 3680M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 3680M,
                TotalUnits = 23
            },
            //13
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234984,
                BidPriceDriverId = 20286743,
                CountryId = 11,
                CountryParameterValue = 54M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 8640M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 8640M,
                TotalUnits = 54M
            },
            //14
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162235008,
                BidPriceDriverId = 20286743,
                CountryId = 35,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 5440M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 5440M,
                TotalUnits = 34M
            },
            //15
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234985,
                BidPriceDriverId = 20286743,
                CountryId = 12,
                CountryParameterValue = 28M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 4480M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 4480M,
                TotalUnits = 28M
            },
            //16
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234976,
                BidPriceDriverId = 20286743,
                CountryId = 3,
                CountryParameterValue = 24M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 160M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 160M,
                TotalCostOutput = 3840M,
                TotalEffortInHours = 0M,
                TotalPriceOutput = 3840M,
                TotalUnits = 24M
            },
            //20286742
            //0
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234917,
                BidPriceDriverId = 20286742,
                CountryId = 14,
                CountryParameterValue = 56M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 2800M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 2800M,
                TotalUnits = 56
            },
            //1
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234922,
                BidPriceDriverId = 20286742,
                CountryId = 19,
                CountryParameterValue = 48M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 2400M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 2400M,
                TotalUnits = 48
            },
            //2
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234909,
                BidPriceDriverId = 20286742,
                CountryId = 6,
                CountryParameterValue = 17M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 850M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 850M,
                TotalUnits = 17M
            },
            //3
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234916,
                BidPriceDriverId = 20286742,
                CountryId = 13,
                CountryParameterValue = 84M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 4200M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 4200M,
                TotalUnits = 84M
            },
            //4
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234905,
                BidPriceDriverId = 20286742,
                CountryId = 2,
                CountryParameterValue = 36M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1800M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1800M,
                TotalUnits = 36M
            },
            //5
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234910,
                BidPriceDriverId = 20286742,
                CountryId = 7,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1700M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1700M,
                TotalUnits = 34M
            },
            //6
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234911,
                BidPriceDriverId = 20286742,
                CountryId = 8,
                CountryParameterValue = 41M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 2050M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 2050M,
                TotalUnits = 41M
            },
            //7
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234912,
                BidPriceDriverId = 20286742,
                CountryId = 9,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1700M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1700M,
                TotalUnits = 34M
            },
            //8
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234924,
                BidPriceDriverId = 20286742,
                CountryId = 21,
                CountryParameterValue = 27M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1350M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1350M,
                TotalUnits = 27M
            },
            //9
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234913,
                BidPriceDriverId = 20286742,
                CountryId = 10,
                CountryParameterValue = 27M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1350M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1350M,
                TotalUnits = 27M
            },
            //10
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234929,
                BidPriceDriverId = 20286742,
                CountryId = 26,
                CountryParameterValue = 10M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 500M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 500M,
                TotalUnits = 10M
            },
            //11
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234951,
                BidPriceDriverId = 20286742,
                CountryId = 48,
                CountryParameterValue = 20M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1000M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1000M,
                TotalUnits = 20M
            },
            //12
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234952,
                BidPriceDriverId = 20286742,
                CountryId = 49,
                CountryParameterValue = 23M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1150M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1150M,
                TotalUnits = 23M
            },
            //13
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234914,
                BidPriceDriverId = 20286742,
                CountryId = 11,
                CountryParameterValue = 54M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 2700M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 2700M,
                TotalUnits = 54M
            },
            //14
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234938,
                BidPriceDriverId = 20286742,
                CountryId = 35,
                CountryParameterValue = 34M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1700M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1700M,
                TotalUnits = 34M
            },
            //15
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234915,
                BidPriceDriverId = 20286742,
                CountryId = 12,
                CountryParameterValue = 28M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1400M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1400M,
                TotalUnits = 28M
            },
            //16
            new BidPriceDriverCountry()
            {
                BidPriceDriverCountryId = 162234906,
                BidPriceDriverId = 20286742,
                CountryId = 3,
                CountryParameterValue = 24M,
                ExchangeRate = 1,
                InflationRate = 1,
                MasterCostPerUnit = 50M,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 50M,
                TotalCostOutput = 1200M,
                TotalEffortInHours = 0,
                TotalPriceOutput = 1200M,
                TotalUnits = 24M
            },
        };
            return pdc;
        }

    }
}




