﻿using System;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidRateCards
    {
        public static BidRateCard PriceBidRateCard = new BidRateCard()
        {
            Active = false,
            BidRateCardId = 149713,
            BidScenarioId = 291556,
            Description = "PRA Standard Rates",
            Employee = 11187,
            ExpirationDate = new DateTime(2100,12,31),
            FeeDiscountPercent = 0,
            InflationStartDate = new DateTime(2010,1,1),
            MasterRateCardId = 106503,
            Notes = "To be entered by Stefan",
            OverrideDate = new DateTime(2010,11,3),
            OverrideReason = "v8:  per Linda B:  applying 100k discount to site management (in rates), on top of 15% SA rate discount - SA request to round rates to whole dollar- done manually in the grid (03Nov 2010 - ",
            RateCardType = DBProperties.enuRateCardType.ACTIVE_BILLING_RATE_CARD,
            StartDate = new DateTime(2009,6,3)
        };
        public static BidRateCard CostBidRateCard = new BidRateCard()
        {
            Active = false,
            BidRateCardId = 149712,
            BidScenarioId = 291556,
            Description = "PRA Standard Cost",
            Employee = 0,
            ExpirationDate = new DateTime(2100,1,1),
            FeeDiscountPercent = 0,
            InflationStartDate = new DateTime(2010,1,1),
            MasterRateCardId = 111390,
            Notes = "",
            OverrideReason = "",
            RateCardType = DBProperties.enuRateCardType.STANDARD_COST_RATE_CARD,
            StartDate = new DateTime(2009,1,6)
        };
    }
}
