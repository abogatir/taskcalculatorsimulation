﻿using System.Collections.Generic;
using System.Linq;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public static class BidPriceDrivers
    {
        public static List<BidPriceDriver> BidPriceDriversList = new List<BidPriceDriver>
        {
            new BidPriceDriver()
            {
                AutoSelectMethodology = DBProperties.enuAutoSelectMethodology.ON,
                BidInstructions = "",
                BidPriceDriverCountries = BidPriceDriverCountries.ListBidPriceDriverCountries.Where(x=>x.BidPriceDriverId==20286741).ToList(),
                BidPriceDriverFTEs = new List<BidPriceDriverFte>(),
                BidTaskId = 6403721,
                ClientDescription = "",
                BidPriceDriverId = 20286741,
                BidPriceDriverOverride = new BidPriceDriverOverride(),
                ConditionalOnStatement = "",
                CountryParameter = new BidValueParameter(),
                CountryParameterId = 5333958,
                Description = "CR",
                EfficiencyFactor = 0,
                EfficiencyParameterId = -1,
                ExchangeRate = 0,
                FrequencyRoundingFunction = DBProperties.enuRoundingFunction.CEILING,
                FrequencyRoundingFunctionId = 0,
                FrequencyValue = 1,
                FunctionCode = "CR",
                FunctionCodeData = new FunctionCode(),
                InflationRate = 0,
                LongDescription = "",
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterFrequencyPeriod = 0,
                MasterFrequencyType = DBProperties.enuFrequencyType.DURATION_OF_STUDY,
                MasterFrequencyTypeId = 0,
                MasterPriceDriverId = 125034,
                MasterPricePerUnit = 0,
                MasterTaskId = 138441,
                PriceDriverType = DBProperties.enuPriceDriverType.SERVICE_HOURLY,
                Selected = true,
                TotalCostOutput = 0,
                TotalEffortInHours = 0,
                TotalPriceOutput = 0,
                TotalUnits = 0,
                UniqueName = "CR",
                UnitParameter = null,
                UnitParameterId = -1,
                UnitParameterValue = 0,
                UsesCountrySpecificPerUnit = true,
                UsesHeadcount = false,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_AND_SOURCE_LOCATION
            },
            new BidPriceDriver()
            {
                AutoSelectMethodology = DBProperties.enuAutoSelectMethodology.OFF,
                BidInstructions = "",
                BidPriceDriverCountries = new List<BidPriceDriverCountry>(),
                BidPriceDriverFTEs = new List<BidPriceDriverFte>(),
                BidPriceDriverId = 72143388,
                BidPriceDriverOverride = new BidPriceDriverOverride(),
                BidTaskId = 6403721,
                ClientDescription = "",
                ConditionalOnStatement = "",
                CountryParameter = null,
                CountryParameterId = -1,
                Description = "Drug Safety Associate",
                EfficiencyFactor = 0,
                EfficiencyParameterId = -1,
                ExchangeRate = 0,
                FrequencyRoundingFunction = DBProperties.enuRoundingFunction.CEILING,
                FrequencyRoundingFunctionId = 0,
                FrequencyValue = 1,
                FunctionCode = "DS",
                FunctionCodeData = new FunctionCode(),
                InflationRate = 0,
                LongDescription = "",
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "ARS",
                MasterEffortPerUnit = 100,
                MasterFrequencyPeriod = 0,
                MasterFrequencyType = DBProperties.enuFrequencyType.DURATION_OF_STUDY,
                MasterFrequencyTypeId = 0,
                MasterPriceDriverId = -1,
                MasterPricePerUnit = 0,
                MasterTaskId = 138441,
                PriceDriverType = DBProperties.enuPriceDriverType.SERVICE_HOURLY,
                Selected = true,
                TotalCostOutput = 0,
                TotalEffortInHours = 100,
                TotalPriceOutput = 0,
                TotalUnits = 1,
                UniqueName = "DS",
                UnitParameter = null,
                UnitParameterId = -1,
                UnitParameterValue = 1,
                UsesCountrySpecificPerUnit = false,
                UsesHeadcount =  false,
                WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_LOCATION_AND_PERCENTAGE
            },
            new BidPriceDriver()
            {
                AutoSelectMethodology = DBProperties.enuAutoSelectMethodology.ON,
                BidInstructions = "",
                BidPriceDriverCountries =BidPriceDriverCountries.ListBidPriceDriverCountries.Where(x=>x.BidPriceDriverId==20286743).ToList(),
                BidPriceDriverFTEs = new List<BidPriceDriverFte>(),
                BidPriceDriverId = 20286743,
                BidPriceDriverOverride = new BidPriceDriverOverride(),
                BidTaskId = 6403721,
                ClientDescription = "",
                ConditionalOnStatement = "",
                CountryParameter = new BidValueParameter(),
                CountryParameterId = 5333958,
                Description = "Lodging Expense",
                EfficiencyFactor = 0,
                EfficiencyParameterId = -1,
                FrequencyRoundingFunction = DBProperties.enuRoundingFunction.CEILING,
                FrequencyRoundingFunctionId = 0,
                ExchangeRate = 0,
                FrequencyValue = 1,
                FunctionCode = "",
                FunctionCodeData = new FunctionCode(),
                InflationRate = 0,
                LongDescription = "",
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterFrequencyPeriod = 0,
                MasterFrequencyType = DBProperties.enuFrequencyType.DURATION_OF_STUDY,
                MasterFrequencyTypeId = 0,
                MasterPriceDriverId = 125035,
                MasterPricePerUnit = 0,
                MasterTaskId = 138441,
                PriceDriverType = DBProperties.enuPriceDriverType.EXPENSE,
                Selected = true,
                TotalCostOutput = 0,
                TotalEffortInHours = 0,
                TotalPriceOutput = 0,
                TotalUnits = 0,
                UniqueName = "LODGING_EXPENSE_1.0-DAY_IMVS",
                UnitParameter = null,
                UnitParameterId = -1,
                UnitParameterValue = 0,
                UsesCountrySpecificPerUnit = true,
                UsesHeadcount = false                             
            },
            new BidPriceDriver()
            {
                AutoSelectMethodology = DBProperties.enuAutoSelectMethodology.ON,
                BidInstructions = "",
                BidPriceDriverCountries =  BidPriceDriverCountries.ListBidPriceDriverCountries.Where(x=>x.BidPriceDriverId==20286742).ToList(),
                BidPriceDriverFTEs = new List<BidPriceDriverFte>(),
                BidPriceDriverId = 20286742,
                BidPriceDriverOverride = new BidPriceDriverOverride(),
                BidTaskId = 6403721,
                ClientDescription = "",
                ConditionalOnStatement = "",
                CountryParameter = new BidValueParameter(),
                CountryParameterId = 5333958,
                Description = "Meals Expense",
                EfficiencyFactor = 0,
                EfficiencyParameterId = -1,
                ExchangeRate = 0,
                FrequencyRoundingFunction = DBProperties.enuRoundingFunction.CEILING,
                FrequencyRoundingFunctionId = 0,
                FrequencyValue = 1,
                FunctionCode = "",
                FunctionCodeData = new FunctionCode(),
                InflationRate = 0,
                LongDescription = "",
                MasterCostPerUnit = 0,
                MasterCurrencyCode = "USD",
                MasterEffortPerUnit = 0,
                MasterFrequencyPeriod = 0,
                MasterFrequencyType = DBProperties.enuFrequencyType.DURATION_OF_STUDY,
                MasterFrequencyTypeId = 0,
                MasterPriceDriverId = 125036,
                MasterPricePerUnit = 0,
                MasterTaskId = 138441,
                PriceDriverType = DBProperties.enuPriceDriverType.EXPENSE,
                Selected = true,
                TotalCostOutput = 0,
                TotalEffortInHours = 0,
                TotalPriceOutput = 0,
                TotalUnits = 0,
                UniqueName = "MEALS_EXPENSE_1.0-DAY_IMVS",
                UnitParameter = null,
                UnitParameterId = -1,
                UnitParameterValue = 0,
                UsesCountrySpecificPerUnit = true,
                UsesHeadcount = false                
            }

        };
    }
}
