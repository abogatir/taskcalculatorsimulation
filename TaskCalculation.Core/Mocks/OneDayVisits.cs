﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskCalculation.Core.Classes;

namespace TaskCalculation.Core.Mocks
{
    public class OneDayVisits
    {
        private static Dictionary<int, List<int>> InitData;

        public static void InitializeData()
        {
            InitData = new Dictionary<int, List<int>>();
            InitData.Add(7, new List<int>() { 0, 0, 0, 0, 5, 7, 8, 7, 7, 7, 7, 7, 8, 7, 7, 4, 5, 4, 4, 4, 2 });
            InitData.Add(2, new List<int>() { 0, 0, 0, 0, 2, 3, 4, 4, 4, 5, 4, 4, 5, 4, 4, 3, 2, 3, 3, 2, 1 });
            InitData.Add(13, new List<int>() { 0, 0, 0, 0, 0, 0, 0, 2, 10, 11, 11, 11, 10, 11, 11, 5, 7, 6, 7, 6, 3 });
        }
        public static List<BidParameterCountryMonth> GetList(int bidScenarioId)
        {
            List<BidParameterCountryMonth> list = new List<BidParameterCountryMonth>();
            if (bidScenarioId != 291556)
            {
                throw new ApplicationException("No Country Month Parameters available for bid scenario " + bidScenarioId);
            }
            InitializeData();
            List<BidParameterCountry> countries = BidParameterCountries.CuntryInputCountrySpecificUnitParameters;
            foreach (int countryId in InitData.Keys)
            {
                BidParameterCountry bpc = countries.First(c => c.CountryId == countryId);
                List<int> months = InitData[countryId];
                for (int m = 0; m < months.Count; m++)
                {
                    int month = m + 1;
                    list.Add(new BidParameterCountryMonth()
                    {
                        BidParameterCountry = bpc,
                        BidParameterCountryId = countryId,
                        BidParameterCountryMonthId = month,
                        BidParameterCountryYearId = -1,
                        MonthNumberInYear = (m % 12) + 1,
                        Value = months[m]
                    });
                }
            }
            return list;
        }
        public static void AttachToCountries(int bidScenarioId, List<BidParameterCountry> countryList)
        {
            if (bidScenarioId != 291556)
            {
                throw new ApplicationException("No Country Month Parameters available for bid scenario " + bidScenarioId);
            }
            InitializeData();
            foreach (BidParameterCountry bpc in countryList)
            {
                {
                    int countryId = bpc.CountryId;
                    List<BidParameterCountryMonth> subList = new List<BidParameterCountryMonth>();
                    if (InitData.Keys.Contains(countryId))
                    {
                        List<int> months = InitData[countryId];
                        for (int m = 0; m < months.Count; m++)
                        {
                            int month = m + 1;
                            subList.Add(new BidParameterCountryMonth()
                            {
                                BidParameterCountry = bpc,
                                BidParameterCountryId = countryId,
                                BidParameterCountryMonthId = month,
                                BidParameterCountryYearId = -1,
                                MonthNumberInYear = (m % 12) + 1,
                                Value = months[m]
                            });
                        }
                    }
                    bpc.CountryMonths = subList;
                }
            }
        }
    }
}
