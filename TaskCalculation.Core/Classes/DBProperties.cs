﻿namespace TaskCalculation.Core.Classes
{
    public class DBProperties
    {

        // The following enumerated type defines the values in the REGION table
        public enum enuRegion : int
        {
            EDS_CEE = 9,
            USA_CANADA = 1,
            EUROPE_AFRICA = 2,
            LATIN_AMERICA = 3,
            ASIA_PACIFIC = 4,
            GLOBAL = 0,
            EDS_US_CLINIC = 5,
            EDS_EU_CLINIC = 6,
            EDS_US_LAB = 7,
            EDS_EU_LAB = 8,
        }

        // The following enumerated type defines the values in the PARAMETER_VALUE_TYPE table
        public enum enuParameterValueType : int
        {
            BOOLEAN_ONE_OR_ZERO = 3,
            INTEGER = 1,
            DECIMAL = 2,
            PERCENTAGE = 4,
        }

        // The following enumerated type defines the values in the PARAMETER_TYPE table
        public enum enuParameterType : int
        {
            COUNTRY_PARAMETER = 5,
            EFFICIENCY_PARAMETER = 3,
            COUNTRY_MONTH_PARAMETER = 6,
            UNIT_PARAMETER = 1,
            TIMELINE_PARAMETER = 2,
            MONITORING_PARAMETER = 4,
        }

        // The following enumerated type defines the values in the MST_VERSION_STATUS table
        public enum enuMasterVersionStatus : int
        {
            OBSOLETE = 1,
            RELEASED = 2,
            EDIT = 3,
        }

        // The following enumerated type defines the values in the CONTRACT_TYPE table
        public enum enuContractType : int
        {
            TIME_AND_MATERIALS = 1,
            FIXED = 2,
            UNIT = 3,
            MILESTONE = 4,
        }

        // The following enumerated type defines the values in the FREQUENCY_TYPE table
        public enum enuFrequencyType : int
        {
            DURATION_IN_MONTHS = 1,
            DURATION_IN_WEEKS = 2,
            DURATION_IN_YEARS = 3,
            DURATION_IN_QUARTERS = 4,
            DURATION_IN_DAYS = 5,
            DURATION_OF_STUDY = 6,
        }

        // The following enumerated type defines the values in the ROUNDING_FUNCTION table
        public enum enuRoundingFunction : int
        {
            CEILING = 1,
            ROUND = 2,
            FLOOR = 3,
            ROUND_TO_HALF = 4,
        }

        // The following enumerated type defines the values in the WBS_LEVEL table
        public enum enuWorkBreakdownStructureLevel : int
        {
            BID_SCENARIO = 1,
            FUNCTIONAL_SERVICE_AREA = 2,
            SERVICE = 3,
            ACTIVITY = 4,
            TASK = 5,
            PRICE_DRIVER = 6,
            PRICE_DRIVER_COUNTRY = 7,
        }

        // The following enumerated type defines the values in the PRICE_DRIVER_TYPE table
        public enum enuPriceDriverType : int
        {
            SERVICE_HOURLY = 1,
            EXPENSE = 2,
            FEE = 3,
        }

        // The following enumerated type defines the values in the DATE_INTERVAL table
        public enum enuDateInterval : int
        {
            WEEKS = 1,
            MONTHS = 2,
            DAYS = 3,
            QUARTERS = 4,
            YEARS = 5,
            ONCE = 6,
        }

        // The following enumerated type defines the values in the SYSTEM_CONFIG_ITEM table
        public enum enuSystemConfigItem : int
        {
            SALES_TRACKING_FCST_DATE = 10000,
            RELEASE_NUMBER = 12,
            PRA_ACCOUNTING_EMAIL = 100040,
            AWARD_ADMIN_EMAIL = 100041,
            LAST_DB_SCRIPT_NUMBER = 100020,
            REFRESH_RUNNING = 1,
            ENVIRONMENT = 13,
            LAST_SCRIPT_RUN = 100050,
            REL_1_5_LAST_SCRIPT_RUN = 100060,
        }

        // The following enumerated type defines the values in the OPPORTUNITY_TYPE table
        public enum enuOpportunityType : int
        {
            CHANGE_ORDER = 1,
            NEW_BUSINESS = 2,
        }

        // The following enumerated type defines the values in the USER_ROLE table
        public enum enuUserRole : int
        {
            DEMO_FUNCTIONAL_ADMINISTRATOR = 13,
            SYSTEM_ADMINISTRATOR = 1,
            FUNCTIONAL_ADMINISTRATOR = 2,
            PROPOSAL_STAFF = 3,
            PROPOSAL_FINANCE = 4,
            PROPOSAL_MANAGERS = 5,
            BID_TEAM_MEMBER = 8,
            TRAINING_USER = 9,
            PROPOSAL_INTAKE = 6,
            BASIC = 7,
            DEMO = 10,
            FINANCIAL_ANALYST = 11,
            PROJECT_ANALYST = 12,
            NBA_ADMINISTRATOR = 14,
            EDS_NBA_ADMINISTRATOR = 15,
        }

        // The following enumerated type defines the values in the WORK_ALLOCATION_METHOD table
        public enum enuWorkAllocationMethod : int
        {
            WORK_AND_SOURCE_LOCATION = 1,
            WORK_LOCATION_AND_PERCENTAGE = 2,
            FTE_AND_LOCATION = 3,
        }

        // The following enumerated type defines the values in the RATE_CARD_TYPE table
        public enum enuRateCardType : int
        {
            STANDARD_BILLING_RATE_CARD = 1,
            STANDARD_COST_RATE_CARD = 2,
            CLIENT_BILLING_RATE_CARD = 3,
            ACTIVE_BILLING_RATE_CARD = 4,
        }

        // The following enumerated type defines the values in the INFLATION_CARD_TYPE table
        public enum enuInflationCardType : int
        {
            STANDARD_BILLING_INFLATION_CARD = 1,
            STANDARD_COST_INFLATION_CARD = 2,
            CLIENT_BILLING_INFLATION_CARD = 3,
            ACTIVE_BILLING_INFLATION_CARD = 4,
        }

        // The following enumerated type defines the values in the UNIT_TYPE table
        public enum enuUnitType : int
        {
            USER_DEFINED = 1,
            UNIT_PARAMETER = 2,
            DURATION = 3,
        }

        // The following enumerated type defines the values in the UNIT_GRID_TYPE table
        public enum enuUnitGridType : int
        {
            PRA_UNIT_GRID = 1,
            CLIENT_UNIT_GRID = 2,
            PROJECT_UNIT_GRID = 3,
            BID_UNIT_GRID = 4,
            BID_SCENARIO_UNIT_GRID = 5,
        }

        // The following enumerated type defines the values in the BID_ROLE table
        public enum enuBidRole : int
        {
            ENGAGEMENT_PARTNER = 1,
            ACCOUNT_DIRECTOR = 2,
            GENERAL_PARTNER = 3,
            THERAPEUTIC_EXPERT = 4,
            STRATEGIC_BD = 5,
            OPERATIONAL_OWNER = 6,
            PROPOSAL_DIRECTOR = 7,
            PROPOSAL_WRITER = 8,
            PROPOSAL_COORDINATOR = 9,
            OTHER = 10,
        }

        // The following enumerated type defines the values in the BID_DEVELOPMENT_STAGE table
        public enum enuBidDevelopmentStage : int
        {
            INTAKE = 1,
            BID_TEAM_ASSIGNMENT = 2,
            IN_DEVELOPMENT = 3,
            SENT = 4,
            CANCELLED = 5,
            SUSPENDED = 6,
        }

        // The following enumerated type defines the values in the CALCULATION_STATUS table
        public enum enuCalculationStatus : int
        {
            CALCULATIONS_COMPLETE = 1,
            CALCULATION_IN_PROGRESS = 2,
            CALCULATION_IS_REQUIRED = 3,
        }

        // The following enumerated type defines the values in the BID_SCENARIO_STATUS table
        public enum enuBidScenarioStatus : int
        {
            ACTIVE = 1,
            DELETED = 2,
        }

        // The following enumerated type defines the values in the OVERRIDE_TYPE table
        public enum enuOverrideType : int
        {
            BID_DISCOUNT_PERCENT_OVERRIDE_ID = 3,
            BID_RATE_OVERRIDE_ID = 4,
            BID_DISCOUNT_PERCENT = 1,
            BID_RATE_OVERRIDE = 2,
        }

        // The following enumerated type defines the values in the CONTRACT_STATUS table
        public enum enuContractStatus : int
        {
            EXECUTIVE_DECISION_VERBAL = 10,
            EMAIL_LOA_AAF = 11,
            CNF_CIPS = 12,
            LOI = 5,
            STARTUP_AGREEMENT = 6,
            CONTRACT_CHANGE_ORDER = 7,
            CANCELLED = 8,
            PRE_DECISION = 9,
        }

        // The following enumerated type defines the values in the SALES_STATUS table
        public enum enuSalesStatus : int
        {
            CHANGE_ORDER_IN_DEVELOPMENT = 12,
            STRATEGIC_OPPORTUNITY = 13,
            EXCLUDE_FROM_REPORTS = 14,
            PRE_RFP = 1,
            RFP_IN_HOUSE = 2,
            REBID_AWARDED_IN_HOUSE = 3,
            REBID_COMPETITIVE_IN_HOUSE = 4,
            SENT_OUTSTANDING = 6,
            AWARDED = 7,
            LOST = 8,
            WITHDRAWN = 9,
            DECLINED = 10,
            CANCELLED = 11,
        }

        // The following enumerated type defines the values in the ADJUSTMENT_REASON table
        public enum enuAdjustmentReason : int
        {
            REGIONAL_TRANSFER = 6,
            ACQUISITION = 7,
            VOLUME_REBATE_ONLY = 8,
            NEW_BUSINESS = 1,
            CHANGE_ORDER = 2,
            HISTORICAL = 5,
            RECONCILING_ITEMS = 9,
        }

        // The following enumerated type defines the values in the AUTO_SELECT_METHODOLOGY table
        public enum enuAutoSelectMethodology : int
        {
            OFF = 1,
            ON = 2,
            CONDITIONAL = 3,
            CORE_RESOURCE = 4,
            FSA_ENABLED = 5,
        }

        // The following enumerated type defines the values in the BUSINESS_DIVISION table
        public enum enuBusinessDivision : int
        {
            LATE_PHASE = 166900,
            PRODUCT_REGISTRATION = 1,
            EDS = 2,
        }

        // The following enumerated type defines the values in the CELL_HORIZONTAL_ALIGNMENT table
        public enum enuCellHorizontalAlignment : int
        {
            CENTER = 1,
            LEFT = 2,
            RIGHT = 3,
            JUSTIFY = 4,
        }

        // The following enumerated type defines the values in the CELL_VERTICAL_ALIGNMENT table
        public enum enuCellVerticalAlignment : int
        {
            BOTTOM = 1,
            CENTER = 2,
            DISTRIBUTED = 3,
            JUSTIFIED = 4,
            TOP = 5,
        }

        // The following enumerated type defines the values in the MST_BID_REPORT_CELL_TYPE table
        public enum enuMasterBidReportCellType : int
        {
            COUNTRY_PARAMETER = 6,
            TEXT = 1,
            PROJECT_ATTRIBUTE = 2,
            TIMELINE_PARAMETER = 4,
            UNIT_PARAMETER = 3,
            DURATION_PARAMETER = 5,
            MONITORING_PARAMETER = 7,
        }

        // The following enumerated type defines the values in the MST_BID_REP_WORKSHEET_TYPE table
        public enum enuMasterBidReportWorksheetType : int
        {
            ROW_DRIVEN = 1,
            COLUMN_DRIVEN = 2,
            COUNTRY_PARAMETER = 3,
        }
    }  // DBProperties

}
