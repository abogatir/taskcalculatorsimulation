﻿namespace TaskCalculation.Core.Classes
{
    public class BidDiscountPercentOverride
    {

        #region Property Name Constants

        // The following constants are used for databinding to properties.
        protected const string BID_RATE_TABLE_ALIAS = "br";
        protected const string BID_SCENARIO_TABLE_ALIAS = "bsc";
        protected const string BID_FUNCTIONAL_SERVICE_AREA_TABLE_ALIAS = "bfsa";
        protected const string BID_SERVICE_TABLE_ALIAS = "bse";
        protected const string BID_ACTIVITY_TABLE_ALIAS = "ba";
        protected const string BID_TASK_TABLE_ALIAS = "bt";
        protected const string BID_RATE_CARD_TABLE_ALIAS = "brc";
        protected const string BU_FUNCTION_TABLE_ALIAS = "buf";

        public const string WBS_LEVEL_PROPERTY = "WbsLevel";

        public const string BID_DISCOUNT_PERCENT_OVER_ID_PROPERTY = "BidDiscountPercentOverrideId";
        public const string BID_RATE_ID_PROPERTY = "BidRateId";
        public const string DISCOUNT_PERCENT_PROPERTY = "DiscountPercent";
        public const string WBS_LEVEL_ID_PROPERTY = "WbsLevelId";
        public const string BID_FUNCTIONAL_SERVICE_AREA_ID_PROPERTY = "BidFunctionalServiceAreaId";
        public const string BID_SERVICE_ID_PROPERTY = "BidServiceId";
        public const string BID_ACTIVITY_ID_PROPERTY = "BidActivityId";
        public const string BID_TASK_ID_PROPERTY = "BidTaskId";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";


        //This column is used to get the parent rate for the object.
        public const string PARENT_DISCOUNT_PERCENT_PROPERTY = "ParentDiscountPercent";

        #endregion // Generated Property Name Constants

        #region Default Values

        protected const DBProperties.enuWorkBreakdownStructureLevel WBS_LEVEL_DEFAULT_VALUE = DBProperties.enuWorkBreakdownStructureLevel.TASK;
        protected const int WORK_BREAKDOWN_STRUCTURE_LEVEL_ID_DEFAULT_VALUE = AppConstants.NO_ID;

        public const int NEW_BID_DISCOUNT_PERCENT_OVER_ID = AppConstants.NO_ID;

        // The following constants are used to define the default values for the instance variables.

        protected const int BID_DISCOUNT_PERCENT_OVER_ID_DEFAULT_VALUE = NEW_BID_DISCOUNT_PERCENT_OVER_ID;
        protected const int BID_RATE_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const decimal DISCOUNT_PERCENT_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const int WBS_LEVEL_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int BID_FUNCTIONAL_SERVICE_AREA_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int BID_SERVICE_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int BID_ACTIVITY_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int BID_TASK_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int BID_SCENARIO_ID_DEFAULT_VALUE = AppConstants.NO_ID;



        #endregion // Generated Default Values

        #region Database Constants

        protected const string UDF_GET_PARENT_OFFSET_VALUE = "udf_getOverrideValue";


        protected const string BID_DISCOUNT_PERCENT_OVER_TABLE_ALIAS = "b";
       

        #endregion // Generated Database Constants

        #region Fields

        #endregion // Generated Fields

        #region Properties

        /// <summary> 
        /// Gets or sets The work breakdown structure level at which the parameter is being overridden
        /// </summary> 
        public int WorkBreakdownStructureLevelId { get; private set; }


        /// <summary>
        /// Gets or sets the WorkBreakdownStructureLevel type that corresponds to WorkBreakdownStructureLevelId
        /// </summary>
        public DBProperties.enuWorkBreakdownStructureLevel WorkBreakdownStructureLevel { get; private set; }


        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidDiscountPercentOverrideId { get; set; }

        /// <summary> 
        /// Gets or sets The rate for which this override has been created
        /// </summary> 
        public int BidRateId { get; set; }


        /// <summary> 
        /// Gets or sets The discount percentage that should be applied for the client to this rate
        /// </summary> 
        public decimal DiscountPercent { get; set; }

        /// <summary> 
        /// Gets or sets The work breakdown structure level at which the parameter is being overridden
        /// </summary> 
        public int WbsLevelId { get; set; }

        /// <summary> 
        /// Gets or sets The functional service area at which it was overridden, if applicable
        /// </summary> 
        public int BidFunctionalServiceAreaId { get; set; }

        /// <summary> 
        /// Gets or sets The service at which it was overridden, if applicable
        /// </summary> 
        public int BidServiceId { get; set; }

        /// <summary> 
        /// Gets or sets The activity at which it was overridden, if applicable/// </summary> 
        public int BidActivityId { get; set; }

        /// <summary> 
        /// Gets or sets The task at which it was overridden, if applicable
        /// </summary> 
        public int BidTaskId { get; set; }

        /// <summary> 
        /// Gets or sets The bid scenario at which it was overridden
        /// </summary> 
        public int BidScenarioId { get; set; }

        #endregion // Generated Properties


    }
}
