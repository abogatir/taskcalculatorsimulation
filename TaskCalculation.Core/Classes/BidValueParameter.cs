﻿namespace TaskCalculation.Core.Classes
{
    public class BidValueParameter : BidParameter
    {
        #region Constants

        #region Database Constants

        private const string PARAMETER_SET_TABLE_ALIAS = "ps";
        private const string BID_SCENARIO_TASK_PARAMETER_TABLE_ALIAS = "bstp";

        //private const string V_ACTIVE_BID_PARAMETERS_ALIAS = "vabp";

        //private const string ACTIVE_SELECT_COLUMNS = V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.BID_PARAMETER_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.DESCRIPTION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.UNIQUE_NAME_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.IS_DERIVED_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.EQUATION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.PARAMETER_TYPE_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.MST_PARAMETER_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.PARAMETER_VALUE_TYPE_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.VALUE_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.PRECISION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.PARAMETER_SET_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.ACTIVE_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.UNIT_DESCRIPTION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.LONG_DESCRIPTION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.CLIENT_DESCRIPTION_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.BID_INSTRUCTIONS_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.MST_PARAMETER_VERSION_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.BID_SCENARIO_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.DISPLAY_ORDER_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.DEFAULT_VALUE_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.CAN_OVERRIDE_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.DATE_VALUE_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.START_DATE_PARAMETER_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.START_DATE_OFFSET_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.START_DATE_OFFSET_INTERVAL_ID_COLUMN +
        //                                             "," + V_ACTIVE_BID_PARAMETERS_ALIAS + "." + DBConstants.V_ACTIVE_BID_PARAMETERS.VISIBILITY_PARAMETER_ID_COLUMN;

        //private const string ACTIVE_SELECT_CMD = "SELECT " + ACTIVE_SELECT_COLUMNS +
        //                                         " FROM " + DBConstants.V_ACTIVE_BID_PARAMETERS.TABLE_NAME + " " + V_ACTIVE_BID_PARAMETERS_ALIAS;

        #endregion // Database Constants

        #region Property Names

        public const string DEFAULT_VALUE_PROPERTY = "DefaultValue";
        public const string EQUATION_PROPERTY = "Equation";
        public const string IS_DERIVED_PROPERTY = "IsDerived";
        public const string PARAMETER_VALUE_TYPE_PROPERTY = "ParameterValueType";
        public const string PRECISION_PROPERTY = "Precision";
        public const string UNIT_DESCRIPTION_PROPERTY = "UnitDescription";
        public const string VALUE_PROPERTY = "Value";

        #endregion

        #region Default Values

        //protected const decimal DEFAULT_VALUE_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const string EQUATION_DEFAULT_VALUE = "";
        protected const bool IS_DERIVED_DEFAULT_VALUE = false;
        protected const DBProperties.enuParameterValueType PARAMETER_VALUE_TYPE_DEFAULT_VALUE = DBProperties.enuParameterValueType.DECIMAL;
        protected const string UNIT_DESCRIPTION_DEFAULT_VALUE = "";
        //protected const decimal VALUE_DEFAULT_VALUE = AppConstants.NO_VALUE;

        #endregion

        #endregion // Constants

        #region Attributes

        //The default value, if applicable, of the parameter
        private decimal? mDefaultValue;

        //The equation that calculates a derived parameter
        private string mEquation;

        //Indicates whether this parameter is derived
        private bool mIsDerived;

        //The type of the value (e.g., decimal)
        private DBProperties.enuParameterValueType mParameterValueType;

        //The precision of the number, if applicable
        private int mPrecision;

        //The description of the unit type
        private string mUnitDescription;

        //The value stored for the parameter
        private decimal? mValue;

        private BidValueParameterOverride mBidValueParameterOverride;

        #endregion // Attributes

        #region Properties

        /// <summary> 
        /// Gets or sets The default value, if applicable, of the parameter
        /// </summary> 
        public decimal? DefaultValue
        {
            get { return mDefaultValue; }
            private set { mDefaultValue = value; }
        }

        /// <summary> 
        /// Gets or sets The equation that calculates a derived parameter
        /// </summary> 
        public string Equation
        {
            get { return mEquation; }
            set { mEquation = value; }
        }

        /// <summary> 
        /// Gets or sets Indicates whether this parameter is derived
        /// </summary> 
        public bool IsDerived
        {
            get { return mIsDerived; }
            private set { mIsDerived = value; }
        }

        /// <summary> 
        /// Gets or sets The type of the value (e.g., decimal)
        /// </summary> 
        public DBProperties.enuParameterValueType ParameterValueType
        {
            get { return mParameterValueType; }
            private set { mParameterValueType = value; }
        }

        /// <summary> 
        /// Gets or sets The precision of the number, if applicable
        /// </summary> 
        public int Precision
        {
            get { return mPrecision; }
            private set { mPrecision = value; }
        }

        /// <summary> 
        /// Gets or sets The description of the unit type
        /// </summary> 
        public string UnitDescription
        {
            get { return mUnitDescription; }
            private set { mUnitDescription = value; }
        }

        /// <summary> 
        /// Gets or sets The value stored for the parameter
        /// </summary> 
        public decimal? Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        /// <summary>
        /// Gets or sets the BidValueParameterOverride
        /// </summary>
        public BidValueParameterOverride BidValueParameterOverride
        {
            get
            {
                return mBidValueParameterOverride;
            }
            set
            {
                mBidValueParameterOverride = value;
            }
        }

        #endregion // Properties

        /// <summary> 
        /// Creates a BidValueParameter object using default values.
        /// </summary> 
        public BidValueParameter ()
            : base ()
        {
            // calling base constructor will invoke InitializeData in this class
        }


    }
}
