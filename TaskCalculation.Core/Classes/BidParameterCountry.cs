﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TaskCalculation.Core.Classes
{
    public class BidParameterCountry
    {

        #region Constants

        protected const string BID_PARAMETER_TABLE_ALIAS = "bp";
        protected const string PARAMETER_SET_TABLE_ALIAS = "ps";
        protected const string BID_COUNTRY_TABLE_ALIAS = "bc";

        #endregion // Constants

        #region Attributes

        private BidValueParameter mBidParameter;
        public List<BidParameterCountryMonth> CountryMonths;

        #endregion // Attributes

        #region Properties

        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidParameterCountryId { get; private set; }

        /// <summary> 
        /// Gets or sets The bid parameter to which this country value relates
        /// </summary> 
        public int BidParameterId { get; private set; }

        /// <summary>
        /// Gets or sets the actual BidValueParameter associated to this BidParameterCountry
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidParameterCountry.Update does not update the attributes on the BidParameter object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidParameter property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidValueParameter BidParameter
        {
            get
            {
                if (mBidParameter == null)
                {
                    throw new InvalidOperationException("BidParameterCountry.BidParameter property must be set before accessing.");
                }

                return mBidParameter;
            }
            set
            {
                mBidParameter = value;
            }
        }

        /// <summary>
        /// Gets or sets the BidParameterCountryOverride associated with this BidParameterCountry 
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidParameterCountry.Update does not update the attributes on the BidParameterCountryOverride object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidParameterCountryOverride property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>

        /// <summary> 
        /// Gets or sets The country for which this value exists
        /// </summary> 
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the actual Country associated to this BidParameterCountry
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidParameterCountry.Update does not update the attributes on the Country object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the Country property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        /// <summary> 
        /// Gets or sets The default value stored for this country parameter
        /// </summary> 
        public decimal? DefaultValue { get; private set; }

        /// <summary> 
        /// Gets or sets The value stored for this country parameter
        /// </summary> 
        public decimal? Value { get; set; }

        #endregion // Properties

        public const string BID_PARAMETER_COUNTRY_ID_PROPERTY = "BidParameterCountryId";
        public const string BID_PARAMETER_ID_PROPERTY = "BidParameterId";
        public const string COUNTRY_ID_PROPERTY = "CountryId";
        public const string VALUE_PROPERTY = "Value";
        public const string DEFAULT_VALUE_PROPERTY = "DefaultValue";

        protected const string BID_PARAMETER_COUNTRY_TABLE_ALIAS = "b";

        public override string ToString()
        {
            return "Country: " + CountryId + ", Value: " + Value;
        }

    }
}
