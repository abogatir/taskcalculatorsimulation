﻿using System;

namespace TaskCalculation.Core.Classes
{
    public class BidWorkAllocation
    {

        #region Constants

        #region Default Values

        public const int COUNTRY_ID_DEFAULT_VALUE = AppConstants.NO_ID;

        #endregion // Default Values

        #region Database Constants

        protected const string BID_SCENARIO_TABLE_ALIAS = "bs";
        protected const string BID_FUNCTIONAL_SERVICE_AREA_TABLE_ALIAS = "bfsa";
        protected const string BID_SERVICE_TABLE_ALIAS = "bsv";
        protected const string BID_ACTIVITY_TABLE_ALIAS = "ba";
        protected const string BID_TASK_TABLE_ALIAS = "bt";
        protected const string BID_PRICE_DRIVER_TABLE_ALIAS = "bpd";
        protected const string FUNCTION_CODE_TABLE_ALIAS = "fc";
        protected const string FUNCTION_CODE_GROUP_TABLE_ALIAS = "fcg";
        protected const string BID_COUNTRY_TABLE_ALIAS = "bc";
        protected const string BID_PRICE_DRIVER_OVERRIDE_TABLE_ALIAS = "bpdo";


        #endregion // Database Constants

        #endregion // Constants

        #region Attributes

        private FunctionCode mFunctionCode;
        private BusinessUnit mBusinessUnit;
        private Country mCountry;

        //The country referred to by this allocation
        private int mCountryId;

        //The function code that this allocation refers to
        private string mNatFunctionCode;

        #endregion // Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the actual BusinessUnit associated to this BidWorkAllocation
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidWorkAllocation.Update does not update the attributes on the BusinessUnit object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BusinessUnit property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BusinessUnit BusinessUnit
        {
            get
            {
                if (mBusinessUnit == null)
                {
                    throw new InvalidOperationException ("BidWorkAllocation.BusinessUnit property must be set before accessing.");
                }

                return mBusinessUnit;
            }
            set
            {
                mBusinessUnit = value;
            }
        }

        /// <summary>
        /// Gets or sets the actual Country associated to this BidWorkAllocation
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidWorkAllocation.Update does not update the attributes on the Country object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the Country property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public Country Country
        {
            get
            {
                if (mCountry == null)
                {
                    throw new InvalidOperationException ("BidWorkAllocation.Country property must be set before accessing.");
                }

                return mCountry;
            }
            set
            {
                mCountry = value;
            }
        }

        /// <summary> 
        /// Gets or sets The country referred to by this allocation
        /// </summary> 
        public int CountryId
        {
            get { return mCountryId; }
            set { mCountryId = value; }
        }

        /// <summary>
        /// Gets or sets the actual FunctionCode associated to this BidWorkAllocation
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidWorkAllocation.Update does not update the attributes on the FunctionCode object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the FunctionCode property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public FunctionCode FunctionCode
        {
            get
            {
                if (mFunctionCode == null)
                {
                    throw new InvalidOperationException ("BidWorkAllocation.FunctionCode property must be set before accessing.");
                }

                return mFunctionCode;
            }
            set
            {
                mFunctionCode = value;
            }
        }

        /// <summary> 
        /// Gets or sets The function code that this allocation refers to
        /// </summary> 
        public string NatFunctionCode
        {
            get { return mNatFunctionCode; }
            set { mNatFunctionCode = value; }
        }

        #endregion // Properties

        // Generated Code Changes
        // Renamed FunctionCode to NatFunctionCode to support function code object propery properly
        // Moved InitializeData and SetDrowValues to support property renaming
        // Moved CountryId to make nullable

        // NOTE: Table Comments - Contains the bid work allocation for resources that are part of the project

        #region Generated Code

        //NOTE: The code inside of this region is generated by DataClassGeneratorCSharp2008.sql
        //NOTE: To modify first move the code down into the appropriate region of the main class implementation below.

        #region Generated Property Name Constants

        // The following constants are used for databinding to properties.

        public const string BID_WORK_ALLOCATION_ID_PROPERTY = "BidWorkAllocationId";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";
        public const string FUNCTION_CODE_PROPERTY = "FunctionCode";
        public const string BU_CODE_PROPERTY = "BuCode";
        public const string PERCENTAGE_PROPERTY = "Percentage";
        public const string COUNTRY_ID_PROPERTY = "CountryId";
        public const string WORK_ALLOCATION_METHOD_PROPERTY = "WorkAllocationMethod";
        protected const DBProperties.enuWorkAllocationMethod WORK_ALLOCATION_METHOD_DEFAULT_VALUE = (DBProperties.enuWorkAllocationMethod)AppConstants.NO_ID;

        #endregion // Generated Property Name Constants

        #region Generated Default Values

        public const int NEW_BID_WORK_ALLOCATION_ID = AppConstants.NO_ID;

        // The following constants are used to define the default values for the instance variables.

        protected const int BID_WORK_ALLOCATION_ID_DEFAULT_VALUE = NEW_BID_WORK_ALLOCATION_ID;
        protected const int BID_SCENARIO_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const string FUNCTION_CODE_DEFAULT_VALUE = "";
        protected const string BU_CODE_DEFAULT_VALUE = "";
        protected const decimal PERCENTAGE_DEFAULT_VALUE = AppConstants.NO_VALUE;

        #endregion // Generated Default Values

        #region Generated Database Constants

        protected const string BID_WORK_ALLOCATION_TABLE_ALIAS = "b";
       

        #endregion // Generated Database Constants

        #region Generated Fields

        //The primary key for the table
        private int mBidWorkAllocationId;

        //The bid scenario for which these defaults are copied
        private int mBidScenarioId;

        //The default business unit for the work allocation
        private string mBuCode;

        //The percentage of the work that should be allocated to this location
        private decimal mPercentage;

        //The work allocation type supported by this allocation
        private DBProperties.enuWorkAllocationMethod mWorkAllocationMethod;

        #endregion // Generated Fields

        #region Generated Properties

        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidWorkAllocationId
        {
            get { return mBidWorkAllocationId; }
            set { mBidWorkAllocationId = value; }
        }

        /// <summary> 
        /// Gets or sets The bid scenario for which these defaults are copied
        /// </summary> 
        public int BidScenarioId
        {
            get { return mBidScenarioId; }
            set { mBidScenarioId = value; }
        }

        /// <summary> 
        /// Gets or sets The default business unit for the work allocation
        /// </summary> 
        public string BuCode
        {
            get { return mBuCode; }
            set { mBuCode = value; }
        }

        /// <summary> 
        /// Gets or sets The percentage of the work that should be allocated to this location
        /// </summary> 
        public decimal Percentage
        {
            get { return mPercentage; }
            set { mPercentage = value; }
        }

        /// <summary> 
        /// Gets or sets The work allocation type supported by this allocation
        /// </summary> 
        public DBProperties.enuWorkAllocationMethod WorkAllocationMethod
        {
            get { return mWorkAllocationMethod; }
            set { mWorkAllocationMethod = value; }
        }

        #endregion // Generated Properties

        #region Generated Constructors

        #endregion // Generated Constructors

        #region Generated Methods

        #endregion  // Generated Methods

        #endregion  // Generated Code

    }
}
