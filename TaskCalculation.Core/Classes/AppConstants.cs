﻿using System;

namespace TaskCalculation.Core.Classes
{
    public class AppConstants
    {
        //'The following constant defines the value used when an entity has no ID
        public const int NO_ID = -1;

        //The following constant defines the null date/time used to determine if a 
        //date/time is null
        public static readonly DateTime NULL_DATE_VALUE = new DateTime (0001, 1, 1, 12, 00, 00); //1/1/0001 12:00:00 AM

        //The following constant defines the minimum date value for the application
        //NOTE: This value must be >= the SQL minimum date which by default is 01/01/1753
        public static readonly DateTime MIN_DATE_VALUE = new DateTime (1900, 1, 1);//#1/1/1900#;

        //The following constant defines the maximum date value for the application
        public static readonly DateTime MAX_DATE_VALUE = new DateTime (9999, 12, 31, 11, 59, 59);//#12/31/9999 11:59:59 PM#;

        //The following constant defines a "non value" to use for integers
        public const int NO_VALUE = 0;

        //The following constant defines the suffix to append to a table name to get it's sequence's name
        public const string SEQUENCE_SUFFIX = "_SQ";

        //The following constant defines the number of forecast periods to display on the resource administration pages
        //public const int EMPLOYEE_FORECAST_DISPLAY_MONTHS = 9; // commented out because this does not belong here
    }

}
