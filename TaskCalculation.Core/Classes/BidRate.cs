﻿using System;

namespace TaskCalculation.Core.Classes
{
    public class BidRate
    {

        #region Constants

        #region Database Constants

        protected const string BU_FUNCTION_TABLE_ALIAS = "bf";
        protected const string BID_RATE_CARD_TABLE_ALIAS = "brc";
        protected const string BID_SCENARIO_TABLE_ALIAS = "bsc";

        protected const string BID_FUNCTIONAL_SERVICE_AREA_TABLE_ALIAS = "bfsa";
        protected const string BID_SERVICE_TABLE_ALIAS = "bsv";
        protected const string BID_ACTIVITY_TABLE_ALIAS = "ba";
        protected const string BID_TASK_TABLE_ALIAS = "bt";
        protected const string BID_PRICE_DRIVER_TABLE_ALIAS = "bpd";
        protected const string FUNCTION_CODE_TABLE_ALIAS = "fc";
        protected const string BID_WORK_ALLOCATION_TABLE_ALIAS = "bwa";
        protected const string BID_COUNTRY_TABLE_ALIAS = "bc";
        protected const string BID_PRICE_DRIVER_OVERRIDE_TABLE_ALIAS = "bpdo";

        #endregion // Database Constants

        #endregion // Constants

        #region Attributes

        private BidRateCard mBidRateCard;
        private BidRateOverride mBidRateOverride;
        private BidRateOverride mBidRateOverrideParent;
        private BidDiscountPercentOverride mBidDiscountPercentOverride;
        private BuFunction mBuFunction;

        #endregion // Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the actual BidRateCard associated to this BidRate
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidRate.Update does not update the attributes on the BidRateCard object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidRateCard property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidRateCard BidRateCard
        {
            get
            {
                if (mBidRateCard == null)
                {
                    throw new InvalidOperationException("BidRate.BidRateCard property must be set before accessing.");
                }

                return mBidRateCard;
            }
            set
            {
                mBidRateCard = value;
            }
        }

        /// <summary>
        /// Gets or sets the actual BidRateOverride associated to this BidRate
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidRate.Update does not update the attributes on the BidRateOverride object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidRateOverride property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidRateOverride BidRateOverride
        {
            get
            {
                if (mBidRateOverride == null)
                {
                    throw new InvalidOperationException("BidRate.BidRateOverride property must be set before accessing.");
                }

                return mBidRateOverride;
            }
            set
            {
                mBidRateOverride = value;
            }
        }


        /// <summary>
        /// Gets or sets the actual Parent BidRateOverride associated to this BidRate
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidRate.Update does not update the attributes on the BidRateOverride object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidRateOverride property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidRateOverride BidRateOverrideParent
        {
            get
            {
                if (mBidRateOverrideParent == null)
                {
                    throw new InvalidOperationException("BidRate.BidRateOverrideParent property must be set before accessing.");
                }

                return mBidRateOverrideParent;
            }
            set
            {
                mBidRateOverrideParent = value;
            }
        }


        /// <summary>
        /// Bid Discount Percent Override
        /// </summary>
        public BidDiscountPercentOverride BidDiscountPercentOverride
        {
            get
            {
                if (mBidDiscountPercentOverride == null)
                {
                    throw new InvalidOperationException("BidRate.BidDiscountPercentOverride property must be set before accessing.");
                }

                return mBidDiscountPercentOverride;
            }

            set
            {
                mBidDiscountPercentOverride = value;
            }

        }










        /// <summary>
        /// Gets or sets the actual BuFunction associated to this BidRate
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidRate.Update does not update the attributes on the BuFunction object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BuFunction property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BuFunction BuFunction
        {
            get
            {
                if (mBuFunction == null)
                {
                    throw new InvalidOperationException("BidRate.BuFunction property must be set before accessing.");
                }

                return mBuFunction;
            }
            set
            {
                mBuFunction = value;
            }
        }

        #endregion // Properties

        // NOTE: Table Comments - Contains the rates for a rate card

        #region Generated Code

        //NOTE: The code inside of this region is generated by DataClassGeneratorCSharp2008.sql
        //NOTE: To modify first move the code down into the appropriate region of the main class implementation below.

        #region Generated Property Name Constants

        // The following constants are used for databinding to properties.

        public const string BID_RATE_ID_PROPERTY = "BidRateId";
        public const string BID_RATE_CARD_ID_PROPERTY = "BidRateCardId";
        public const string BUF_CODE_PROPERTY = "BufCode";
        public const string RATE_PROPERTY = "Rate";
        public const string CURRENCY_CODE_PROPERTY = "CurrencyCode";
        public const string DISCOUNT_PERCENT_PROPERTY = "DiscountPercent";
        public const string APPLIED_RATE_PROPERTY = "AppliedRate";
        public const string INFLATION_RATE_PROPERTY = "InflationRate";
        public const string EXCHANGE_RATE_PROPERTY = "ExchangeRate";

        #endregion // Generated Property Name Constants

        #region Generated Default Values

        public const int NEW_BID_RATE_ID = AppConstants.NO_ID;

        // The following constants are used to define the default values for the instance variables.

        protected const int BID_RATE_ID_DEFAULT_VALUE = NEW_BID_RATE_ID;
        protected const int BID_RATE_CARD_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const string BUF_CODE_DEFAULT_VALUE = "";
        protected const decimal RATE_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const string CURRENCY_CODE_DEFAULT_VALUE = "";
        protected const decimal DISCOUNT_PERCENT_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const decimal APPLIED_RATE_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const decimal INFLATION_RATE_DEFAULT_VALUE = AppConstants.NO_VALUE;
        protected const decimal EXCHANGE_RATE_DEFAULT_VALUE = AppConstants.NO_VALUE;

        #endregion // Generated Default Values

        #region Generated Database Constants

        protected const string BID_RATE_TABLE_ALIAS = "b";


        #endregion // Generated Database Constants

        #region Generated Fields

        //The primary key for the table
        private int mBidRateId;

        //The rate card to which the rate belongs
        private int mBidRateCardId;

        //The bufcode for which the rate is being assigned
        private string mBufCode;

        //The rate assigned
        private decimal mRate;

        //The currency of the rate
        private string mCurrencyCode;

        //The discount percentage that should be applied for the client to this rate
        private decimal mDiscountPercent;

        //The applied rate, only calculated on the active rate card
        private decimal mAppliedRate;

        //The inflation rate used to determine the applied rate
        private decimal mInflationRate;

        //The exchange rate
        private decimal mExchangeRate;

        #endregion // Generated Fields

        #region Generated Properties

        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidRateId
        {
            get { return mBidRateId; }
            set { mBidRateId = value; }
        }

        /// <summary> 
        /// Gets or sets The rate card to which the rate belongs
        /// </summary> 
        public int BidRateCardId
        {
            get { return mBidRateCardId; }
            set { mBidRateCardId = value; }
        }

        /// <summary> 
        /// Gets or sets The bufcode for which the rate is being assigned
        /// </summary> 
        public string BufCode
        {
            get { return mBufCode; }
            set { mBufCode = value; }
        }

        /// <summary> 
        /// Gets or sets The rate assigned
        /// </summary> 
        public decimal Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }

        /// <summary> 
        /// Gets or sets The currency of the rate
        /// </summary> 
        public string CurrencyCode
        {
            get { return mCurrencyCode; }
            set { mCurrencyCode = value; }
        }

        /// <summary> 
        /// Gets or sets The discount percentage that should be applied for the client to this rate
        /// </summary> 
        public decimal DiscountPercent
        {
            get { return mDiscountPercent; }
            set { mDiscountPercent = value; }
        }

        /// <summary> 
        /// Gets or sets The applied rate, only calculated on the active rate card
        /// </summary> 
        public decimal AppliedRate
        {
            get { return mAppliedRate; }
            set { mAppliedRate = value; }
        }

        /// <summary> 
        /// Gets or sets The inflation rate used to determine the applied rate
        /// </summary> 
        public decimal InflationRate
        {
            get { return mInflationRate; }
            set { mInflationRate = value; }
        }

        /// <summary> 
        /// Gets or sets The exchange rate
        /// </summary> 
        public decimal ExchangeRate
        {
            get { return mExchangeRate; }
            set { mExchangeRate = value; }
        }

        public string DisplayMember
        {
            get { return BufCode + "(" + BidRateId + ")"; }
        }

        #endregion // Generated Properties

        #region Generated Constructors

        /// <summary> 
        /// Creates a BidRate object using default values.
        /// </summary> 
        public BidRate()
        {
            InitializeData();
        }

        /// <summary> 
        /// Creates a BidRate object using default values.
        /// </summary> 
        /// 
        /// <param name="dRow"> DataRow containing the data row to use to initialize the instance variables</param>
        #endregion // Generated Constructors

        #region Generated Methods

        /// <summary> 
        /// Initializes the instance variable with default values.
        /// </summary> 
        protected void InitializeData()
        {
            mBidRateId = BID_RATE_ID_DEFAULT_VALUE;
            mBidRateCardId = BID_RATE_CARD_ID_DEFAULT_VALUE;
            mBufCode = BUF_CODE_DEFAULT_VALUE;
            mRate = RATE_DEFAULT_VALUE;
            mCurrencyCode = CURRENCY_CODE_DEFAULT_VALUE;
            mDiscountPercent = DISCOUNT_PERCENT_DEFAULT_VALUE;
            mAppliedRate = APPLIED_RATE_DEFAULT_VALUE;
            mInflationRate = INFLATION_RATE_DEFAULT_VALUE;
            mExchangeRate = EXCHANGE_RATE_DEFAULT_VALUE;
        }

        #endregion  // Generated Methods

        #endregion  // Generated Code

        #region Added Methods

        public string DisplayKey
        {
            get
            {
                string str = mBidRateId + "," + mAppliedRate;
                return str;
            }
        }
        #endregion Added Methods

    }
}
