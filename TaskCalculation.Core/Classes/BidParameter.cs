﻿using System;

namespace TaskCalculation.Core.Classes
{
    public abstract class BidParameter
    {

        #region Generated Code


        #region Generated Property Name Constants

        // The following constants are used for databinding to properties.

        public const string BID_PARAMETER_ID_PROPERTY = "BidParameterId";
        public const string DESCRIPTION_PROPERTY = "Description";
        public const string UNIQUE_NAME_PROPERTY = "UniqueName";
        public const string IS_DERIVED_PROPERTY = "IsDerived";
        public const string PARAMETER_TYPE_ID_PROPERTY = "ParameterTypeId";
        public const string MASTER_PARAMETER_ID_PROPERTY = "MasterParameterId";
        public const string PARAMETER_VALUE_TYPE_ID_PROPERTY = "ParameterValueTypeId";
        public const string VALUE_PROPERTY = "Value";
        public const string PRECISION_PROPERTY = "Precision";
        public const string PARAMETER_SET_ID_PROPERTY = "ParameterSetId";
        public const string ACTIVE_PROPERTY = "Active";
        public const string UNIT_DESCRIPTION_PROPERTY = "UnitDescription";
        public const string LONG_DESCRIPTION_PROPERTY = "LongDescription";
        public const string CLIENT_DESCRIPTION_PROPERTY = "ClientDescription";
        public const string BID_INSTRUCTIONS_PROPERTY = "BidInstructions";
        public const string MASTER_PARAMETER_VERSION_ID_PROPERTY = "MasterParameterVersionId";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";
        public const string DISPLAY_ORDER_PROPERTY = "DisplayOrder";
        public const string DEFAULT_VALUE_PROPERTY = "DefaultValue";
        public const string CAN_OVERRIDE_PROPERTY = "CanOverride";
        public const string DATE_VALUE_PROPERTY = "DateValue";
        public const string START_DATE_PARAMETER_ID_PROPERTY = "StartDateParameterId";
        public const string START_DATE_OFFSET_PROPERTY = "StartDateOffset";
        public const string START_DATE_OFFSET_INTERVAL_ID_PROPERTY = "StartDateOffsetIntervalId";
        public const string VISIBILITY_PARAMETER_ID_PROPERTY = "VisibilityParameterId";
        public const string EQUATION_PROPERTY = "Equation";
        public const string DISPLAY_TOTAL_PROPERTY = "DisplayTotal";
        public const string ACTIVE_PARAMETER_ID_PROPERTY = "ActiveParameterId";
        public const string PARAMETER_ACTIVE_PROPERTY = "ParameterActive";

        #endregion // Generated Property Name Constants
        #region Generated Properties

        /// <summary> 
        /// Gets or sets The primary key of the bid parameter
        /// </summary> 
        public int BidParameterId { get; set; }

        /// <summary> 
        /// Gets or sets The Description of the parameter (a short name used to identify it)
        /// </summary> 
        public string Description { get; set; }

        /// <summary> 
        /// Gets or sets This is a unique name for the parameter. 
        /// </summary> 
        public string UniqueName { get; set; }

        /// <summary> 
        /// Gets or sets Indicates whether this parameter is derived
        /// </summary> 
        public bool IsDerived { get; set; }

        /// <summary> 
        /// Gets or sets The type of parameter (e.g., date)
        /// </summary> 
        public int ParameterTypeId { get; set; }

        /// <summary> 
        /// Gets or sets The parent master parameter
        /// </summary> 
        public int MasterParameterId { get; set; }

        /// <summary> 
        /// Gets or sets The type of the value (e.g., decimal)
        /// </summary> 
        public int ParameterValueTypeId { get; set; }

        /// <summary> 
        /// Gets or sets The value stored for the parameter
        /// </summary> 
        public decimal Value { get; set; }

        /// <summary> 
        /// Gets or sets The precision of the number, if applicable
        /// </summary> 
        public int Precision { get; set; }

        /// <summary> 
        /// Gets or sets The parameter set to which this parameter belongs
        /// </summary> 
        public int ParameterSetId { get; set; }

        /// <summary> 
        /// Gets or sets Value should always be 'Y'.  Here for object-oriented inheritance support when copied from MST_PARAMETER
        /// </summary> 
        public bool Active { get; set; }

        /// <summary> 
        /// Gets or sets The description of the unit type
        /// </summary> 
        public string UnitDescription { get; set; }

        /// <summary> 
        /// Gets or sets The long description of the parameter
        /// </summary> 
        public string LongDescription { get; set; }

        /// <summary> 
        /// Gets or sets The client description that would be output from the system
        /// </summary> 
        public string ClientDescription { get; set; }

        /// <summary> 
        /// Gets or sets Instructions for the user on how it is to be used within a bid
        /// </summary> 
        public string BidInstructions { get; set; }

        /// <summary> 
        /// Gets or sets The version of the master parameter that this bid parameter was created from
        /// </summary> 
        public int MasterParameterVersionId { get; set; }

        /// <summary> 
        /// Gets or sets Indicates the bid scenario that this parameter is part of
        /// </summary> 
        public int BidScenarioId { get; set; }

        /// <summary> 
        /// Gets or sets The display order for the parameter within its parameter set
        /// </summary> 
        public int DisplayOrder { get; set; }

        /// <summary> 
        /// Gets or sets The default value, if applicable, of the parameter
        /// </summary> 
        public decimal DefaultValue { get; set; }

        /// <summary> 
        /// Gets or sets Indicates whether this parameter can or cannot be overridden
        /// </summary> 
        public bool CanOverride { get; set; }

        /// <summary> 
        /// Gets or sets Stores the date value for the parameter
        /// </summary> 
        public DateTime DateValue { get; set; }

        /// <summary> 
        /// Gets or sets The parameter that contains the date that is used as the start date for this parameter (the offset is used to determine the date value of this parameter)
        /// </summary> 
        public int StartDateParameterId { get; set; }

        /// <summary> 
        /// Gets or sets The start date offset for the task, if it doesn't have the same date as the parameter
        /// </summary> 
        public decimal StartDateOffset { get; set; }

        /// <summary> 
        /// Gets or sets The offset interval from the start date parameter, expressed as ('q','d', 'ww', 'm', 'yyyy').
        /// </summary> 
        public int StartDateOffsetIntervalId { get; set; }

        /// <summary> 
        /// Gets or sets Contains information about what parameter impacts whether this parameter is visible to the end user.  The referenced parameter must have a boolean value type.
        /// </summary> 
        public int VisibilityParameterId { get; set; }

        /// <summary> 
        /// Gets or sets The equation that calculates a derived parameter
        /// </summary> 
        public string Equation { get; set; }

        /// <summary> 
        /// Gets or sets Determines if total should be displayed on country parameters page.
        /// </summary> 
        public bool DisplayTotal { get; set; }

        /// <summary> 
        /// Gets or sets Parameter will be active if referenced parameter is active
        /// </summary> 
        public int ActiveParameterId { get; set; }

        /// <summary> 
        /// Gets or sets Indicates whether the parameter is active in the current bid scenario.  This is different from the 'Active' column which is copied from the master table and should always be 'Y'.
        /// </summary> 
        public bool ParameterActive { get; set; }

        #endregion // Generated Properties

       

        #endregion  // Generated Code


        #region User Supplied Code

        #region Constants

        #region Property Names

        public const string PARAMETER_TYPE_PROPERTY = "ParameterType";

        #endregion // Property Names

        #region Default Values

        protected const DBProperties.enuParameterType PARAMETER_TYPE_DEFAULT_VALUE = DBProperties.enuParameterType.UNIT_PARAMETER;
        protected const string EQUATION_DEFAULT_VALUE = "";

        #endregion // Default Values


        #endregion // Constants

        #region Attributes

        //The type of parameter (e.g., date)

        //The actual BidValueParameter that when evaluated determines whether this parameter is visible or not
        private BidValueParameter mVisibilityParameter;

        //The actual BidValueParameter that when evaluated determines whether this parameter is active or not
        private BidValueParameter mActiveParameter;

        #endregion // Attributes

        #region Properties

        /// <summary> 
        /// Gets or sets The type of parameter (e.g., date)
        /// </summary> 
        public DBProperties.enuParameterType ParameterType { get; private set; }

        /// <summary>
        /// Gets/sets the BidValueParameter object associated as the visibility parameter for this BidParameter
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface. 
        /// 
        /// NOTE: Because there is no ID constructor on BidValueParameter, it is possible that after population
        ///       the property is still null so the consumer must first check whether VisibilityParameterId != BidValueParameter.NEW_BID_PARAMETER_ID
        ///       before accessing
        /// 
        /// NOTE: BidParameter.Update does not update the attributes on the VisibilityParameter object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the VisibilityParameter property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidValueParameter VisibilityParameter
        {
            get
            {
                if (mVisibilityParameter == null)
                {
                    throw new InvalidOperationException ("BidValueParameter.VisibilityParameter property must be set before accessing.");
                }

                return mVisibilityParameter;
            }
            set
            {
                mVisibilityParameter = value;
            }
        }

        /// <summary>
        /// Gets/sets the BidValueParameter object associated as the active parameter for this BidParameter
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface. 
        /// 
        /// NOTE: Because there is no ID constructor on BidValueParameter, it is possible that after population
        ///       the property is still null so the consumer must first check whether ActiveParameterId != BidValueParameter.NEW_BID_PARAMETER_ID
        ///       before accessing
        /// 
        /// NOTE: BidParameter.Update does not update the attributes on the ActiveParameter object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the ActiveParameter property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidValueParameter ActiveParameter
        {
            get
            {
                if (mActiveParameter == null)
                {
                    throw new InvalidOperationException ("BidValueParameter.ActiveParameter property must be set before accessing.");
                }

                return mActiveParameter;
            }
            set
            {
                mActiveParameter = value;
            }
        }

        #endregion // Properties


        #endregion  // User Supplied Code
    }
}
