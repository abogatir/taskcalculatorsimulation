﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskCalculation.Core.Mocks;

namespace TaskCalculation.Core.Classes
{
    public class TaskCalculator
    {

        /// <summary>
        /// Contains output data from the results of calculations
        /// </summary>
        public class Output
        {
            /// <summary>
            /// Validation and general error messages encountered while calculating task
            /// </summary>
            /// 
            /// <remarks>Unexpected exceptions are not communicated through this list</remarks>
            public List<string> ErrorMessages { get; set; }

            /// <summary>
            /// The Task overview data ready to have final summations performed and be saved
            /// </summary>
            public BidOverview Overview { get; set; }

            public Output()
            {
                ErrorMessages = new List<string>();
            }
        }

        #region Properties

        private BidScenario Scenario { get; set; }
        private int BidFunctionalServiceAreaId { get; set; }
        private int BidServiceId { get; set; }
        private int BidActivityId { get; set; }
        private BidTask Task { get; set; }
        private decimal FeeDiscount { get; set; }
        private DateTime CurrentDateTime { get; set; }
        private bool IndividualTaskCalculation { get; set; }

        /// <summary>
        /// Used as transient data containers for cost and price information
        /// that will drive the task BidOverview. All costs and prices are based on 
        /// price drivers.
        /// </summary>
        private List<BidOverview> OverviewDrivers { get; set; }

        private Output Result { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates a TaskCalculator with the specified arguments
        /// </summary>
        /// <param name="scenario">Scenario containing Task</param>
        /// <param name="bidFunctionalServiceAreaId">ServiceArea containing Task</param>
        /// <param name="bidServiceId">Service containing Task</param>
        /// <param name="bidActivityId">Activity containing Task</param>
        /// <param name="task">Task to calculate</param>
        /// <param name="currentDateTime">The current date and time</param>    
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection</param>
        public TaskCalculator(BidScenario scenario,
                              int bidFunctionalServiceAreaId,
                              int bidServiceId,
                              int bidActivityId,
                              BidTask task,
                              DateTime currentDateTime)
        {
            Scenario = scenario;
            BidFunctionalServiceAreaId = bidFunctionalServiceAreaId;
            BidServiceId = bidServiceId;
            BidActivityId = bidActivityId;
            Task = task;
            CurrentDateTime = currentDateTime;
            Result = new Output();
            OverviewDrivers = new List<BidOverview>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates and saves a single BidPriceDriver and BidParameters needed to determine
        /// within the context of a task task
        /// </summary>
        /// 
        /// <param name="scenario">Scenario containing the PriceDriver Task</param>
        /// <param name="priceDriver"></param>
        /// <param name="currentDateTime">The current date and time</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection</param>
        /// 
        /// <remarks>
        /// The full WBS tree is used to provide a rich context for each calculation 
        /// and to populate the BidOverview ancestry. 
        /// </remarks>
        public static Output CalculateBidPriceDriver(BidScenario scenario,
                                                     BidPriceDriver priceDriver,
                                                     DateTime currentDateTime)
        {
            BidTask task = Enumerable.FirstOrDefault<BidTask>(MockProvider.BidPriceDriverBidTasks, x => x.BidTaskId == priceDriver.BidTaskId);

            BidTaskTree tree = MockProvider.BidTaskTree;

            TaskCalculator calculator = new TaskCalculator(scenario,
                                                           tree.BidFunctionalServiceAreaId,
                                                           tree.BidServiceId,
                                                           tree.BidActivityId,
                                                           task,
                                                           currentDateTime);

            calculator.IndividualTaskCalculation = true;
            calculator.Calculate(priceDriver);

            return calculator.Result;
        }

        /// <summary>
        /// Calculates all BidPriceDrivers and BidParameters needed to determine
        /// total cost and total price for the specified task
        /// </summary>
        /// 
        /// <param name="scenario">Scenario containing Task</param>
        /// <param name="bidFunctionalServiceAreaId">ServiceArea containing Task</param>
        /// <param name="bidServiceId">Service containing Task</param>
        /// <param name="bidActivityId">Activity containing Task</param>
        /// <param name="task">Task to calculate</param>
        /// <param name="currentDateTime">The current date and time</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection</param>
        /// 
        /// <remarks>
        /// The full WBS tree is used to provide a rich context for each calculation 
        /// and to populate the BidOverview ancestry. 
        /// </remarks>
        public static Output Calculate(BidScenario scenario,
                              int bidFunctionalServiceAreaId,
                              int bidServiceId,
                              int bidActivityId,
                              BidTask task,
                              DateTime currentDateTime)
        {
            TaskCalculator calculator = new TaskCalculator(scenario,
                                                           bidFunctionalServiceAreaId,
                                                           bidServiceId,
                                                           bidActivityId,
                                                           task,
                                                           currentDateTime);

            calculator.Calculate();

            return calculator.Result;
        }

        /// <summary>
        /// Calculates all BidPriceDrivers and BidParameters needed to determine
        /// total cost and total price for the specified task
        /// </summary>
        /// 
        /// <param name="scenario">Scenario containing Task</param>
        /// <param name="bidFunctionalServiceAreaId">ServiceArea containing Task</param>
        /// <param name="bidServiceId">Service containing Task</param>
        /// <param name="bidActivityId">Activity containing Task</param>
        /// <param name="task">Task to calculate</param>
        /// <param name="currentDateTime">The current date and time</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection</param>
        /// 
        /// <remarks>
        /// The full WBS tree is used to provide a rich context for each calculation 
        /// and to populate the BidOverview ancestry. 
        /// </remarks>
        public static Output CalculateIndividualTask(BidScenario scenario,
                              int bidFunctionalServiceAreaId,
                              int bidServiceId,
                              int bidActivityId,
                              BidTask task,
                              DateTime currentDateTime)
        {
            TaskCalculator calculator = new TaskCalculator(scenario,
                                                           bidFunctionalServiceAreaId,
                                                           bidServiceId,
                                                           bidActivityId,
                                                           task,
                                                           currentDateTime);

            calculator.IndividualTaskCalculation = true;
            calculator.Calculate();

            return calculator.Result;
        }

        /// <summary>
        /// Calculates the expense, fee, or effort for a task and all of its price driver and saves the calculated values
        /// </summary>    
        public void Calculate()
        {
            // Recalculat BidTask dates
            CalculateBidTaskDates();

            //Short-circuit
            if (Result.ErrorMessages.Count > 0)
            {
                return;
            }

            //Set the fee discount percent used by price driver calculator
            FeeDiscount = GetFeeDiscountPercent();

            //Short-circuit
            if (Result.ErrorMessages.Count > 0)
            {
                return;
            }

            // Load price drivers
            IEnumerable<BidPriceDriver> bidPriceDrivers;


            //If only calculating an individual task, do not use batch manager, which loads all for the scenario
            bidPriceDrivers = BidPriceDrivers.BidPriceDriversList;


            //Calculate each price driver
            foreach (BidPriceDriver priceDriver in bidPriceDrivers)
            {

                // Calculate BidPriceDriver price/cost/effort and save it
                CalculateAndSaveBidPriceDriver(priceDriver);

                //Break out of calculation loop if an error message is returned
                if (Result.ErrorMessages.Count > 0)
                    break;
            }

            //Build the task overview that is returned on the task calculation output
            //NOTE: The overview is not saved inside the TaskCalculator; the consumer is responsible for that.
            BuildBidOverview(bidPriceDrivers.ToList());
        }

        /// <summary>
        /// Calculates a single price driver within the context of a Task and saves its value
        /// </summary>    
        public void Calculate(BidPriceDriver priceDriver)
        {
            // Recalculat BidTask dates
            CalculateBidTaskDates();

            //Short-circuit
            if (Result.ErrorMessages.Count > 0)
            {
                return;
            }

            // Calculate BidPriceDriver price/cost/effort and save it
            CalculateAndSaveBidPriceDriver(priceDriver);
        }

        /// <summary>
        /// Calculates the expense, fee, or effort for a price driver and saves the calculated values
        /// </summary>        
        /// <param name="priceDriver">Price driver to calculate</param>
        /// 
        /// <returns>A list of error messages</returns>
        private void CalculateAndSaveBidPriceDriver(BidPriceDriver priceDriver)
        {
            PriceDriverCalculator.Output output;
            BidPriceDriverCountry bpdCountry;

            output = PriceDriverCalculator.Calculate(Scenario,
                                                     priceDriver,
                                                     Task,
                                                     CurrentDateTime,
                                                     FeeDiscount);

            //Short-circuit if error occurs
            if (output.ErrorMessages.Count > 0)
            {
                Result.ErrorMessages.AddRange(output.ErrorMessages);
                return;
            }

            #region Map Results and Save BPD

            ////Only copy price driver if a calculation actually changed
            //if (output.ValuesHaveChanged(priceDriver))
            //{
            //  // Map output back to BPD object
            //  output.CopyValuesTo(priceDriver);
            //}
            ////Save BidPriceDriver with calculated totals
            //DSBidServices.SaveBidPriceDriver(priceDriver,
            //                                 DbConnection,
            //                                 DbTransaction);

            // Map output back to BPD object
            output.CopyValuesTo(priceDriver);


            if (priceDriver.UsesCountrySpecificPerUnit)
            {

                //  Map country outputs back to BPD country objects
                foreach (PriceDriverCalculator.Output countryOutput in output.CountrySpecificOutput)
                {
                    int outputCountryId = countryOutput.CountryId.Value;

                    //Find the country in bpdCountries
                    bpdCountry = priceDriver.BidPriceDriverCountries.Single(
                      bpdc => bpdc.CountryId == outputCountryId);

                    //Don't save to database unless something has changed
                    if (countryOutput.ValuesHaveChanged(bpdCountry))
                    {
                        countryOutput.CopyValuesTo(bpdCountry);


                    }
                }
            }
            else if (priceDriver.PriceDriverType == DBProperties.enuPriceDriverType.SERVICE_HOURLY &&
                     priceDriver.WorkAllocationMethod == DBProperties.enuWorkAllocationMethod.FTE_AND_LOCATION)
            {
                foreach (PriceDriverCalculator.Output fteOutput in output.FTESpecificOutput)
                {
                    string buCode = fteOutput.BuCode;

                    var matchingFte = priceDriver.BidPriceDriverFTEs.FirstOrDefault(
                      f => f.BuCode == buCode);

                    if (matchingFte != null)
                    {
                        //Don't save to database unless something has changed
                        if (fteOutput.ValuesHaveChanged(matchingFte))
                        {
                            fteOutput.CopyValuesTo(matchingFte);
                        }
                    }
                }
            }

            #endregion

            //Calculation BidPriceDriverWorkAllocation for service drivers
            if (priceDriver.PriceDriverType == DBProperties.enuPriceDriverType.SERVICE_HOURLY)
            {
                CalculateWorkAllocation(priceDriver);
            }
            else
            {
                //Otherwise, add overview drivers for fee and expense
                if (priceDriver.UsesCountrySpecificPerUnit)
                {
                    //beware here
                    foreach (var pdCountry in priceDriver.BidPriceDriverCountries)
                    {
                        AddOverviewDriver(priceDriver.PriceDriverType, pdCountry.TotalPriceOutput, pdCountry.TotalCostOutput, 0);
                    }
                }
                else
                {
                    AddOverviewDriver(priceDriver.PriceDriverType, priceDriver.TotalPriceOutput, priceDriver.TotalCostOutput, 0);
                }
            }
        }

        /// <summary>
        /// Calculates work allocation for the price driver
        /// </summary>    
        private void CalculateWorkAllocation(BidPriceDriver priceDriver)
        {
            PriceDriverWorkAllocationCalculator.Output allocationOutput;

            //Use the allocation calculator to build a list of price driver work allocations
            allocationOutput =
              PriceDriverWorkAllocationCalculator.CalculatePriceDriverWorkAllocations(Scenario.BidScenarioId,
                                                                                      BidFunctionalServiceAreaId,
                                                                                      BidServiceId,
                                                                                      BidActivityId,
                                                                                      Task,
                                                                                      priceDriver,
                                                                                      CurrentDateTime,
                                                                                      IndividualTaskCalculation);

            //Save each price driver work allocation
            bool allocationChanged = false;
            foreach (var allocation in allocationOutput.PriceDriverWorkAllocations)
            {
                //Preclude closure issues
                var allocationLocal = allocation;

                //Determine if the allocation already existed and if it has changed
                var originalAllocation = allocationOutput.OriginalPriceDriverWorkAllocations.
                  SingleOrDefault(op => op.BidPriceDriverWorkAllocationId == allocationLocal.BidPriceDriverWorkAllocationId);

                //Only save if the allocation is new or has changed
            }

            foreach (var allocation in allocationOutput.PriceDriverWorkAllocations)
            {
                AddOverviewDriver(DBProperties.enuPriceDriverType.SERVICE_HOURLY, allocation.TotalPrice, 0, allocation.TotalEffort);
            }

            //Add a task overview driver for each cost
            foreach (var allocation in allocationOutput.PriceDriverWorkAllocationCosts)
            {
                AddOverviewDriver(DBProperties.enuPriceDriverType.SERVICE_HOURLY, 0, allocation.TotalPrice, 0.00M);
            }
        }

        /// <summary>
        /// Add a price and/or cost to the overview drivers 
        /// </summary>
        /// <param name="type">PriceDriverType</param>
        /// <param name="price">Price to add; use 0.00 if unknown</param>
        /// <param name="cost">Cost to add; use 0.00 if unknown</param>
        /// <param name="effortHours">Effort in hours to add, use 0.00 if unknown</param>
        private void AddOverviewDriver(DBProperties.enuPriceDriverType type,
                                       decimal price,
                                       decimal cost,
                                       decimal effortHours)
        {
            BidOverview overview = new BidOverview();
            overview.TotalEffort = effortHours;

            switch (type)
            {
                case DBProperties.enuPriceDriverType.SERVICE_HOURLY:
                    overview.TotalServicePrice = Math.Round(price, 2);
                    overview.TotalServiceCost = Math.Round(cost, 2);
                    break;
                case DBProperties.enuPriceDriverType.FEE:
                    overview.TotalFeePrice = Math.Round(price, 2);
                    overview.TotalFeeCost = Math.Round(cost, 2);
                    break;
                case DBProperties.enuPriceDriverType.EXPENSE:
                    overview.TotalExpensePrice = Math.Round(price, 2);
                    overview.TotalExpenseCost = Math.Round(cost, 2);
                    break;
            }

            OverviewDrivers.Add(overview);
        }

        /// <summary>
        /// Builds an overview object for the BidTask and assigns it to the calculation results
        /// </summary>
        private void BuildBidOverview(List<BidPriceDriver> priceDrivers)
        {
            Result.Overview = new BidOverview();

            Result.Overview.CurrencyCode = Scenario.OutputCurrencyCode;
            Result.Overview.BidScenarioId = Scenario.BidScenarioId;
            Result.Overview.BidFunctionalServiceAreaId = BidFunctionalServiceAreaId;
            Result.Overview.BidServiceId = BidServiceId;
            Result.Overview.BidActivityId = BidActivityId;
            Result.Overview.BidTaskId = Task.BidTaskId;

            Result.Overview.TotalEffort = OverviewDrivers.Sum(b => b.TotalEffort);
            Result.Overview.TotalServicePrice = OverviewDrivers.Sum(b => b.TotalServicePrice);
            Result.Overview.TotalServiceCost = OverviewDrivers.Sum(b => b.TotalServiceCost);
            Result.Overview.TotalFeePrice = OverviewDrivers.Sum(b => b.TotalFeePrice);
            Result.Overview.TotalFeeCost = OverviewDrivers.Sum(b => b.TotalFeeCost);
            Result.Overview.TotalExpensePrice = OverviewDrivers.Sum(b => b.TotalExpensePrice);
            Result.Overview.TotalExpenseCost = OverviewDrivers.Sum(b => b.TotalExpenseCost);

            Result.Overview.PriceDrivers = priceDrivers;
        }

        /// <summary>
        /// Gets the fee discount percent that should be used to calculate the TAsk
        /// </summary>
        /// <returns>Either the overridden or the default fee discount percent</returns>
        private decimal GetFeeDiscountPercent()
        {
            //mocked
            return 0;
        }

        #region Calculate BidTask Dates

        /// <summary>
        /// Calculates the bid task start and end date
        /// </summary>
        private void CalculateBidTaskDates()
        {

            Task.StartDate = new DateTime(2011, 7, 30);
            Task.EndDate = new DateTime(2011, 12, 30);
        }

        #endregion //Calculate BidTask Dates

        #endregion //Methods

    }
}
