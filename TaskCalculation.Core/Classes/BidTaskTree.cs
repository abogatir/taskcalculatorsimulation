﻿namespace TaskCalculation.Core.Classes
{
    public class BidTaskTree
    {

        #region Constants

        // The following constants are used for databinding to properties.

        public const string BID_TASK_ID_PROPERTY = "BidTaskId";
        public const string BID_ACTIVITY_ID_PROPERTY = "BidActivityId";
        public const string BID_SERVICE_ID_PROPERTY = "BidServiceId";
        public const string BID_FUNCTIONAL_SERVICE_AREA_ID_PROPERTY = "BidFunctionalServiceAreaId";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";


        protected const string BID_TASK_TABLE_ALIAS = "b";
        const string BID_SCENARIO_TABLE_ALIAS = "bs";
        const string BID_ACTIVITY_TABLE_ALIAS = "ba";
        const string BID_SERVICE_TABLE_ALIAS = "bsrv";
        const string BID_FUNCTIONAL_SERVICE_AREA_TABLE_ALIAS = "bfsa";

        #endregion // Constants

        #region Attributes

        private int mBidTaskId;
        private int mBidActivityId;
        private int mBidServiceId;
        private int mBidFunctionalServiceAreaId;
        private int mBidScenarioId;

        #endregion // Attributes

        #region Properties


        public int BidTaskId
        {
            get { return mBidTaskId; }
            set { mBidTaskId = value; }
        }

        public int BidActivityId
        {
            get { return mBidActivityId; }
            set { mBidActivityId = value; }
        }

        public int BidServiceId
        {
            get { return mBidServiceId; }
            set { mBidServiceId = value; }
        }

        public int BidFunctionalServiceAreaId
        {
            get { return mBidFunctionalServiceAreaId; }
            set { mBidFunctionalServiceAreaId = value; }
        }

        public int BidScenarioId
        {
            get { return mBidScenarioId; }
            set { mBidScenarioId = value; }
        }

        #endregion // Properties


    }

}
