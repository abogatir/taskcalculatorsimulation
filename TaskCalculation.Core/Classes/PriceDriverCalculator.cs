﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskCalculation.Core.Mocks;

namespace TaskCalculation.Core.Classes
{
    public partial class PriceDriverCalculator
    {

        #region Constants
        private const decimal DEFAULT_UNIT_PARAMETER_VALUE = 1M;
        private const decimal DEFAULT_EFFICIENCY_FACTOR = 0M;
        private const decimal DEFAULT_FREQUENCY_VALUE = 1M;

        #endregion

        #region Properties


        private BidScenario Scenario { get; set; }
        private BidTask Task { get; set; }
        private BidPriceDriver PriceDriver { get; set; }
        private DateTime CurrentDateTime { get; set; }
        private decimal FeeDiscountPercent { get; set; }

        #endregion

        /// <summary>
        /// Constructs a calculator with required data
        /// </summary>
        /// <param name="scenario">The BidScenario to which the task and price driver belong</param>
        /// <param name="bidTask">The Task providing an override context for the PriceDriver</param>
        /// <param name="bidPriceDriver">The Price Driver to calculate</param>
        /// <param name="currentDateTime">The current DateTime to use</param>
        /// <param name="feeDiscountPercent">The FeeDiscountPercent to use in calculations</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection if necessary, null otherwise</param>
        protected PriceDriverCalculator(BidScenario scenario,
                                        BidTask bidTask,
                                        BidPriceDriver bidPriceDriver,
                                        DateTime currentDateTime,
                                        decimal feeDiscountPercent)
        {
            Scenario = scenario;
            PriceDriver = bidPriceDriver;
            Task = bidTask;
            CurrentDateTime = currentDateTime;
            FeeDiscountPercent = feeDiscountPercent;
        }

        /// <summary>Calculates the total expense, fee, or effort for a PriceDriver and
        /// returns and object containing all calculated values
        /// </summary>
        /// 
        /// <param name="scenario">The BidScenario to which the task and price driver belong</param>
        /// <param name="bidPriceDriver">The BidPriceDriver needing calculation</param>
        /// <param name="bidTask">The BidTask to which the price driver belongs</param>
        /// <param name="currentDateTime">The current DateTime to use</param>
        /// <param name="feeDiscountPercent">The FeeDiscountPercent to use in calculations</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection if necessary, null otherwise</param>
        /// 
        /// <returns>An output object populated with all calculated data for a price driver</returns>
        /// 
        /// <remarks>This method does not save any calculated data. Instead it returns all calculated data
        /// in an object graph and lets the consumer decide to persist the calculated fields.</remarks>
        public static Output Calculate(BidScenario scenario,
                                       BidPriceDriver bidPriceDriver,
                                       BidTask bidTask,
                                       DateTime currentDateTime,
                                       decimal feeDiscountPercent)
        {
            PriceDriverCalculator calculator = new PriceDriverCalculator(scenario,
                bidTask,
                bidPriceDriver,
                currentDateTime,
                feeDiscountPercent);

            return calculator.Calculate();
        }

        /// <summary>Calculates the total expense, fee, or effort for a PriceDriver and
        /// returns and object containing all calculated values
        /// </summary>
        /// <returns>An output object populated with all calculated data for a price driver</returns>
        /// 
        /// <remarks>This method does not save any calculated data. Instead it returns all calculated data
        /// in an object graph and lets the consumer decide to persist the calculated fields.</remarks>
        protected Output Calculate()
        {
            //Build the input needed to perform the calculation, 
            // including price driver overrides and country-specific data
            Input input = BuildInput();

            //Calculate the expense, fee, or effort, 
            // including denormalized subtotals and country-specific data
            Output output = CalculateOutput(input);

            return output;
        }

        #region Build Input

        /// <summary> Builds a calculator input object from the specified PriceDriver and Task
        /// </summary>
        /// 
        /// <returns>An input object populated with all data needed to perform price driver calculations</returns>
        private Input BuildInput()
        {
            Input input = new Input();

            //1. Copy data from BidTask
            input.CopyValuesFrom(Task);

            //2. Copy data from PriceDriver
            input.CopyValuesFrom(PriceDriver);

            //TODO done
            //3. Check if an override exists and use its values      
            //if (PriceDriver.BidPriceDriverOverride.BidPriceDriverOverrideId
            //    != BidPriceDriverOverride.NEW_BID_PRICE_DRIVER_OVERRIDE_ID)
            //{
            //    input.CopyValuesFrom (PriceDriver.BidPriceDriverOverride);
            //}

            //4. If country-specific per-unit information exists, build country-specific input for each one.
            if (input.UsesCountrySpecificPerUnit)
            {
                foreach (BidPriceDriverCountry bpdc in PriceDriver.BidPriceDriverCountries)
                {
                    input.CountrySpecificInput.Add(BuildInputPerCountry(input, bpdc));
                }
            }
            //5. If FTE is used, build FTE-specific input for each one
            else if (input.WorkAllocationMethod == DBProperties.enuWorkAllocationMethod.FTE_AND_LOCATION)
            {
                foreach (BidPriceDriverFte fte in PriceDriver.BidPriceDriverFTEs)
                {
                    input.FTESpecificInput.Add(BuildInputPerFte(input, fte));
                }
            }
            //6. If neither special case exists, just get the inflation and exchange rate for monetary drivers.
            else
            {
                if (input.PriceDriverType != DBProperties.enuPriceDriverType.SERVICE_HOURLY)
                {
                    //Set financial rates on fee and expense drivers
                    SetInflationExchangeRateAndDiscount(input);
                }
            }

            //7. Return the built input, including country-specific and FTE children input
            return input;
        }

        /// <summary> Builds a calculator input object for a specific PriceDrivercountry
        /// </summary>
        /// <param name="bidPriceDriverDefaultInput">The calculation input created for the overall PriceDriver</param>
        /// <param name="priceDriverCountry">The PriceDriverCoutnry to build calculation input for</param>
        /// <returns>An input object populated with data needed to perform price driver calculations for a specific PriceDriverCountry</returns>
        private Input BuildInputPerCountry(Input bidPriceDriverDefaultInput,
                                           BidPriceDriverCountry priceDriverCountry)
        {
            Input countryInput;

            //1. Initialize country-specific input with info from generic PriceDriver input
            countryInput = new Input(bidPriceDriverDefaultInput);

            //2. Copy country-specific data from the PriceDriverCountry
            countryInput.CopyValuesFrom(priceDriverCountry);

            //3. Check if a country override exists and use its      
            //TODO done
            //if (priceDriverCountry.BidPriceDriverCountryOverride.BidPriceDriverCountryOverId !=
            //    BidPriceDriverCountryOverride.NEW_BID_PRICE_DRIVER_COUNTRY_OVER_ID)
            //{
            //    countryInput.CopyValuesFrom (priceDriverCountry.BidPriceDriverCountryOverride);
            //}

            //4. Country-specific input should have a CountryParameter that defines the unit value per country
            if (bidPriceDriverDefaultInput.CountryParameter == null)
            {
                countryInput.CountrySpecificUnitParameter = null;
            }
            else
            {
                //TODO done
                //countryInput.CountrySpecificUnitParameter =
                //  EvaluationCacheManager.GetBidParameterCountry (bidPriceDriverDefaultInput.CountryParameter.BidScenarioId,
                //                                                bidPriceDriverDefaultInput.CountryParameter.UniqueName,
                //                                                priceDriverCountry.CountryId,
                //                                                DbConnection,
                //                                                DbTransaction);
                countryInput.CountrySpecificUnitParameter = Enumerable.FirstOrDefault<BidParameterCountry>(BidParameterCountries.CuntryInputCountrySpecificUnitParameters, x => x.CountryId == priceDriverCountry.CountryId);
            }

            //5. Country-specific input should never have nested countries
            countryInput.UsesCountrySpecificPerUnit = false;

            if (countryInput.PriceDriverType != DBProperties.enuPriceDriverType.SERVICE_HOURLY)
            {
                //Set financial rates on fee and expense drivers
                SetInflationExchangeRateAndDiscount(countryInput);
            }

            //6. Return built country-specific input
            return countryInput;
        }

        /// <summary> Builds a calculator input object for a specific PriceDriverFTE
        /// </summary>
        /// <param name="bidPriceDriverDefaultInput">The calculation input created for the overall PriceDriver</param>
        /// <param name="priceDriverFte">The PriceDriverFTE to build calculation input for</param>
        /// <returns>An input object populated with data needed to perform price driver calculations for a specific PriceDriverFTE</returns>
        private Input BuildInputPerFte(Input bidPriceDriverDefaultInput,
                                       BidPriceDriverFte priceDriverFte)
        {
            Input fteInput;

            //1. Initialize country-specific input with info from generic PriceDriver input
            fteInput = new Input(bidPriceDriverDefaultInput);

            //2. Copy country-specific data from the PriceDriverCountry
            fteInput.CopyValuesFrom(priceDriverFte);

            if (fteInput.PriceDriverType != DBProperties.enuPriceDriverType.SERVICE_HOURLY)
            {
                throw new InvalidOperationException("Only service PriceDrivers can use FTE work allocation.");
            }

            //6. Return built country-specific input
            return fteInput;
        }

        /// <summary>
        /// Sets inflation and exchange rate on fee or expense calculation input
        /// </summary>    
        private void SetInflationExchangeRateAndDiscount(Input input)
        {
            //Validate price driver type - Services should use work allocation
            if (input.PriceDriverType == DBProperties.enuPriceDriverType.SERVICE_HOURLY)
            {
                throw new InvalidOperationException(
                  "Services should have inflation and exchange rate calculated through WorkAllocation. BidPriceDriverId:" +
                  input.BidPriceDriverId);
            }
            //TODO done
            //1. Get the exchange rate between the price driver currency and the scenario output currency
            //var exchangeRate = CacheManager.GetBidCurrencyExchange (Scenario.BidScenarioId,
            //                                                        input.CurrencyCode,
            //                                                        Scenario.OutputCurrencyCode,
            //                                                        DbConnection,
            //                                                        DbTransaction);

            //if (exchangeRate.BidCurrencyExchangeOverride.BidCurrencyExchangeOverrideId != AppConstants.NO_ID)
            //{
            //    input.ExchangeRate = exchangeRate.BidCurrencyExchangeOverride.ExchangeRate;
            //}
            //else
            //{
            //    input.ExchangeRate = exchangeRate.ExchangeRate;
            //}
            input.ExchangeRate = BidCurrencyExchangeRates.SetInflationExchangeRateAndDiscountExchangeRate.ExchangeRate;

            //2. Get the currency code to find the default business unit 
            //TODO done
            //var currency = CacheManager.GetCurrency (input.CurrencyCode);
            var currency = new Currency()
            {
                CurrencyCode = "USD",
                Active = true,
                Description = "United States Dollar"
            };

            //3. Get the inflation card for the scenario
            //NOTE: PriceDrivers currently use a single inflation rate for both Cost and Price amounts
            //var inflationCard =
            //  CalculationBatchManager.GetBidInflationCard (Scenario.BidScenarioId,
            //                                              DBProperties.enuInflationCardType.ACTIVE_BILLING_INFLATION_CARD,
            //                                              CurrentDateTime,
            //                                              DbConnection,
            //                                              DbTransaction);
            var inflationCard = MockProvider.BidInflationCardForBidScenario;
            ////4. Get the inflation rate for the price driver's business unit
            //var matchingInflationRate =
            //  CalculationBatchManager.GetBidInflationRates (inflationCard.BidInflationCardId,
            //                                               DbConnection,
            //                                               DbTransaction)
            //                                               .Where (ir => ir.BuCode == currency.InflationBuCode);

            var matchingInflationRate = new List<BidInflationRate>();

            //Validate the inflation rate matching
            if (matchingInflationRate.Count() > 1)
            {
                throw new InvalidOperationException(
                  string.Format("Multiple inflation rates found: BidScenarioId {0} BuCode {1} InflationType {2} ", Scenario.BidScenarioId, currency.InflationBuCode, inflationCard.InflationCardType));
            }

            if (matchingInflationRate.Count() == 0)
            {
                //NOTE: A decision was made to default to an inflation rate of 1.0
                input.InflationRate = 1.0M;
            }
            else
            {
                input.InflationRate = matchingInflationRate.First().AppliedInflationRate;
            }

            //The fee discount percent comes from the active BidRateCard or an overridden value.
            input.FeeDiscountPercent = FeeDiscountPercent;
        }

        #endregion //Build Input

        #region Calculate Output

        /// <summary>Performs expense, fee, and effort calculations for a price driver
        /// and returns an output object containing the calculated values
        /// </summary>
        /// 
        /// <param name="input">The input used to perform price driver calculations</param>
        /// 
        /// <returns>An output object containing all calculated values</returns>
        private Output CalculateOutput(Input input)
        {
            Output output = new Output(input);

            //1. Calculate PriceDriver EfficiencyFactor, it's constant across price driver types
            output.EfficiencyFactor = CalculateEfficiencyFactor(output.TaskEligibleForEfficiency,
                                                                output.EfficiencyParameterId);

            //Calculate subtotals, e.g., FrequencyValue, TotalUnits
            if (output.WorkAllocationMethod == DBProperties.enuWorkAllocationMethod.FTE_AND_LOCATION)
            {
                CalculateOutputForFTE(output);
            }
            else
            {
                CalculateOutputForNonFTE(output);
            }

            //5. Calculate final output totals
            CalculateAndSetTotals(output);

            //6. Return complete calculated output, including country-specific and FTE output
            return output;
        }

        /// <summary>
        /// Calculates output values for price drivers using FTE work allocation
        /// </summary>    
        private void CalculateOutputForFTE(Output output)
        {
            decimal unitParameterValue;
            List<string> tempErrorMessages;

            //1. Calculate Unit value
            tempErrorMessages = CalculateUnitParameterValue(output.UnitParameter,
                                                      out unitParameterValue);

            output.ErrorMessages.AddRange(tempErrorMessages);
            output.UnitParameterValue = unitParameterValue;

            //2. Calculate subtotals for each FTE
            foreach (var fteInput in output.FTESpecificInput)
            {
                output.FTESpecificOutput.Add(CalculateFTESpecificOutput(output, fteInput));
            }

            //3. Calculate FTE-specific output totals   
            foreach (Output fteOutput in output.FTESpecificOutput)
            {
                //Calculate totals on the FTE output
                CalculateAndSetTotals(fteOutput);
            }
        }

        /// <summary>
        /// Calculates output values for price drivers not using FTE work allocation
        /// </summary>  
        private void CalculateOutputForNonFTE(Output output)
        {
            decimal totalFrequencyPeriods;
            decimal unitParameterValue;
            List<string> tempErrorMessages;

            //1. Calculate PriceDriver FrequencyValue for standard drivers
            totalFrequencyPeriods = CalculateTotalFrequencyPeriodsInTask(output.TaskStartDate,
                                                                         output.TaskEndDate,
                                                                         output.FrequencyType);

            //If the frequency type is Once, use the default frequency value
            if (output.FrequencyType == DBProperties.enuFrequencyType.DURATION_OF_STUDY)
            {
                output.FrequencyValue = DEFAULT_FREQUENCY_VALUE;
            }
            else
            {
                output.FrequencyValue = CalculatePriceDriverFrequencyValue(totalFrequencyPeriods,
                                                                           output.FrequencyPeriod,
                                                                           output.RoundingFunction);
            }

            //2. Calculate Unit value and total units     
            if (output.UsesCountrySpecificPerUnit)
            {
                foreach (Input countryInput in output.CountrySpecificInput)
                {
                    output.CountrySpecificOutput.Add(CalculateUnitsPerCountry(output, countryInput));
                }
            }
            else
            {
                tempErrorMessages = CalculateUnitParameterValue(output.UnitParameter,
                                                                out unitParameterValue);

                output.ErrorMessages.AddRange(tempErrorMessages);
                output.UnitParameterValue = unitParameterValue;

                output.TotalUnits = CalculateTotalUnits(output.UnitParameterValue,
                                                        output.FrequencyValue);
            }

            //3. Calculate country-specific output totals     
            if (output.UsesCountrySpecificPerUnit)
            {
                foreach (Output countryOutput in output.CountrySpecificOutput)
                {
                    //Calculate totals on the country output
                    CalculateAndSetTotals(countryOutput);
                }
            }
        }

        /// <summary>Performs unit value and total units calculations for a country-specific price driver
        /// and returns an output object containing the calculated values
        /// </summary>
        /// 
        /// <param name="defaultOutput">The output for the container price driver, with non-country-specific inputs</param>
        /// <param name="countryInput">The input built for this country-specific price driver calculation</param>
        /// 
        /// <returns>An output object containing calculated units values for the specified country price driver</returns>
        private Output CalculateUnitsPerCountry(Output defaultOutput,
                                                Input countryInput)
        {
            Output output = new Output();
            List<string> tempErrorMessages;
            decimal countryParamValue;

            //1. Copy values from container price driver
            output.CopyValuesFrom(defaultOutput);

            //2. Copy values from country-specific input
            output.CopyValuesFrom(countryInput);

            //3. Calculate the unit value for this country parameter
            tempErrorMessages =
              CalculateUnitCountryParameterValue(output.CountrySpecificUnitParameter,
                                                 out countryParamValue);

            output.ErrorMessages.AddRange(tempErrorMessages);
            output.CountryParameterValue = countryParamValue;

            output.TotalUnits = CalculateTotalUnits(output.CountryParameterValue,
                                                    output.FrequencyValue);

            //4. Return calculated country-specific unit value and total units
            return output;
        }

        /// <summary>Performs unit value and total units calculations for a FTE-specific price driver
        /// and returns an output object containing the calculated values
        /// </summary>
        /// 
        /// <param name="defaultOutput">The output for the container price driver, with non-country-specific inputs</param>
        /// <param name="fteInput">The input built for this FTE-specific price driver calculation</param>
        /// 
        /// <returns>An output object containing calculated units values for the specified FTE price driver</returns>

        private Output CalculateFTESpecificOutput(Output defaultOutput,
                                          Input fteInput)
        {
            Output output = new Output();
            decimal totalFrequencyPeriods;

            //1. Copy values from container price driver
            output.CopyValuesFrom(defaultOutput);

            //2. Copy values from FTE-specific input
            output.CopyValuesFrom(fteInput);

            totalFrequencyPeriods = CalculateTotalFrequencyPeriodsInTask(output.TaskStartDate,
                                                                   output.TaskEndDate,
                                                                   output.FrequencyType);

            output.FrequencyValue = CalculatePriceDriverFrequencyValue(totalFrequencyPeriods,
                                                                       output.FrequencyPeriod,
                                                                       output.RoundingFunction);

            if (output.UsesHeadcount)
            {
                output.TotalUnits = CalculateTotalUnits(output.UnitParameterValue,
                                                        output.Headcount.Value,
                                                        output.FrequencyValue);
            }
            else
            {
                output.TotalUnits = CalculateTotalUnits(output.UnitParameterValue,
                                                        output.FrequencyValue);
            }

            //4. Return calculated fte-specific unit value and total units
            return output;
        }

        #endregion //Calculate Output

        #region Calculate PriceDriver SubTotals

        #region Calculate UnitParameterValue

        /// <summary>Calculates the value of the specified unit parameter
        /// </summary>    
        /// <param name="unitParameter">The unit parameter to calculate a value for</param>
        /// <param name="unitParameterValue">The calculated unit parameter value</param>
        /// 
        /// <returns>A List ofError messages</returns>
        private List<string> CalculateUnitParameterValue(BidValueParameter unitParameter,
                                                         out decimal unitParameterValue)
        {
            List<string> errors = new List<string>();
            //TODO clarify (only this branch is used here)
            //If no unit parameter is specified, use the default value
            if (unitParameter == null ||
                unitParameter.BidParameterId == AppConstants.NO_ID)
            {
                unitParameterValue = DEFAULT_UNIT_PARAMETER_VALUE;
                return errors;
            }

            //errors = BSBidServices.TryEvaluateParameter (Task,
            //                                            unitParameter,
            //                                            out unitParameterValue,
            //                                            DbConnection,
            //                                            DbTransaction);
            unitParameterValue = 0;
            return errors;
        }

        #endregion //Calculate UnitParameterValue

        #region Calculate CountryParameterValue

        /// <summary>Calculates the value of the specified unit parameter
        /// </summary>    
        /// <param name="countryParameter">The unit country parameter to calculate a value for</param>
        /// <param name="countryParameterValue">The calculated unit parameter value</param>
        /// 
        /// <returns>A List ofError messages</returns>
        private List<string> CalculateUnitCountryParameterValue(BidParameterCountry countryParameter,
                                                                out decimal countryParameterValue)
        {
            List<string> errors = new List<string>();

            //TODO clarify (swapping evaluating, seems it is simply returns value)

            //If no unit parameter is specified, use the default value
            //if (countryParameter == null ||
            //    countryParameter.BidParameterId == AppConstants.NO_ID)
            //{
            //    countryParameterValue = DEFAULT_UNIT_PARAMETER_VALUE;
            //    return errors;
            //}

            //errors = BSBidServices.TryEvaluateParameter (countryParameter,
            //                                            out countryParameterValue,
            //                                            DbConnection,
            //                                            DbTransaction);
            if (countryParameter.CountryMonths != null && countryParameter.CountryMonths.Count > 0)
            {
                countryParameterValue = countryParameter.CountryMonths.Sum(cm => cm.Value ?? 0);
                List<BidPriceDriverCountryMonth> pdcm = countryParameter.CountryMonths.Select(cm => new BidPriceDriverCountryMonth()
                {
                    MonthNumber = cm.BidParameterCountryMonthId,
                    NumberOfUnits = cm.Value
                }).ToList();

                BidPriceDriverCountry pdc = PriceDriver.BidPriceDriverCountries.FirstOrDefault(p => p.CountryId == countryParameter.CountryId);
                pdc.PriceDriverCountryMonths = pdcm;
                return errors; // Here we count the CountryMonth units rather than the stored value on PriceDriverCountry values.
            }
            countryParameterValue = countryParameter.Value.HasValue ? countryParameter.Value.Value : 0;
            return errors;
        }

        #endregion //Calculate UnitParameterValue

        #region Calculate EfficiencyFactor

        /// <summary>Retrieves the price driver's efficiency parameter and derives the efficiency parameter value    
        /// </summary>    
        /// <param name="taskEligibleForEfficiency">Boolean indicating if the price driver's task is eligible for efficiency</param>
        /// <param name="efficiencyParameterId">Id of the efficiency parameter</param>    
        ///
        /// <returns>The calculated efficiency parameter value or the default unit value</returns>
        private decimal CalculateEfficiencyFactor(bool taskEligibleForEfficiency,
                                                  int efficiencyParameterId)
        {
            BidValueParameter efficiencyParam = null;
            //TODO clarify (seems not used in this scenario)
            //if (efficiencyParameterId == AppConstants.NO_ID)
            //{
            //    efficiencyParam = null;
            //}
            //else
            //{
            //    efficiencyParam = EvaluationCacheManager.GetBidValueParameter (Scenario.BidScenarioId,
            //                                                                  Task.BidTaskId,
            //                                                                  DBProperties.enuWorkBreakdownStructureLevel.TASK,
            //                                                                  efficiencyParameterId,
            //                                                                  DbConnection,
            //                                                                  DbTransaction);
            //}

            return CalculateEfficiencyFactor(taskEligibleForEfficiency,
                                             efficiencyParam);
        }

        /// <summary>Calculates the value of the specified efficiency parameter
        /// </summary>    
        /// <param name="taskEligibleForEfficiency">Boolean indicating if the price driver's task is eligible for efficiency</param>
        /// <param name="efficiencyParameter">The unit efficiency to calculate a value for</param>
        /// 
        /// <returns>The calculated value of the unit parameter or the default efficiency value</returns>
        private decimal CalculateEfficiencyFactor(bool taskEligibleForEfficiency,
                                                  BidValueParameter efficiencyParameter)
        {
            decimal efficiencyFactor = 0;

            // Ignore efficiency if the Task is not eligible for efficiency or the efficiency parameter is null.      
            if (!taskEligibleForEfficiency ||
                efficiencyParameter == null)
            {
                return DEFAULT_EFFICIENCY_FACTOR;
            }
            //TODO clarify (seems not used in this scenario)
            //BSBidServices.TryEvaluateParameter (Task,
            //                                   efficiencyParameter,
            //                                   out efficiencyFactor,
            //                                   DbConnection,
            //                                   DbTransaction);


            return efficiencyFactor;
        }

        #endregion //Calculate EfficiencyFactor

        /// <summary>Calculates the frequency value for the price driver 
        /// based on task duration and frequency arguments
        /// </summary>
        /// <param name="totalFrequencyPeriods">The total frequency periods that exist in this price driver's task duration</param>
        /// <param name="unitFrequencyPeriod">The PriceDriver FrequencyPeriod</param>
        /// <param name="roundingFunction">The PriceDriver RoundingFunction</param>
        /// 
        /// <returns>The calculated price driver frequency value</returns>
        private decimal CalculatePriceDriverFrequencyValue(decimal totalFrequencyPeriods,
                                                           decimal unitFrequencyPeriod,
                                                           DBProperties.enuRoundingFunction roundingFunction)
        {
            decimal unitFrequencyPeriods;

            //If the price driver has 0 for unit frequency period (can't be divisor), 
            // return the default frequency value
            if (unitFrequencyPeriod == 0)
            {
                return DEFAULT_FREQUENCY_VALUE;
            }

            //Calculate the number of unit periods (e.g., number of visits)
            // And round based on the rounding function     
            //TODO clarify (seems not used in this scenario)
            //unitFrequencyPeriods = roundingFunction.Round (totalFrequencyPeriods / unitFrequencyPeriod);
            unitFrequencyPeriods = totalFrequencyPeriods / unitFrequencyPeriod;
            return unitFrequencyPeriods;
        }

        /// <summary>Calculates a price driver's total units
        /// </summary>
        /// <param name="unitsPerPeriod">The units per period</param>
        /// <param name="frequencyValue">The frequency value</param>
        /// 
        /// <returns>The calculated total units</returns>
        private decimal CalculateTotalUnits(decimal unitsPerPeriod,
                                            decimal frequencyValue)
        {
            return unitsPerPeriod * frequencyValue;
        }

        /// <summary>Calculates a price driver's total units with the headcount factor
        /// </summary>
        /// <param name="unitsPerPeriod">The units per period</param>
        /// <param name="headcount">The headcount</param>
        /// <param name="frequencyValue">The frequency value</param>
        /// 
        /// <returns>The calculated total units</returns>
        private decimal CalculateTotalUnits(decimal unitsPerPeriod,
                                            int headcount,
                                            decimal frequencyValue)
        {
            return unitsPerPeriod * headcount * frequencyValue;
        }

        /// <summary>Calculates the total number of periods for the specified frequency type
        /// between the start and end dates.
        /// </summary>
        /// <param name="taskStart">The task start date</param>
        /// <param name="taskEnd">The task end date</param>
        /// <param name="frequencyType">The frequency type</param>
        /// 
        /// <returns>A decimal representing the number of whole and fractional
        /// periods in the date range</returns>
        private decimal CalculateTotalFrequencyPeriodsInTask(DateTime taskStart,
                                                             DateTime taskEnd,
                                                             DBProperties.enuFrequencyType frequencyType)
        {
            decimal totalFrequencyPeriods = 0;

            //Map the frequency to a DateInterval and call DateDiff            
            totalFrequencyPeriods = DateCalculator.DateDiff(frequencyType.ToDateInterval(), taskStart, taskEnd);

            //Ensure duration/periods isn't negative
            if (totalFrequencyPeriods < 0)
            {
                totalFrequencyPeriods = 0;
            }

            return totalFrequencyPeriods;
        }

        #endregion //Calculate PriceDriver SubTotals

        #region Calculate PriceDriver Totals

        /// <summary> Calculates total properties (TotalEffort, TotalPrice, TotalCost) 
        /// for the price driver and sets the corresponding total properties 
        /// on the output argument
        /// </summary>
        /// <param name="output">The output to calculate totals on</param>
        private void CalculateAndSetTotals(Output output)
        {
            //If this is a PriceDriver does not have country-specific data, 
            // calculate the appropriate totals
            if (!output.UsesCountrySpecificPerUnit)
            {
                switch (output.PriceDriverType)
                {
                    //Expense and Fee need total Price & Cost calculated
                    case DBProperties.enuPriceDriverType.EXPENSE:
                    case DBProperties.enuPriceDriverType.FEE:
                        output.TotalCostOutput = CalculateTotalCostOutput(output);
                        output.TotalPriceOutput = CalculateTotalPriceOutput(output);
                        break;

                    case DBProperties.enuPriceDriverType.SERVICE_HOURLY:
                        //Function/Effort need total Effort calculated
                        output.TotalEffortInHours = CalculateTotalEffortInHours(output);
                        break;
                }

            }
        }

        /// <summary>Calculates the total effort for a PriceDriver
        /// </summary>
        /// <param name="output">The calculation output object containing all calculated subtotals </param>
        /// 
        /// <returns>The total effort calculated from the Output subtotals</returns>
        private decimal CalculateTotalEffortInHours(Output output)
        {
            decimal effort;

            effort = output.TotalUnits * output.EffortPerUnit * (1M - output.EfficiencyFactor);
            BidPriceDriverCountry pdc = PriceDriver.BidPriceDriverCountries.FirstOrDefault(p => p.CountryId == output.CountryId);
            // Possibly has no country specific input
            if (pdc != null)
            {
                foreach (BidPriceDriverCountryMonth pdcm in pdc.PriceDriverCountryMonths)
                {
                    pdcm.Effort = (pdcm.NumberOfUnits ?? 0) *
                        output.EffortPerUnit * (1M - output.EfficiencyFactor);
                }
            }

            return effort;
        }

        /// <summary>Calculates the total cost for a PriceDriver
        /// </summary>
        /// <param name="output">The calculation output object containing all calculated subtotals </param>
        /// 
        /// <returns>The total cost calculated from the Output subtotals</returns>
        private decimal CalculateTotalCostOutput(Output output)
        {
            decimal cost;

            cost = output.TotalUnits *
                   output.CostPerUnit *
                   (1M - output.EfficiencyFactor) *
                   output.InflationRate *
                   output.ExchangeRate;
            BidPriceDriverCountry pdc = PriceDriver.BidPriceDriverCountries.FirstOrDefault(p => p.CountryId == output.CountryId);
            foreach (BidPriceDriverCountryMonth pdcm in pdc.PriceDriverCountryMonths)
            {
                pdcm.Cost = (pdcm.NumberOfUnits ?? 0) *
                   output.CostPerUnit *
                   (1M - output.EfficiencyFactor) *
                   output.InflationRate *
                   output.ExchangeRate;
            }
            return cost;
        }

        /// <summary>Calculates the total price for a PriceDriver
        /// </summary>
        /// <param name="output">The calculation output object containing all calculated subtotals </param>
        /// 
        /// <returns>The total price calculated from the Output subtotals</returns>
        private decimal CalculateTotalPriceOutput(Output output)
        {
            decimal price;

            price = output.TotalUnits *
                    output.PricePerUnit *
                    (1M - output.EfficiencyFactor) *
                    output.InflationRate *
                    output.ExchangeRate;

            if (output.PriceDriverType == DBProperties.enuPriceDriverType.FEE)
            {
                price = price * (1M - output.FeeDiscountPercent);
            }
            BidPriceDriverCountry pdc = PriceDriver.BidPriceDriverCountries.FirstOrDefault(p => p.CountryId == output.CountryId);
            foreach (BidPriceDriverCountryMonth pdcm in pdc.PriceDriverCountryMonths)
            {
                pdcm.Price = (pdcm.NumberOfUnits ?? 0) *
                   output.PricePerUnit *
                   (1M - output.EfficiencyFactor) *
                   output.InflationRate *
                   output.ExchangeRate;
            }

            return price;
        }

        #endregion //Calculate PriceDriver Totals

    }

    public partial class PriceDriverCalculator
    {

        /// <summary>
        /// Contains all data needed to perform PriceDriver calculations and 
        /// methods to help map fields from BidPriceDriver and override objects
        /// </summary>
        public class Input
        {

            #region Properties

            public int BidPriceDriverId;
            public DBProperties.enuPriceDriverType PriceDriverType;
            public bool UsesCountrySpecificPerUnit;
            public int? CountryId;

            public bool UsesHeadcount;
            public int? Headcount;
            public string BuCode;

            public DateTime TaskStartDate;
            public DateTime TaskEndDate;

            public DBProperties.enuFrequencyType FrequencyType;
            public decimal FrequencyPeriod;
            public DBProperties.enuRoundingFunction RoundingFunction;

            //public int UnitParameterId;
            public BidValueParameter UnitParameter;

            //public int UnitCountryParameterId;
            public BidValueParameter CountryParameter;
            public BidParameterCountry CountrySpecificUnitParameter;

            public bool TaskEligibleForEfficiency;
            public int EfficiencyParameterId;

            public string FunctionCode;
            public DBProperties.enuWorkAllocationMethod WorkAllocationMethod;
            public string CurrencyCode;

            public decimal PricePerUnit;
            public decimal CostPerUnit;
            public decimal EffortPerUnit;

            public decimal ExchangeRate;
            public decimal InflationRate;
            public decimal FeeDiscountPercent;

            public List<Input> CountrySpecificInput;
            public List<Input> FTESpecificInput;

            private List<string> mErrorMessages;
            public List<string> ErrorMessages
            {
                get
                {
                    if (mErrorMessages == null)
                    {
                        mErrorMessages = new List<string>();
                    }

                    return mErrorMessages;
                }
            }

            #endregion

            #region Constructors

            /// <summary>
            /// Constructs default instance
            /// </summary>
            public Input()
            {
                CountrySpecificInput = new List<Input>();
                FTESpecificInput = new List<Input>();
            }

            /// <summary>
            /// Constructs instance with values copied from input argument
            /// </summary>
            /// <param name="input">Input argument to copy values from</param>
            public Input(Input input)
                : this()
            {
                CopyValuesFrom(input);
            }

            #endregion

            #region Mapping Methods

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidTask value)
            {
                TaskStartDate = value.StartDate;
                TaskEndDate = value.EndDate;
                TaskEligibleForEfficiency = value.EligibleForEfficiency;
            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidPriceDriver value)
            {
                BidPriceDriverId = value.BidPriceDriverId;
                PriceDriverType = value.PriceDriverType;
                UsesCountrySpecificPerUnit = value.UsesCountrySpecificPerUnit;
                UsesHeadcount = value.UsesHeadcount;

                FrequencyType = value.MasterFrequencyType;
                RoundingFunction = value.FrequencyRoundingFunction;
                FrequencyPeriod = value.MasterFrequencyPeriod;
                UnitParameter = value.UnitParameter;
                EfficiencyParameterId = value.EfficiencyParameterId;
                FunctionCode = value.FunctionCode;

                //Only service drivers have a fucntion code
                if (value.PriceDriverType == DBProperties.enuPriceDriverType.SERVICE_HOURLY)
                {
                    WorkAllocationMethod = value.WorkAllocationMethod;
                }
                else
                {
                    //Use a default if the price driver sh
                    WorkAllocationMethod = DBProperties.enuWorkAllocationMethod.WORK_LOCATION_AND_PERCENTAGE;
                }

                CostPerUnit = value.MasterCostPerUnit;
                PricePerUnit = value.MasterPricePerUnit;
                EffortPerUnit = value.MasterEffortPerUnit;
                CurrencyCode = value.MasterCurrencyCode;
                ExchangeRate = value.ExchangeRate;
                InflationRate = value.InflationRate;

                UnitParameter = value.UnitParameter;
                CountryParameter = value.CountryParameter;

            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidPriceDriverCountry value)
            {
                CostPerUnit = value.MasterCostPerUnit;
                PricePerUnit = value.MasterPricePerUnit;
                EffortPerUnit = value.MasterEffortPerUnit;
                CountryId = value.CountryId;
                CurrencyCode = value.MasterCurrencyCode;
                ExchangeRate = value.ExchangeRate;
                InflationRate = value.InflationRate;
            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidPriceDriverFte value)
            {
                FrequencyType = value.FrequencyTypeId;
                RoundingFunction = value.FrequencyRoundingFunctionId;
                FrequencyPeriod = value.FrequencyPeriod;
                EfficiencyParameterId = value.Headcount;
                Headcount = value.Headcount;
                BuCode = value.BuCode;
                EffortPerUnit = value.Effort;
            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidPriceDriverCountryOverride value)
            {
                //Only copy values that have actually been overridden -- non-null values
                //Using the ?? operator had minor performance issues
                if (value.CostPerUnit != null)
                {
                    CostPerUnit = value.CostPerUnit.Value;
                }

                if (value.PricePerUnit != null)
                {
                    PricePerUnit = value.PricePerUnit.Value;
                }

                if (value.EffortPerUnit != null)
                {
                    EffortPerUnit = value.EffortPerUnit.Value;
                }

                if (value.CurrencyCode != null)
                {
                    CurrencyCode = value.CurrencyCode;
                }
            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(BidPriceDriverOverride value)
            {

                //Only copy values that have actually been overridden -- non-null values
                //Using the ?? operator had minor performance issues
                if (value.FrequencyType != null)
                {
                    FrequencyType = value.FrequencyType.Value;
                }

                if (value.FrequencyPeriod != null)
                {
                    FrequencyPeriod = value.FrequencyPeriod.Value;
                }

                if (value.FrequencyRoundingFunction != null)
                {
                    RoundingFunction = value.FrequencyRoundingFunction.Value;
                }

                if (value.FunctionCode != null)
                {
                    FunctionCode = value.FunctionCode;
                }

                if (value.CostPerUnit != null)
                {
                    CostPerUnit = value.CostPerUnit.Value;
                }

                if (value.PricePerUnit != null)
                {
                    PricePerUnit = value.PricePerUnit.Value;
                }

                if (value.EffortPerUnit != null)
                {
                    EffortPerUnit = value.EffortPerUnit.Value;
                }

                if (value.CurrencyCode != null)
                {
                    CurrencyCode = value.CurrencyCode;
                }

            }

            /// <summary>
            /// Copy applicable calculation input values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(Input value)
            {
                BidPriceDriverId = value.BidPriceDriverId;
                PriceDriverType = value.PriceDriverType;
                UsesCountrySpecificPerUnit = value.UsesCountrySpecificPerUnit;
                CountryId = value.CountryId;

                UsesHeadcount = value.UsesHeadcount;
                Headcount = value.Headcount;
                BuCode = value.BuCode;

                TaskStartDate = value.TaskStartDate;
                TaskEndDate = value.TaskEndDate;

                FrequencyType = value.FrequencyType;
                FrequencyPeriod = value.FrequencyPeriod;
                RoundingFunction = value.RoundingFunction;

                UnitParameter = value.UnitParameter;
                CountryParameter = value.CountryParameter;
                CountrySpecificUnitParameter = value.CountrySpecificUnitParameter;

                TaskEligibleForEfficiency = value.TaskEligibleForEfficiency;
                EfficiencyParameterId = value.EfficiencyParameterId;

                FunctionCode = value.FunctionCode;
                WorkAllocationMethod = value.WorkAllocationMethod;
                CurrencyCode = value.CurrencyCode;

                PricePerUnit = value.PricePerUnit;
                CostPerUnit = value.CostPerUnit;
                EffortPerUnit = value.EffortPerUnit;
                ExchangeRate = value.ExchangeRate;
                InflationRate = value.InflationRate;
                FeeDiscountPercent = value.FeeDiscountPercent;

                CountrySpecificInput = value.CountrySpecificInput;
                FTESpecificInput = value.FTESpecificInput;
            }
            #endregion
        }

    }
    public partial class PriceDriverCalculator
    {

        /// <summary>Contains all calculated PriceDriver data and 
        /// methods to help map fields from output to BidPriceDriver and BidPriceDriverCountry objects
        /// </summary>
        public class Output : Input
        {
            #region Properties

            public decimal UnitParameterValue;
            public decimal CountryParameterValue;
            public decimal FrequencyValue;
            public decimal TotalUnits;
            public decimal EfficiencyFactor;

            public decimal TotalEffortInHours;
            public decimal TotalPriceOutput;
            public decimal TotalCostOutput;

            public List<Output> CountrySpecificOutput;
            public List<Output> FTESpecificOutput;

            #endregion

            #region Constructors

            /// <summary>
            /// Constructs default instance
            /// </summary>
            public Output()
            {
                CountrySpecificOutput = new List<Output>();
                FTESpecificOutput = new List<Output>();
            }

            /// <summary>
            /// Constructs instance with values copied from output argument
            /// </summary>
            /// <param name="output">Output argument to copy values from</param>
            public Output(Output output)
                : this()
            {
                CopyValuesFrom(output);
            }

            /// <summary>
            /// Constructs instance with values copied from input argument
            /// </summary>
            /// <param name="input">Input argument to copy values from</param>
            public Output(Input input)
                : this()
            {
                CopyValuesFrom(input);
            }

            #endregion

            #region Mapping Methods

            /// <summary>
            /// Copy applicable calculation output values to the specified object
            /// </summary>
            /// <param name="value">Object to copy values to</param>
            public void CopyValuesTo(BidPriceDriver value)
            {
                value.UnitParameterValue = UnitParameterValue;
                value.FrequencyValue = FrequencyValue;
                value.TotalUnits = TotalUnits;
                value.EfficiencyFactor = EfficiencyFactor;
                value.TotalEffortInHours = TotalEffortInHours;
                value.TotalCostOutput = TotalCostOutput;
                value.TotalPriceOutput = TotalPriceOutput;
                value.ExchangeRate = ExchangeRate;
                value.InflationRate = InflationRate;
            }

            /// <summary>
            /// Determines if calculation output values have changed
            /// </summary>
            /// <param name="value">Object with original values to</param>
            public bool ValuesHaveChanged(BidPriceDriver value)
            {
                if (value.BidPriceDriverId != BidPriceDriverId)
                    return true;

                if (value.UnitParameterValue != UnitParameterValue)
                    return true;

                if (value.FrequencyValue != FrequencyValue)
                    return true;

                if (value.TotalUnits != TotalUnits)
                    return true;

                if (value.EfficiencyFactor != EfficiencyFactor)
                    return true;

                if (value.TotalEffortInHours != TotalEffortInHours)
                    return true;

                if (value.TotalCostOutput != TotalCostOutput)
                    return true;

                if (value.TotalPriceOutput != TotalPriceOutput)
                    return true;

                if (value.ExchangeRate != ExchangeRate)
                    return true;

                if (value.InflationRate != InflationRate)
                    return true;


                return false;
            }

            /// <summary>
            /// Copy applicable calculation output values to the specified object
            /// </summary>
            /// <param name="value">Object to copy values to</param>
            public void CopyValuesTo(BidPriceDriverCountry value)
            {
                value.CountryParameterValue = CountryParameterValue;
                value.TotalUnits = TotalUnits;
                value.TotalEffortInHours = TotalEffortInHours;
                value.TotalCostOutput = TotalCostOutput;
                value.TotalPriceOutput = TotalPriceOutput;
                value.ExchangeRate = ExchangeRate;
                value.InflationRate = InflationRate;
            }

            /// <summary>
            /// Determines if calculation output values have changed
            /// </summary>
            /// <param name="value">Object with original values to</param>
            public bool ValuesHaveChanged(BidPriceDriverCountry value)
            {
                if (value.BidPriceDriverId != BidPriceDriverId)
                    return true;

                if (value.CountryParameterValue != CountryParameterValue)
                    return true;

                if (value.TotalUnits != TotalUnits)
                    return true;

                if (value.TotalEffortInHours != TotalEffortInHours)
                    return true;

                if (value.TotalCostOutput != TotalCostOutput)
                    return true;

                if (value.TotalPriceOutput != TotalPriceOutput)
                    return true;

                if (value.ExchangeRate != ExchangeRate)
                    return true;

                if (value.InflationRate != InflationRate)
                    return true;

                return false;
            }

            /// <summary>
            /// Copy applicable calculation output values to the specified object
            /// </summary>
            /// <param name="value">Object to copy values to</param>
            public void CopyValuesTo(BidPriceDriverFte value)
            {
                value.FrequencyValue = FrequencyValue;
                value.TotalUnits = TotalUnits;
                value.TotalEffortInHours = TotalEffortInHours;
            }

            /// <summary>
            /// Determines if calculation output values have changed
            /// </summary>
            /// <param name="value">Object with original values to</param>
            public bool ValuesHaveChanged(BidPriceDriverFte value)
            {
                if (value.BidPriceDriverId != BidPriceDriverId)
                    return true;

                if (value.FrequencyValue != FrequencyValue)
                    return true;

                if (value.TotalUnits != TotalUnits)
                    return true;

                if (value.TotalEffortInHours != TotalEffortInHours)
                    return true;

                return false;
            }

            /// <summary>
            /// Copy applicable calculation output values
            /// </summary>
            /// <param name="value">Object to copy values from</param>
            public void CopyValuesFrom(Output value)
            {
                UnitParameterValue = value.UnitParameterValue;
                FrequencyValue = value.FrequencyValue;
                TotalUnits = value.TotalUnits;
                EfficiencyFactor = value.EfficiencyFactor;

                TotalEffortInHours = value.TotalEffortInHours;
                TotalPriceOutput = value.TotalPriceOutput;
                TotalCostOutput = value.TotalCostOutput;
            }

            #endregion
        }

    }

}
