﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCalculation.Core.Classes
{
    public class BidParameterCountryMonth
    {
        //The ID of the BidParameterCountryYear record

        //The index + 1 of this month in the year

        //The value of the country parameter for the given month

        //The default value of the country parameter for the given month
        private decimal? mDefaultValue;

        //The actual BidParameterCountry object referenced by the BidParameterCountryMonth
        private BidParameterCountry mBidParameterCountry;

        //The primary key for the parameter country month
        private int mBidParameterCountryMonthId;

        //The country parameter to which this month entry relates
        private int mBidParameterCountryId;

        //The month number, from start, that this refers to
        private int mMonthNumberFromStart;

        /// <summary> 
        /// Gets or sets The primary key for the parameter country month
        /// </summary> 
        public int BidParameterCountryMonthId
        {
            get
            {
                return mBidParameterCountryMonthId;
            }
            set
            {
                mBidParameterCountryMonthId = value;
            }
        }

        /// <summary> 
        /// Gets or sets The country parameter to which this month entry relates
        /// </summary> 
        public int BidParameterCountryId
        {
            get
            {
                return mBidParameterCountryId;
            }
            set
            {
                mBidParameterCountryId = value;
            }
        }

        /// <summary>
        /// Gets or sets the ID of the BidParameterCountryYear
        /// </summary>
        public int BidParameterCountryYearId { get; set; }

        public int MonthNumberInYear { get; set; }

        public decimal? Value { get; set; }

        /// <summary>
        /// Gets or sets the BidParameterCountry associated to this BidParameterCountryMonth
        /// </summary>
        /// <remarks>
        /// NOTE: This property is used only to facilitate displaying lists in the user interface and possibly in calculations
        /// 
        /// NOTE: BidParameterCountryMonth.Update does not update the attributes on the BidParameterCountry object.
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        /// If the BidParameterCountry property is accessed before setting a value, 
        /// an InvalidOperationException will be thrown.
        /// </exception>
        public BidParameterCountry BidParameterCountry
        {
            get
            {
                if (mBidParameterCountry == null)
                {
                    throw new InvalidOperationException("BidParameterCountryMonth.BidParameterCountry property must be set before accessing.");
                }

                return mBidParameterCountry;
            }
            set
            {
                mBidParameterCountry = value;
            }
        }

        #region Methods
        public override string ToString()
        {
            return "Country: " + mBidParameterCountryId.ToString() + "(" + mBidParameterCountryMonthId + "), Value: " + Value.ToString();
        }
        #endregion Methods

    }
}
