﻿using System;

namespace TaskCalculation.Core.Classes
{
    public abstract class BidParameterOverride
    {
        // NOTE: Since this class is different enough from what is generated, 
        // the whole Generated Code Paradigm is thrown out for this class. Coder Beware.

        #region Constants

        #region Properties
        // The following constants are used for databinding to properties.

        public const string BID_PARAMETER_OVERRIDE_ID_PROPERTY = "BidParameterOverrideId";
        public const string BID_PARAMETER_ID_PROPERTY = "BidParameterId";
        public const string WORK_BREAKDOWN_STRUCTURE_LEVEL_ID_PROPERTY = "WorkBreakdownStructureLevelId";
        public const string WORK_BREAKDOWN_STRUCTURE_LEVEL_PROPERTY = "WorkBreakdownStructureLevel";
        public const string OVERRIDE_REASON_PROPERTY = "OverrideReason";
        public const string EMPLOYEE_ID_PROPERTY = "EmployeeId";
        public const string OVERRIDE_DATE_PROPERTY = "OverrideDate";

        #endregion

        #region Default Values

        public const int NEW_BID_PARAMETER_OVERRIDE_ID = AppConstants.NO_ID;

        // The following constants are used to define the default values for the instance variables.

        protected const int BID_PARAMETER_OVERRIDE_ID_DEFAULT_VALUE = NEW_BID_PARAMETER_OVERRIDE_ID;
        protected const int BID_PARAMETER_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const int WORK_BREAKDOWN_STRUCTURE_LEVEL_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected const DBProperties.enuWorkBreakdownStructureLevel WORK_BREAKDOWN_STRUCTURE_LEVEL_DEFAULT_VALUE = DBProperties.enuWorkBreakdownStructureLevel.BID_SCENARIO;
        protected const string OVERRIDE_REASON_DEFAULT_VALUE = "";
        protected const int EMPLOYEE_ID_DEFAULT_VALUE = AppConstants.NO_ID;
        protected static readonly DateTime OVERRIDE_DATE_DEFAULT_VALUE = AppConstants.NULL_DATE_VALUE;

        #endregion

        #region Database Values

        protected const string BID_PARAMETER_OVERRIDE_TABLE_ALIAS = "bpo";

        protected const string BID_PARAMETER_TABLE_ALIAS = "bp";
        protected const string BID_SCENARIO_TABLE_ALIAS = "bsc";
        protected const string BID_FUNCTIONAL_SERVICE_AREA_TABLE_ALIAS = "bfsa";
        protected const string BID_SERVICE_TABLE_ALIAS = "bse";
        protected const string BID_ACTIVITY_TABLE_ALIAS = "ba";
        protected const string BID_TASK_TABLE_ALIAS = "bt";

        

        #endregion

        #endregion // Constants

        #region Attributes

        #endregion // Attributes

        #region Properties

        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidParameterOverrideId { get; set; }

        /// <summary> 
        /// Gets or sets The bid parameter being overridden
        /// </summary> 
        public int BidParameterId { get; set; }

        /// <summary> 
        /// Gets or sets The work breakdown structure level at which the parameter is being overridden
        /// </summary> 
        public int WorkBreakdownStructureLevelId { get; private set; }

        /// <summary>
        /// Gets or sets the WorkBreakdownStructureLevel type that corresponds to WorkBreakdownStructureLevelId
        /// </summary>
        public DBProperties.enuWorkBreakdownStructureLevel WorkBreakdownStructureLevel { get; private set; }

        /// <summary> 
        /// Gets or sets The comment as to why the override was made
        /// </summary> 
        public string OverrideReason { get; set; }

        /// <summary> 
        /// Gets or sets The employee who made the override
        /// </summary> 
        public int EmployeeId { get; set; }

        /// <summary> 
        /// Gets or sets The date on which the override occurred
        /// </summary> 
        public DateTime OverrideDate { get; set; }

        #endregion // Properties

    }
}
