﻿namespace TaskCalculation.Core.Classes
{
    public class BidValueParameterOverride : BidParameterOverride
    {
        #region Constants

        public const string VALUE_PROPERTY = "Value";

        protected const decimal VALUE_DEFAULT_VALUE = AppConstants.NO_VALUE;

        #endregion // Constants

        #region Attributes

        //The numeric value, if applicable, of the override
        private decimal mValue;

        #endregion // Attributes

        #region Properties

        /// <summary> 
        /// Gets or sets The numeric value, if applicable, of the override
        /// </summary> 
        public decimal Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        #endregion // Properties
    }
}
