﻿using System;
using System.Text.RegularExpressions;

namespace TaskCalculation.Core.Classes
{
    public class TextFormatter
    {

        private static readonly Regex LineEndingRegex = new Regex(@"(\r\n|\r|\n)+", RegexOptions.Compiled);

        #region Number Formatters

        /// <summary>
        /// Multiplies the parameter by 100 and returns it as a string.
        /// </summary>
        /// <param name="value">The value to format</param>
        /// <returns>The formatted value</returns>
        public static string GetPercentageText(decimal value)
        {
            decimal rounded;

            rounded = Math.Round(value * 100, 2);

            if (rounded == 0)
            {
                return "0.00";
            }
            else
            {
                return rounded.ToString("##.00");
            }
        }

        /// <summary>
        /// Multiplies the parameter by 100 and returns it as a string with an appended '%' sign
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetPercentageTextWithSign(decimal value)
        {
            return GetPercentageText(value) + "%";
        }

        public static string GetCurrencyText(decimal value, string currencyCode)
        {
            if (!string.IsNullOrEmpty(currencyCode))
            {
                return (value.ToString(SiteConstants.CURRENCY_DISPLAY) + " " + currencyCode);
            }

            return value.ToString(SiteConstants.CURRENCY_DISPLAY);
        }

        #endregion // Number Formatters

        #region Style Formatters



        #endregion // Style Formatters

        #region Text Formatters

        public static string GetOverrideTooltipText(DBProperties.enuWorkBreakdownStructureLevel workBreakdownStructureLevel)
        {
            const string FORMAT = "Overridden at {0} level";
            switch (workBreakdownStructureLevel)
            {
                case DBProperties.enuWorkBreakdownStructureLevel.BID_SCENARIO:
                    return string.Format(FORMAT, "Scenario");

                case DBProperties.enuWorkBreakdownStructureLevel.FUNCTIONAL_SERVICE_AREA:
                    return string.Format(FORMAT, "Functional Service Area");

                case DBProperties.enuWorkBreakdownStructureLevel.SERVICE:
                    return string.Format(FORMAT, "Service");

                case DBProperties.enuWorkBreakdownStructureLevel.ACTIVITY:
                    return string.Format(FORMAT, "Activity");

                case DBProperties.enuWorkBreakdownStructureLevel.TASK:
                    return string.Format(FORMAT, "Task");

                case DBProperties.enuWorkBreakdownStructureLevel.PRICE_DRIVER:
                    return string.Format(FORMAT, "Price Driver");

                case DBProperties.enuWorkBreakdownStructureLevel.PRICE_DRIVER_COUNTRY:
                    return string.Format(FORMAT, "Country-Specific Price Driver");

                default:
                    throw new ArgumentOutOfRangeException("workBreakdownStructureLevel");
            }
        }

        /// <summary>
        /// Replaces \r\n|\r|\n within a string with an html br tag
        /// </summary>
        /// <param name="source">Original String</param>
        /// <returns>String</returns>
        public static string ReplaceWithBR(string source)
        {
            return LineEndingRegex.Replace(source, "<br />");
        }

        #endregion // Text Formatters
    }

}
