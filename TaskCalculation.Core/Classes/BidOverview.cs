﻿using System;
using System.Collections.Generic;

namespace TaskCalculation.Core.Classes
{
    public class BidOverview
    {
        // NOTE: Table Comments - Contains price and cost information for the bid and each level; NOTE: this may not be the price the client is paying.  The client grid output must be used to retrieve that information.

        //WBSLevelId - renamed and changed datatype
        #region Generated Code

        #region Generated Property Name Constants

        // The following constants are used for databinding to properties.

        public const string BID_OVERVIEW_ID_PROPERTY = "BidOverviewId";
        public const string TOTAL_SERVICE_PRICE_PROPERTY = "TotalServicePrice";
        public const string TOTAL_FEE_PRICE_PROPERTY = "TotalFeePrice";
        public const string TOTAL_EXPENSE_PRICE_PROPERTY = "TotalExpensePrice";
        public const string TOTAL_PRICE_PROPERTY = "TotalPrice";
        public const string TOTAL_SERVICE_COST_PROPERTY = "TotalServiceCost";
        public const string TOTAL_FEE_COST_PROPERTY = "TotalFeeCost";
        public const string TOTAL_EXPENSE_COST_PROPERTY = "TotalExpenseCost";
        public const string TOTAL_COST_PROPERTY = "TotalCost";
        public const string CURRENCY_CODE_PROPERTY = "CurrencyCode";
        public const string REGION_ID_PROPERTY = "RegionId";
        public const string SERVICE_MARGIN_PROPERTY = "ServiceMargin";
        public const string FEE_MARGIN_PROPERTY = "FeeMargin";
        public const string EXPENSE_MARGIN_PROPERTY = "ExpenseMargin";
        public const string TOTAL_MARGIN_PROPERTY = "TotalMargin";
        //public const string WBS_LEVEL_ID_PROPERTY = "WbsLevelId";
        public const string BID_FUNCTIONAL_SERVICE_AREA_ID_PROPERTY = "BidFunctionalServiceAreaId";
        public const string BID_SERVICE_ID_PROPERTY = "BidServiceId";
        public const string BID_ACTIVITY_ID_PROPERTY = "BidActivityId";
        public const string BID_TASK_ID_PROPERTY = "BidTaskId";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";
        public const string TOTAL_EFFORT_PROPERTY = "TotalEffort";

        #endregion // Generated Property Name Constants


        #endregion // Generated Database Constants

        #region Generated Fields

        //The primary key for the table

        //The total price for all services
        private decimal mTotalServicePrice;

        //The total price for all fees
        private decimal mTotalFeePrice;

        //The total price for all expenses
        private decimal mTotalExpensePrice;

        //The total price for the bid, as calculated
        private decimal mTotalPrice;

        //The total cost for all services
        private decimal mTotalServiceCost;

        //The total cost for all fees
        private decimal mTotalFeeCost;

        //The total cost for all expenses
        private decimal mTotalExpenseCost;

        //The total cost for the bid, as calculated
        private decimal mTotalCost;

        //The currency for the price and costs

        //The region for the record, if necessary

        //The profit margin, as a percentage
        private decimal mServiceMargin;

        //The profit margin, as a percentage
        private decimal mFeeMargin;

        //The profit margin, as a percentage, usually 0%
        private decimal mExpenseMargin;

        //The profit margin, as a percentage
        private decimal mTotalMargin;


        //The total effort in hours
        private decimal mTotalEffort;

        #endregion // Generated Fields

        #region Generated Properties

        /// <summary> 
        /// Gets or sets The primary key for the table
        /// </summary> 
        public int BidOverviewId { get; set; }

        /// <summary> 
        /// Gets or sets The currency for the price and costs
        /// </summary> 
        public string CurrencyCode { get; set; }

        /// <summary> 
        /// Gets or sets The region for the record, if necessary
        /// </summary> 
        public int RegionId { get; set; }

        /// <summary> 
        /// Gets or sets The functional service area at which it was overridden, if applicable
        /// </summary> 
        public int BidFunctionalServiceAreaId { get; set; }

        /// <summary> 
        /// Gets or sets The service at which it was overridden, if applicable
        /// </summary> 
        public int BidServiceId { get; set; }

        /// <summary> 
        /// Gets or sets The activity at which it was overridden, if applicable
        /// </summary> 
        public int BidActivityId { get; set; }

        /// <summary> 
        /// Gets or sets The task at which it was overridden, if applicable
        /// </summary> 
        public int BidTaskId { get; set; }

        /// <summary> 
        /// Gets or sets The bid scenario at which it was overridden
        /// </summary> 
        public int BidScenarioId { get; set; }

        #endregion // Generated Properties

        #region Constants
        public const string WBS_LEVEL_PROPERTY = "WbsLevel";
        #endregion // Constants
        #region Properties


        public List<BidPriceDriver> PriceDrivers;

        //NOTE: The financial properties are rounded when set to ensure consistent 
        // rounding between database and calculation code.

        /// <summary> 
        /// Gets or sets The total price for all services
        /// </summary> 
        public decimal TotalServicePrice
        {
            get { return mTotalServicePrice; }
            set { mTotalServicePrice = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total price for all fees
        /// </summary> 
        public decimal TotalFeePrice
        {
            get { return mTotalFeePrice; }
            set { mTotalFeePrice = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total price for all expenses
        /// </summary> 
        public decimal TotalExpensePrice
        {
            get { return mTotalExpensePrice; }
            set { mTotalExpensePrice = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total price for the bid, as calculated
        /// </summary> 
        public decimal TotalPrice
        {
            get { return mTotalPrice; }
            set { mTotalPrice = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total cost for all services
        /// </summary> 
        public decimal TotalServiceCost
        {
            get { return mTotalServiceCost; }
            set { mTotalServiceCost = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total cost for all fees
        /// </summary> 
        public decimal TotalFeeCost
        {
            get { return mTotalFeeCost; }
            set { mTotalFeeCost = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total cost for all expenses
        /// </summary> 
        public decimal TotalExpenseCost
        {
            get { return mTotalExpenseCost; }
            set { mTotalExpenseCost = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The total cost for the bid, as calculated
        /// </summary> 
        public decimal TotalCost
        {
            get { return mTotalCost; }
            set { mTotalCost = Math.Round(value, 2); }
        }

        /// <summary> 
        /// Gets or sets The profit margin, as a percentage
        /// </summary> 
        public decimal ServiceMargin
        {
            get { return mServiceMargin; }
            set { mServiceMargin = Math.Round(value, 4); }
        }

        /// <summary> 
        /// Gets or sets The profit margin, as a percentage
        /// </summary> 
        public decimal FeeMargin
        {
            get { return mFeeMargin; }
            set { mFeeMargin = Math.Round(value, 4); }
        }

        /// <summary> 
        /// Gets or sets The profit margin, as a percentage, usually 0%
        /// </summary> 
        public decimal ExpenseMargin
        {
            get { return mExpenseMargin; }
            set { mExpenseMargin = Math.Round(value, 4); }
        }

        /// <summary> 
        /// Gets or sets The profit margin, as a percentage
        /// </summary> 
        public decimal TotalMargin
        {
            get { return mTotalMargin; }
            set { mTotalMargin = Math.Round(value, 4); }
        }

        /// <summary> 
        /// Gets or sets The total effort in hours
        /// </summary> 
        public decimal TotalEffort
        {
            get { return mTotalEffort; }
            set { mTotalEffort = Math.Round(value, 2); }
        }
        #endregion // Properties



    }

}
