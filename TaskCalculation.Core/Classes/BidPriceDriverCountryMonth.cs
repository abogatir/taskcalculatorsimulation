using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCalculation.Core.Classes
{
    public class BidPriceDriverCountryMonth
    {
        public int MonthNumber;
        public decimal? NumberOfUnits;
        public decimal Price;
        public decimal Cost;
        public decimal Effort;

        public override string ToString()
        {
            return "Month: " + MonthNumber + ", # " + NumberOfUnits + ", Effort: " + Effort.ToString("0.00") + ", Price: " + Price.ToString("0.00") + ", Cost: " + Cost.ToString("0.00");
        }
    }
}
