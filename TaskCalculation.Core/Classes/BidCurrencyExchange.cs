﻿using System;

namespace TaskCalculation.Core.Classes
{
    public class BidCurrencyExchange
    {

        #region Constants

        #region Default Values

        protected const decimal EXCHANGE_RATE_DEFAULT_VALUE = 1M;

        #endregion // Default Values

        #region Database Constants

        protected const string FROM_CURRENCY_TABLE_ALIAS = "fc";
        protected const string TO_CURRENCY_TABLE_ALIAS = "tc";

        #endregion // Database Constants

        #endregion // Constants

        #region Attributes


        #endregion // Attributes


        #region Generated Property Name Constants

        // The following constants are used for databinding to properties.

        public const string FROM_CURRENCY_CODE_PROPERTY = "FromCurrencyCode";

        public const string TO_CURRENCY_CODE_PROPERTY = "ToCurrencyCode";
        public const string EXCHANGE_RATE_PROPERTY = "ExchangeRate";
        public const string BID_SCENARIO_ID_PROPERTY = "BidScenarioId";
        public const string BID_CURRENCY_EXCHANGE_ID_PROPERTY = "BidCurrencyExchangeId";

        #endregion // Generated Property Name Constants


        #region Generated Properties

        /// <summary> 
        /// Gets or sets The original currency code
        /// </summary> 
        public string FromCurrencyCode { get; set; }

        /// <summary> 
        /// Gets or sets The output currency code
        /// </summary> 
        public string ToCurrencyCode { get; set; }

        /// <summary> 
        /// Gets or sets The exchange rate
        /// </summary> 
        public decimal ExchangeRate { get; set; }

        /// <summary> 
        /// Gets or sets The bid scenario for which the exchange rate applies
        /// </summary> 
        public int BidScenarioId { get; set; }

        /// <summary> 
        /// Gets or sets The primary key
        /// </summary> 
        public int BidCurrencyExchangeId { get; set; }

        #endregion // Generated Properties



    }
}