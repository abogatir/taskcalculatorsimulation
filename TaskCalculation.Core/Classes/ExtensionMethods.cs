﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskCalculation.Core.Classes
{
    public static class ExtensionMethods
    {

        /// <summary>
        /// Returns the decimal rounded to the specified number of places.
        /// </summary>
        /// 
        /// <param name="value">A decimal number to be rounded</param>
        /// <param name="decimals">The number of decimal places in the return value</param>    
        public static decimal PraRound (this decimal value, int decimals)
        {
            return Math.Round (value, decimals, MidpointRounding.AwayFromZero);
        }


        /// <summary>
        /// Returns the decimal rounded to the specified number of places.
        /// </summary>
        /// 
        /// <param name="value">A decimal number to be rounded</param>
        /// <param name="decimals">The number of decimal places in the return value</param>    
        public static decimal? PraRound (this decimal? value, int decimals)
        {
            if (value.HasValue)
            {
                return Math.Round (value.Value,
                                  decimals,
                                  MidpointRounding.AwayFromZero);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the result of the offset calculation provided by the offset value and the offset interval
        /// </summary>
        /// 
        /// <param name="startDate">The start date</param>
        /// <param name="offset">The offset value</param>
        /// <param name="offsetInterval">The offset interval</param>
        /// 
        /// <returns>The result of the offset calculation provided by the offset value and the offset interval</returns>
        public static DateTime Add (this DateTime startDate,
                                   decimal offset,
                                   DBProperties.enuDateInterval offsetInterval)
        {
            return DateCalculator.DateAdd (startDate, offset, offsetInterval);
        }

        /// <summary>
        /// Rounds a decimal value according to the enuRoundingFunction rule
        /// </summary>
        /// <param name="roundingFunction">The enuRoundingFunction rule to use</param>
        /// <param name="value">The decimal value to round</param>
        /// 
        /// <returns>A decimal rounded according to the enuRoundingFunction rule</returns>
        public static decimal Round (this DBProperties.enuRoundingFunction roundingFunction,
                                    decimal value)
        {
            decimal roundedValue;

            switch (roundingFunction)
            {
                case DBProperties.enuRoundingFunction.CEILING:
                    roundedValue = Math.Ceiling (value);
                    break;

                case DBProperties.enuRoundingFunction.FLOOR:
                    roundedValue = Math.Floor (value);
                    break;

                case DBProperties.enuRoundingFunction.ROUND:
                    roundedValue = Math.Round (value, MidpointRounding.AwayFromZero);
                    break;

                case DBProperties.enuRoundingFunction.ROUND_TO_HALF:
                    roundedValue = (Math.Round (value * 2) / 2);
                    break;

                default:
                    throw new NotSupportedException (
                      string.Format ("The specified enuRoundingFunction [{0}] is not supported.", roundingFunction));
            }

            return roundedValue;
        }

        /// <summary>
        /// Converts a DBProperties.enuFrequencyType to its equivalent DBProperties.enuRoundingFunction value
        /// </summary>
        /// <param name="frequencyType">The enuFrequencyType value to convert</param>
        /// 
        /// <returns>The equivalent enuDateInterval value</returns>
        public static DBProperties.enuRoundingFunction ToDefaultRoundingFunction (this DBProperties.enuFrequencyType frequencyType)
        {
            // DEFER: Consider creating a FrequencyType class and moving this function to it
            DBProperties.enuRoundingFunction roundingFunction;

            switch (frequencyType)
            {
                case DBProperties.enuFrequencyType.DURATION_IN_DAYS:
                case DBProperties.enuFrequencyType.DURATION_IN_WEEKS:
                case DBProperties.enuFrequencyType.DURATION_IN_QUARTERS:
                case DBProperties.enuFrequencyType.DURATION_OF_STUDY:
                    roundingFunction = DBProperties.enuRoundingFunction.ROUND;

                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_YEARS:
                    roundingFunction = DBProperties.enuRoundingFunction.CEILING;

                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_MONTHS:
                    roundingFunction = DBProperties.enuRoundingFunction.ROUND_TO_HALF;

                    break;

                default:
                    throw new NotSupportedException (
                      string.Format ("Conversion from enuFrequencyType [{0}] to a enuRoundingFunction value is not supported.", frequencyType));
            }

            return roundingFunction;
        }

        /// <summary>
        /// Converts a DBProperties.enuFrequencyType to its equivalent DBProperties.enuDateInterval value
        /// </summary>
        /// <param name="frequencyType">The enuFrequencyType value to convert</param>
        /// 
        /// <returns>The equivalent enuDateInterval value</returns>
        public static DBProperties.enuDateInterval ToDateInterval (this DBProperties.enuFrequencyType frequencyType)
        {
            DBProperties.enuDateInterval dateInterval;

            switch (frequencyType)
            {
                case DBProperties.enuFrequencyType.DURATION_IN_DAYS:
                    dateInterval = DBProperties.enuDateInterval.DAYS;
                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_MONTHS:
                    dateInterval = DBProperties.enuDateInterval.MONTHS;
                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_QUARTERS:
                    dateInterval = DBProperties.enuDateInterval.QUARTERS;
                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_WEEKS:
                    dateInterval = DBProperties.enuDateInterval.WEEKS;
                    break;

                case DBProperties.enuFrequencyType.DURATION_IN_YEARS:
                    dateInterval = DBProperties.enuDateInterval.YEARS;
                    break;

                case DBProperties.enuFrequencyType.DURATION_OF_STUDY:
                    dateInterval = DBProperties.enuDateInterval.ONCE;
                    break;

                default:
                    throw new NotSupportedException (
                      string.Format ("Conversion from enuFrequencyType [{0}] to a enuDateInterval value is not supported.", frequencyType));
            }

            return dateInterval;
        }


        /// <summary>
        /// Converts a DBProperties.enuDateInterval to its equivalent DBProperties.enuFrequencyType value
        /// </summary>
        /// <param name="dateInterval">The enuDateInterval value to convert</param>
        /// 
        /// <returns>The equivalent enuFrequencyType value</returns>
        public static DBProperties.enuFrequencyType ToFrequencyType (this DBProperties.enuDateInterval dateInterval)
        {
            DBProperties.enuFrequencyType frequencyType;

            switch (dateInterval)
            {
                case DBProperties.enuDateInterval.DAYS:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_IN_DAYS;
                    break;

                case DBProperties.enuDateInterval.MONTHS:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_IN_MONTHS;
                    break;

                case DBProperties.enuDateInterval.QUARTERS:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_IN_QUARTERS;
                    break;

                case DBProperties.enuDateInterval.WEEKS:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_IN_WEEKS;
                    break;

                case DBProperties.enuDateInterval.YEARS:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_IN_YEARS;
                    break;

                case DBProperties.enuDateInterval.ONCE:
                    frequencyType = DBProperties.enuFrequencyType.DURATION_OF_STUDY;
                    break;

                default:
                    throw new NotSupportedException (
                      string.Format ("Conversion from enuDateInterval [{0}] to a enuFrequencyType value is not supported.", dateInterval));
            }

            return frequencyType;
        }

        //public static string ToDelimitedString<T>(this List<T> list, char delimiter)
        //{
        //  const string FORMAT = "{0}{1}";

        //  if (list.Count == 0)
        //  {
        //    throw new InvalidOperationException("Cannot delimit an empty list");
        //  }

        //  StringBuilder sb = new StringBuilder();

        //  for (int i = 0; i < list.Count - 1; i++)
        //  {
        //    sb.AppendFormat(FORMAT, list[i], delimiter);
        //  }
        //  sb.Append(list[list.Count - 1]);

        //  return sb.ToString();
        //}

        /// <summary>
        /// Transforms a list items to delimited string
        /// </summary>
        public static string ToDelimitedString<TSource> (this IEnumerable<TSource> list, char delimiter)
        {
            return CreateDelimitedString (list.ToList (), delimiter);
        }

        /// <summary>
        /// Transforms a list items to delimited string
        /// </summary>
        public static string ToDelimitedString<TSource> (this IEnumerable<TSource> list, char delimiter, string qualifier)
        {
            return CreateDelimitedString (list.ToList (), delimiter, qualifier);
        }

        public static string CreateDelimitedString<TSource> (this List<TSource> list, char delimiter)
        {
            const string FORMAT = "{0}{1}";

            if (list.Count == 0)
            {
                throw new InvalidOperationException ("Cannot delimit an empty list");
            }

            StringBuilder sb = new StringBuilder ();

            for (int i = 0; i < list.Count - 1; i++)
            {
                sb.AppendFormat (FORMAT, list[i], delimiter);
            }
            sb.Append (list[list.Count - 1]);

            return sb.ToString ();
        }

        /// <summary>
        /// Transforms a list items to delimited string
        /// </summary>
        public static string CreateDelimitedString<TSource> (List<TSource> list, char delimiter, string qualifier)
        {

            const string FORMAT = "{0}{1}{2}{3}";

            if (list.Count == 0)
            {
                throw new InvalidOperationException ("Cannot delimit an empty list");
            }

            StringBuilder sb = new StringBuilder ();

            for (int i = 0; i <= list.Count - 1; i++)
            {
                sb.AppendFormat (FORMAT, qualifier, list[i], qualifier, delimiter);
            }

            //remove last delimiter
            sb.Remove (sb.Length - 1, 1);



            return sb.ToString ();
        }


        #region Enumerable Helpers

        /// <summary>
        /// Checks if an IEnumerable is null or empty, containing no items
        /// </summary>    
        public static bool IsNullOrEmpty<TSource> (this IEnumerable<TSource> source)
        {
            return (source == null || !source.Any ());
        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition or 
        /// a default value if no such element exists; this method throws an exception if more than one element satisfies the condition.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">An IEnumerable(Of T) to return the single element of.</param>
        /// <param name="predicate">A function to test an element for a condition.</param>
        /// <param name="defaultValue">A value to return if no element is found</param>
        /// <returns></returns>
        public static TSource SingleOrDefault<TSource> (this IEnumerable<TSource> source, Func<TSource, bool> predicate, TSource defaultValue)
        {
            var value = source.SingleOrDefault (predicate);

            if (Equals (value, default (TSource)))
            {
                value = defaultValue;
            }

            return value;
        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition or 
        /// throws an exception if 0 or multiple are found, using the specified error message.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">An IEnumerable(Of T) to return the single element of.</param>
        /// <param name="predicate">A function to test an element for a condition.</param>
        /// <param name="errorMessage">The error message to use if no element is found</param>    
        public static TSource Single<TSource> (this IEnumerable<TSource> source, Func<TSource, bool> predicate, string errorMessage)
        {
            try
            {
                return source.Single (predicate);
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException (errorMessage, exc);
            }
        }

        /// <summary>
        /// Returns the first element of the sequence that satisfies a condition or a default value if no such elem
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">An IEnumerable(Of T) to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <param name="defaultValue">A value to return if no element is found</param>
        /// <returns></returns>
        public static TSource FirstOrDefault<TSource> (this IEnumerable<TSource> source, Func<TSource, bool> predicate, TSource defaultValue)
        {
            var value = source.FirstOrDefault (predicate);

            if (Equals (value, default (TSource)))
            {
                value = defaultValue;
            }

            return value;
        }

        #endregion

    }


}
