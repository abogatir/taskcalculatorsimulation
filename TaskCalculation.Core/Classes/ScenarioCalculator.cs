﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskCalculation.Core.Classes
{
    public class ScenarioCalculator
    {
        /// <summary>
        /// Performs the final summing and margin calculations and saves the overview
        /// </summary>    
        public static void CalculateAndSaveBidOverview(BidOverview overview)
        {
            overview.ServiceMargin = CalculateMargin(overview.TotalServiceCost, overview.TotalServicePrice);
            overview.FeeMargin = CalculateMargin(overview.TotalFeeCost, overview.TotalFeePrice);
            overview.ExpenseMargin = CalculateMargin(overview.TotalExpenseCost, overview.TotalExpensePrice);

            overview.TotalPrice = (overview.TotalServicePrice + overview.TotalFeePrice + overview.TotalExpensePrice);
            overview.TotalCost = (overview.TotalServiceCost + overview.TotalFeeCost + overview.TotalExpenseCost);
            overview.TotalMargin = CalculateMargin(overview.TotalCost, overview.TotalPrice);
        }
        /// <summary>
        /// Calculates margin based on cost and price
        /// </summary>    
        /// <returns>The calculated margin or 0 if not calculable</returns>
        private static decimal CalculateMargin(decimal cost, decimal price)
        {
            decimal margin;

            //Don't divide by 0
            if (price == 0)
            {
                return 0;
            }

            //TODO: Write margin algorithm when both factors are negative

            //If both factors are negative, a different margin formula is needed
            if (cost < 0 && price < 0)
            {
                return 0;
            }

            //If either factor is negative, the normal margin calculation is invalid
            if (cost < 0 || price < 0)
            {
                return 0;
            }

            //Calculate margin with the default algorithm
            margin = 1M - (cost / price);

            return margin;
        }
    }
}
