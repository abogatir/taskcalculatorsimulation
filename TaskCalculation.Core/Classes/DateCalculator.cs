﻿using System;

namespace TaskCalculation.Core.Classes
{
    public static class DateCalculator
    {

        #region DateAdd

        /// <summary>
        /// Calculates a date based on the start date, offset and date interval
        /// </summary>
        /// 
        /// <param name="startDate">The start date</param>
        /// <param name="offset">The offset value</param>
        /// <param name="offsetInterval">The offset interval</param>
        /// 
        /// <returns>The result of the offset calculation provided by the offset value and the offset interval</returns>
        public static DateTime DateAdd (DateTime startDate,
                                       decimal offset,
                                       DBProperties.enuDateInterval offsetInterval)
        {
            DateTime endDate;

            switch (offsetInterval)
            {
                case DBProperties.enuDateInterval.DAYS:
                    endDate = startDate.AddDays (Convert.ToDouble (offset));
                    break;

                case DBProperties.enuDateInterval.WEEKS:
                    endDate = startDate.AddDays (7 * Convert.ToDouble (offset));
                    break;

                case DBProperties.enuDateInterval.MONTHS:
                    endDate = MonthAdd (startDate, offset);
                    break;

                case DBProperties.enuDateInterval.QUARTERS:
                    endDate = MonthAdd (startDate, offset * 3);
                    break;

                case DBProperties.enuDateInterval.YEARS:
                    endDate = YearAdd (startDate, offset);
                    break;

                case DBProperties.enuDateInterval.ONCE:
                    //Dates are equal if there is no interval
                    endDate = startDate;
                    break;

                default:
                    throw new NotSupportedException (
                      string.Format ("The specified enuDateInterval [{0}] is not supported.", offsetInterval));
            }

            return endDate;
        }

        /// <summary>
        /// Calculates a date based on the start date, offset months
        /// </summary>
        /// 
        /// <param name="startDate">The start date</param>
        /// <param name="offset">The offset value in months</param>    
        /// 
        /// <returns>The result of the offset calculation provided by the offset value and the offset months</returns>   
        private static DateTime MonthAdd (DateTime startDate,
                                         decimal offset)
        {
            DateTime endDate;
            int offsetInteger;
            decimal offsetFraction;
            decimal fractionalMonthAsDays;

            offsetInteger = Convert.ToInt32 (Math.Truncate (offset));
            offsetFraction = offset - offsetInteger;

            //Add the whole months
            endDate = startDate.AddMonths (offsetInteger);

            //Add the number of days represented by the offset fraction
            fractionalMonthAsDays = (offsetFraction * DateTime.DaysInMonth (endDate.Year, endDate.Month));
            endDate = endDate.AddDays (Convert.ToDouble (fractionalMonthAsDays));

            return endDate;
        }

        /// <summary>
        /// Calculates a date based on the start date, offset years
        /// </summary>
        /// 
        /// <param name="startDate">The start date</param>
        /// <param name="offset">The offset value in years</param>    
        /// 
        /// <returns>The result of the offset calculation provided by the offset value and the offset years</returns>       
        private static DateTime YearAdd (DateTime startDate,
                                        Decimal offset)
        {
            DateTime endDate;
            int offsetInteger;
            decimal offsetFraction;
            decimal fractionalYearAsDays;
            decimal daysInYearFactor;

            offsetInteger = Convert.ToInt32 (Math.Truncate (offset));
            offsetFraction = offset - offsetInteger;

            //Add the whole years
            endDate = startDate.AddYears (offsetInteger);

            //Account for leap years in fractional logic
            if (DateTime.IsLeapYear (endDate.Year))
            {
                daysInYearFactor = 366M;
            }
            else
            {
                daysInYearFactor = 365M;
            }

            //Add the number of days represented by the offset fraction
            fractionalYearAsDays = (offsetFraction * daysInYearFactor);
            endDate = endDate.AddDays (Convert.ToDouble (fractionalYearAsDays));

            return endDate;
        }

        #endregion

        #region DateDiff

        /// <summary>
        /// Calculates difference in dates in terms of the enuDateInterval
        /// </summary>
        /// 
        /// <param name="interval">The interval defining the unit of difference</param>
        /// <param name="startDate">The earlier date</param>
        /// <param name="endDate">The later date</param>
        /// 
        /// <returns>The difference in dates in terms of the interval</returns>
        public static decimal DateDiff (DBProperties.enuDateInterval interval,
                                       DateTime startDate,
                                       DateTime endDate)
        {
            TimeSpan ts = endDate.Date - startDate.Date;

            switch (interval)
            {
                case DBProperties.enuDateInterval.DAYS:

                    //We do not need to use business days - 
                    // holidays and weekends can be included
                    return (decimal)(ts.TotalDays);

                case DBProperties.enuDateInterval.WEEKS:

                    //We do not need to use FirstDayOfWeek - 
                    // a week is simply 7 days, not the WeekOfYear.
                    return (decimal)(ts.TotalDays / 7.0);

                case DBProperties.enuDateInterval.MONTHS:

                    //Use MonthDiff to ensure fractional month difference is calculated
                    return MonthDiff (startDate, endDate);

                case DBProperties.enuDateInterval.QUARTERS:

                    //We do not need to use fiscal/business quarters -
                    // a quarter is simply 3 months
                    return MonthDiff (startDate, endDate) / 3;

                case DBProperties.enuDateInterval.YEARS:

                    //Use YearDiff to ensure fractional year difference is calculated
                    return YearDiff (startDate, endDate);

                case DBProperties.enuDateInterval.ONCE:

                    //If the date interval is Once, the interval is effectively the entire duration. 
                    // So, there will always be exactly 1 of that interval
                    return 1;

                default:
                    throw new NotSupportedException (
                      string.Format ("The specified enuDateInterval [{0}] is not supported.", interval));
            }
        }

        /// <summary>
        /// Calculates the date difference in terms of years
        /// </summary>
        /// <param name="startDate">The earlier date</param>
        /// <param name="endDate">The later date</param>
        /// 
        /// <returns>The difference between the dates, including a fractional difference in days</returns>    
        private static decimal YearDiff (DateTime startDate,
                                        DateTime endDate)
        {
            decimal yearDifference;
            decimal dayOfYearDifferenceAsFractionOfYear;
            decimal divisor;

            //Account for leap years in fractional logic
            if (DateTime.IsLeapYear (endDate.Year))
            { divisor = 366; }
            else
            { divisor = 365; }

            //NOTE: divisor is a decimal to force the CLR to preserve decimal precision in the quotient      

            dayOfYearDifferenceAsFractionOfYear = ((endDate.DayOfYear - startDate.DayOfYear) / divisor);

            yearDifference = (endDate.Year - startDate.Year);

            return yearDifference + dayOfYearDifferenceAsFractionOfYear;
        }

        /// <summary>
        /// Calculates the date difference in terms of months
        /// </summary>
        /// <param name="startDate">The earlier date</param>
        /// <param name="endDate">The later date</param>
        /// 
        /// <returns>The difference between the dates, including a fractional difference in days</returns>
        private static decimal MonthDiff (DateTime startDate,
                                         DateTime endDate)
        {
            decimal dayDifferenceAsFractionOfMonth;
            decimal monthOfYearDifference;
            decimal yearDifferenceInMonths;

            //NOTE: The decimal cast is necessary to force the CLR to preserve decimal precision in the quotient

            //Use the later date as the divisor to calculate fractional month difference
            dayDifferenceAsFractionOfMonth = ((endDate.Day - (decimal)startDate.Day) / DateTime.DaysInMonth (endDate.Year, endDate.Month));

            monthOfYearDifference = (endDate.Month - startDate.Month);
            yearDifferenceInMonths = (12 * (endDate.Year - startDate.Year));

            return dayDifferenceAsFractionOfMonth + monthOfYearDifference + yearDifferenceInMonths;
        }

        #endregion
    }
}
