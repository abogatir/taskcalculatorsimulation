﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskCalculation.Core.Mocks;

namespace TaskCalculation.Core.Classes
{
    public class PriceDriverWorkAllocationCalculator
    {
        /// <summary>
        /// Contains output data from the results of calculations
        /// </summary>
        public class Output
        {
            #region Attributes
            private List<string> mErrorMessages;
            #endregion

            #region Properties


            /// <summary>
            /// List of the original PriceDriverWorkAllocation that were persisted      
            /// </summary>
            public List<BidPriceDriverWorkAllocation> OriginalPriceDriverWorkAllocations { get; set; }

            /// <summary>
            /// List of PriceDriverWorkAllocation that need to be persisted, 
            /// representing the price driver's total allocated price
            /// </summary>
            public List<BidPriceDriverWorkAllocation> PriceDriverWorkAllocations { get; set; }

            /// <summary>
            /// List of PriceDriverWorkAllocation temporarily calculated, 
            ///  representing the price driver's total allocated cost
            /// </summary>
            public List<BidPriceDriverWorkAllocation> PriceDriverWorkAllocationCosts { get; set; }

            /// <summary>
            /// Calculation errors
            /// </summary>
            public List<string> ErrorMessages
            {
                get
                {
                    if (mErrorMessages == null)
                    {
                        mErrorMessages = new List<string> ();
                    }

                    return mErrorMessages;
                }
            }

            #endregion

            #region Constructors

            /// <summary>
            /// Constructs default instance
            /// </summary>
            public Output ()
            {
                PriceDriverWorkAllocations = new List<BidPriceDriverWorkAllocation> ();
                PriceDriverWorkAllocationCosts = new List<BidPriceDriverWorkAllocation> ();
            }

            #endregion
        }

        #region Properties

        private int BidScenarioId { get; set; }
        private int BidFunctionalServiceAreaId { get; set; }
        private int BidServiceId { get; set; }
        private int BidActivityId { get; set; }
        private BidTask Task { get; set; }
        private BidPriceDriver PriceDriver { get; set; }
        private DateTime CurrentDateTime { get; set; }
        private bool IndividualTaskCalculation { get; set; }

        private Output Result { get; set; }

        #endregion

        #region Constructor

        private PriceDriverWorkAllocationCalculator (int bidScenarioId,
                                                    int bidFunctionalServiceAreaId,
                                                    int bidServiceId,
                                                    int bidActivityId,
                                                    BidTask task,
                                                    BidPriceDriver priceDriver,
                                                    DateTime currentDateTime)
        {
            BidScenarioId = bidScenarioId;
            BidFunctionalServiceAreaId = bidFunctionalServiceAreaId;
            BidServiceId = bidServiceId;
            BidActivityId = bidActivityId;
            Task = task;
            PriceDriver = priceDriver;
            CurrentDateTime = currentDateTime;
            Result = new Output ();
        }

        #endregion

        /// <summary>
        /// Calculates the work allocation for a price driver
        /// </summary>
        /// <param name="bidScenarioId">Id of the scenario to which the price driver belongs</param>
        /// <param name="bidFunctionalServiceAreaId">ServiceArea containing Task</param>
        /// <param name="bidServiceId">Service containing Task</param>
        /// <param name="bidActivityId">Activity containing Task</param>
        /// <param name="task">BidTask to which the price driver belongs</param>
        /// <param name="priceDriver">The PriceDriver to calculate work allocation for</param>
        /// <param name="currentDateTime">The current date and time</param>
        /// <param name="dbConn">An open connection to the database</param>
        /// <param name="dbTrans">An open transaction on the connection if necessary, null otherwise</param>
        /// <returns>The calculated output with errors, price and cost</returns>
        public static Output CalculatePriceDriverWorkAllocations (int bidScenarioId,
                                                                 int bidFunctionalServiceAreaId,
                                                                 int bidServiceId,
                                                                 int bidActivityId,
                                                                 BidTask task,
                                                                 BidPriceDriver priceDriver,
                                                                 DateTime currentDateTime,
                                                                 bool individualTaskCalculation)
        {
            var calculator = new PriceDriverWorkAllocationCalculator (bidScenarioId,
                                                                     bidFunctionalServiceAreaId,
                                                                     bidServiceId,
                                                                     bidActivityId,
                                                                     task,
                                                                     priceDriver,
                                                                     currentDateTime);

            calculator.IndividualTaskCalculation = individualTaskCalculation;
            calculator.Calculate ();

            return calculator.Result;
        }

        /// <summary>
        /// Calculates the work allocation for a price driver
        /// </summary>
        private void Calculate ()
        {

            //Get the current BidWorkAllocations 
            // Notice DSBidServices method is called. This method currently doesn't need BWA reference properties.
            //TODO clarify (this is mocked)
            //var bidAllocations =
            //   CacheManager.GetBidWorkAllocationsWithoutReferences (BidScenarioId,
            //                                      null,
            //                                      BSBidServices.GetEffectiveFunctionCode (PriceDriver),
            //                                      true,
            //                                      DbConnection,
            //                                      DbTransaction).Where (bwa => bwa.WorkAllocationMethod == PriceDriver.WorkAllocationMethod);
            var bidAllocations = BidWorkAllocations.BidWorkAllocationsList.Where(x=>x.NatFunctionCode==PriceDriver.FunctionCode);

            //Get the already-existing PriceDriverWorkAllocations
            //if (IndividualTaskCalculation)
            //{
            //    Result.OriginalPriceDriverWorkAllocations =
            //      DSBidServices.GetBidPriceDriverWorkAllocations (PriceDriver.BidPriceDriverId,
            //                                                    DbConnection,
            //                                                    DbTransaction);
            //}
            //else
            //{
            //    Result.OriginalPriceDriverWorkAllocations =
            //      CalculationBatchManager.GetBidPriceDriverWorkAllocation (PriceDriver.BidPriceDriverId,
            //                                                              DbConnection,
            //                                                              DbTransaction);
            //}
            //TODO clarify this is mocked
            Result.OriginalPriceDriverWorkAllocations = BidPriceDriverWorkAllocations.BidPriceDriverWorkAllocationsList.Where(x=>x.BidPriceDriverId==PriceDriver.BidPriceDriverId).ToList();
            //Get the active Price card
            //BidRateCard priceCard = CacheManager.GetBidRateCard (BidScenarioId,
            //                                                    DBProperties.enuRateCardType.ACTIVE_BILLING_RATE_CARD,
            //                                                    CurrentDateTime,
            //                                                    DbConnection,
            //                                                    DbTransaction);

            ////Get the active Cost card
            //BidRateCard costCard = CacheManager.GetBidRateCard (BidScenarioId,
            //                                                   DBProperties.enuRateCardType.STANDARD_COST_RATE_CARD,
            //                                                   CurrentDateTime,
            //                                                   DbConnection,
            //                                                   DbTransaction);
            BidRateCard priceCard = BidRateCards.PriceBidRateCard;
            BidRateCard costCard = BidRateCards.CostBidRateCard;

            //Build the list of allocations and calculation financials for each one
            CalculatePriceDriverWorkAllocation (bidAllocations,
                                               priceCard.BidRateCardId,
                                               costCard.BidRateCardId);

        }

        #region Calculation Methods

        /// <summary>
        /// Calculates non-country-specific work allocation records for a price driver
        /// </summary>
        /// <param name="bidAllocations">BidWorkAllocations that exist for the scenario</param>    
        /// <param name="priceRateCardId">The BidRateCard containing price rates</param>
        /// <param name="costRateCardId">The BidRateCard containing cost rates</param>
        private void CalculatePriceDriverWorkAllocation (IEnumerable<BidWorkAllocation> bidAllocations,
                                                        int priceRateCardId,
                                                        int costRateCardId)
        {
            List<BidPriceDriverWorkAllocation> newPriceDriverAllocations;


            if (PriceDriver.UsesCountrySpecificPerUnit)
            {
                //Build a list of country-specific price driver allocations based on work allocations for the scenario
                newPriceDriverAllocations = BuildCountryPriceDriverAllocations (bidAllocations.Where (bwa => bwa.CountryId != AppConstants.NO_ID));
            }
            else
            {
                //Build a list of price driver allocations based on work allocations for the scenario
                newPriceDriverAllocations = BuildPriceDriverAllocations (bidAllocations.Where (bwa => bwa.CountryId == AppConstants.NO_ID));
            }

            //Get the bid rates for the price driver function code
            List<BidRate> rates = GetBidRates ();

            //Calculate items that will be saved
            CalculateAllocationPrices (newPriceDriverAllocations, rates, priceRateCardId);

            //Calculate items that will be used only to perform margin calculations
            CalculateAllocationCosts (newPriceDriverAllocations, rates, costRateCardId);
        }

        /// <summary>
        /// Calculates the PriceDriverWorkAllocations that will be saved and 
        /// used as the price driver's total price
        /// </summary>
        /// <param name="priceDriverAllocations">The list of allocations to be calculated with rates</param>
        /// <param name="rates">List of applicable bid rates</param>
        /// <param name="priceRateCardId">Id of the price BidRateCard</param>
        private void CalculateAllocationPrices (List<BidPriceDriverWorkAllocation> priceDriverAllocations,
                                               IEnumerable<BidRate> rates,
                                               int priceRateCardId)
        {
            //For each allocation that should be save, calculate pricing information
            foreach (BidPriceDriverWorkAllocation priceDriverAllocation in priceDriverAllocations)
            {

                //Find the rate that matches the BufCode and belongs to the desired active rate card type
                //TODO clarify (do not uses override so simply function code) 
                string bufCode = priceDriverAllocation.BuCode + PriceDriver.FunctionCode;
                BidRate matchingRate = rates.SingleOrDefault (r => r.BufCode == bufCode &&
                                                             r.BidRateCardId == priceRateCardId);


                //If none exists, throw exception; this shouldn't occur
                if (matchingRate == null)
                {
                    //matchingRate = GetTestBidRate();
                    throw new InvalidOperationException (
                      string.Format ("No BidRate exists: BidScenarioId {0} BufCode {1} PriceRateCardId {2}", BidScenarioId, bufCode,
                                    priceRateCardId));
                }

                //Use the BidRate to populate financial data on the allocation
                PopulateAllocationFinancialData (priceDriverAllocation, matchingRate);
            }

            //Assign the new allocations to the result
            Result.PriceDriverWorkAllocations = priceDriverAllocations;

        }

        /// <summary>
        /// Calculates the temporary PriceDriverWorkAllocations that will be  
        /// used as the price driver's total cost
        /// </summary>
        /// <param name="priceDriverAllocations">The list of allocations to be calculated with rates</param>
        /// <param name="rates">List of applicable bid rates</param>
        /// <param name="costRateCardId">Id of the cost BidRateCard</param>
        private void CalculateAllocationCosts (IEnumerable<BidPriceDriverWorkAllocation> priceDriverAllocations,
                                              IEnumerable<BidRate> rates,
                                              int costRateCardId)
        {

            //Calculate cost information to let consumers calculate overview with margins
            foreach (BidPriceDriverWorkAllocation priceDriverAllocation in priceDriverAllocations)
            {
                //TODO clarify (do not uses overrides, so simply function code)
                //Find the rate that matches the BufCode and belongs to the desired active rate card type
                string bufCode = priceDriverAllocation.BuCode + PriceDriver.FunctionCode;
                BidRate matchingRate = rates.SingleOrDefault (r => r.BufCode == bufCode &&
                                                             r.BidRateCardId == costRateCardId);

                //Fabricate a PDWorkAllocation just to temporarily store cost informaiton
                BidPriceDriverWorkAllocation allocationCost = new BidPriceDriverWorkAllocation ();
                allocationCost.TotalEffort = priceDriverAllocation.TotalEffort;
                allocationCost.Percentage = priceDriverAllocation.Percentage;
                allocationCost.BidPriceDriverId = priceDriverAllocation.BidPriceDriverId;
                allocationCost.BuCode = priceDriverAllocation.BuCode;

                //If none exists, use the default bid rate
                if (matchingRate == null)
                {
                    matchingRate = GetTestBidRate ();

                    //throw new InvalidOperationException(
                    //  string.Format("No BidRate exists: BidScenarioId {0} BufCode {1} PriceRateCardId {2}", BidScenarioId, bufCode,
                    //                priceRateCardId));
                }

                //Use the BidRate to populate financial data on the allocation
                PopulateAllocationFinancialData (allocationCost, matchingRate);

                //Add the cost to the list of work allocation costs
                Result.PriceDriverWorkAllocationCosts.Add (allocationCost);
            }
        }

        /// <summary>
        /// Builds a list of all work allocations that should exist for a price driver. 
        /// </summary>
        /// <param name="bidAllocations">List of relevant BidWorkAllocations that exist for the BidScenario</param>    
        /// 
        /// <returns>A list of BidPriceDriverWorkAllocations ready to have their financial data populated</returns>
        private List<BidPriceDriverWorkAllocation> BuildPriceDriverAllocations (IEnumerable<BidWorkAllocation> bidAllocations)
        {
            List<BidPriceDriverWorkAllocation> newPriceDriverAllocations = new List<BidPriceDriverWorkAllocation> ();

            foreach (BidWorkAllocation allocation in bidAllocations)
            {
                //Don't access modified closure
                string buCode = allocation.BuCode;
                BidPriceDriverWorkAllocation newAllocation;

                //Instantiate with minimal identifying data
                newAllocation = new BidPriceDriverWorkAllocation
                {
                    BuCode = allocation.BuCode,
                    BidPriceDriverId = PriceDriver.BidPriceDriverId
                };

                //See if the PDAllocation already exists
                BidPriceDriverWorkAllocation existingAllocation =
                  Result.OriginalPriceDriverWorkAllocations.SingleOrDefault (w => w.BidPriceDriverId == PriceDriver.BidPriceDriverId &&
                                                                      w.BuCode == buCode);

                //If it exists, copy pertinent data
                if (existingAllocation != null)
                {
                    newAllocation.BidPriceDriverWorkAllocationId = existingAllocation.BidPriceDriverWorkAllocationId;
                }

                //Update with current allocation data
                if (PriceDriver.WorkAllocationMethod == DBProperties.enuWorkAllocationMethod.FTE_AND_LOCATION)
                {
                    //NOTE: If Headcount/FTE is used, the percentage should always be 100% 
                    // and the effort should match the FTE effort manually entered

                    BidPriceDriverFte fte = PriceDriver.BidPriceDriverFTEs.SingleOrDefault (f => f.BuCode == buCode);

                    //If the FTE doesn't exist, there was a change to bid work allocation during calculation, 
                    // just go forward and expect the change to be picked up by the next calculation.
                    if (fte == null)
                    {
                        newAllocation.Percentage = 0;
                        newAllocation.TotalEffort = 0;
                    }
                    else
                    {
                        newAllocation.Percentage = 1;
                        newAllocation.TotalEffort = fte.TotalEffortInHours;
                    }

                }
                else
                {
                    //Otherwise, use the scenario work allocation percentage
                    // and the price driver's total effort
                    newAllocation.Percentage = allocation.Percentage;
                    newAllocation.TotalEffort = PriceDriver.TotalEffortInHours * allocation.Percentage;
                }

                newPriceDriverAllocations.Add (newAllocation);
            }

            return newPriceDriverAllocations;
        }

        /// <summary>
        /// Builds a list of all work allocations that should exist for a price driver. 
        /// </summary>
        /// <param name="bidAllocations">List of relevant BidWorkAllocations that exist for the BidScenario</param>    
        /// 
        /// <returns>A list of BidPriceDriverWorkAllocations ready to have their financial data populated</returns>
        private List<BidPriceDriverWorkAllocation> BuildCountryPriceDriverAllocations (IEnumerable<BidWorkAllocation> bidAllocations)
        {
            List<BidPriceDriverWorkAllocation> newPriceDriverAllocations = new List<BidPriceDriverWorkAllocation> ();

            //Group by business unit
            var groupedAllocations = bidAllocations.GroupBy (bwa => bwa.BuCode);

            //Build up the list of country work allocations per business unit
            foreach (var group in groupedAllocations)
            {
                string buCode = group.Key;
                BidPriceDriverWorkAllocation newAllocation;

                //Instatiate new allocation
                newAllocation = new BidPriceDriverWorkAllocation
                {
                    BuCode = buCode,
                    BidPriceDriverId = PriceDriver.BidPriceDriverId,
                    Percentage = 1
                };

                //See if the PriceDriverAllocation already exists
                BidPriceDriverWorkAllocation existingAllocation =
                  Result.OriginalPriceDriverWorkAllocations.SingleOrDefault (w => w.BidPriceDriverId == PriceDriver.BidPriceDriverId &&
                                                                      w.BuCode == buCode);

                //If it already exists, copy pertinent data
                if (existingAllocation != null)
                {
                    newAllocation.BidPriceDriverWorkAllocationId = existingAllocation.BidPriceDriverWorkAllocationId;
                    newAllocation.Percentage = existingAllocation.Percentage;
                }

                //Find all of the price driver country records that are allocated to this business unit
                foreach (var allocation in group)
                {
                    int countryId = allocation.CountryId;
                    BidPriceDriverCountry matchingCountry;

                    //TODO: Is it OK for work to be allocated in a country that this price driver doesn't have?
                    matchingCountry = PriceDriver.BidPriceDriverCountries.FirstOrDefault (bpdc => bpdc.CountryId == countryId);

                    //Add each country's total effor to the business unit's allocation
                    if (matchingCountry != null)
                    {
                        newAllocation.TotalEffort += matchingCountry.TotalEffortInHours;
                    }
                }

                newPriceDriverAllocations.Add (newAllocation);
            }

            return newPriceDriverAllocations;
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Populate financial data (rate and totals) on a price driver allocation
        /// </summary>
        /// <param name="priceDriverAllocation">The allocation to calculate financical data for</param>
        /// <param name="rate">The rate information</param>
        private void PopulateAllocationFinancialData (BidPriceDriverWorkAllocation priceDriverAllocation, BidRate rate)
        {
            if (rate.BidRateOverride.BidRateOverrideId == BidRateOverride.NEW_BID_RATE_OVERRIDE_ID)
            {
                priceDriverAllocation.AppliedRate = rate.AppliedRate;
                priceDriverAllocation.CurrencyCode = rate.CurrencyCode;
                priceDriverAllocation.ExchangeRate = rate.ExchangeRate;
                priceDriverAllocation.InflationRate = rate.InflationRate;
            }
            else
            {
                priceDriverAllocation.AppliedRate = rate.BidRateOverride.AppliedRate;
                priceDriverAllocation.CurrencyCode = rate.BidRateOverride.CurrencyCode;
                priceDriverAllocation.ExchangeRate = rate.BidRateOverride.ExchangeRate;
                priceDriverAllocation.InflationRate = rate.BidRateOverride.InflationRate;
            }

            //TODO clarify
            priceDriverAllocation.DiscountPercent = 0.15M;//BSBidServices.GetParentBidRateOverride (rate.BidRateId, DBProperties.enuOverrideType.BID_DISCOUNT_PERCENT, rate.BidRateOverride.WorkBreakdownStructureLevel, rate.BidRateOverride.WorkBreakdownStructureLevelId);
            priceDriverAllocation.Rate = 100;//Convert.ToDecimal (BSBidServices.GetParentBidRateOverride (rate.BidRateId, DBProperties.enuOverrideType.BID_RATE_OVERRIDE, rate.BidRateOverride.WorkBreakdownStructureLevel, rate.BidRateOverride.WorkBreakdownStructureLevelId));

            //Calculate the total price
            priceDriverAllocation.TotalPrice = CalculateTotalPrice (priceDriverAllocation);
        }

        /// <summary>
        /// Calculates the total price for a price driver work allocation
        /// </summary>    
        /// <returns>The calculated total price</returns>
        private decimal CalculateTotalPrice (BidPriceDriverWorkAllocation priceDriverAllocation)
        {
            return (priceDriverAllocation.TotalEffort * priceDriverAllocation.AppliedRate);
        }

        /// <summary>
        /// Gets the list of bid rates that might apply to this price driver's function code
        /// </summary>    
        private List<BidRate> GetBidRates ()
        {
            //TODO clarify (got to be rechecked)
            if (PriceDriver.FunctionCode == "CR")
                return Enumerable.Take<BidRate>(BidRates.BidRatesList, 45).ToList();
            if (PriceDriver.FunctionCode == "DS")
                return Enumerable.Skip<BidRate>(BidRates.BidRatesList, 45).Take(3).ToList();      
            return new List<BidRate>();
            //return CalculationBatchManager.GetBidRates (BSBidServices.GetEffectiveFunctionCode (PriceDriver),
            //                         null,
            //                         BidScenarioId,
            //                         BidFunctionalServiceAreaId,
            //                         BidServiceId,
            //                         BidActivityId,
            //                         Task.BidTaskId,
            //                         CurrentDateTime,
            //                         DbConnection,
            //                         DbTransaction);
        }

        /// <summary>
        /// Gets a default bid rate, used only during testing when a real bid rate doesn't exist.
        /// </summary>    
        private BidRate GetTestBidRate ()
        {
            return new BidRate
            {
                BidRateOverride = new BidRateOverride ()
                {
                    BidRateOverrideId = -1
                },
                AppliedRate = 1M,
                CurrencyCode = "USD",
                DiscountPercent = 0M,
                ExchangeRate = 1M,
                InflationRate = 1M,
                Rate = 1M
            };
        }

        #endregion
    }

}
