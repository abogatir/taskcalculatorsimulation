﻿namespace TaskCalculation.Core.Classes
{
    public static class SiteConstants
    {
        public const string HOURS_DISPLAY = "###,###,###,##0";
        public const string CURRENCY_DISPLAY = "###,###,###,##0.00";
    }
}
